import * as $protobuf from "protobufjs";
/** Namespace code. */
export namespace code {

    /** Namespace justin. */
    namespace justin {

        /** Namespace tv. */
        namespace tv {

            /** Namespace commerce. */
            namespace commerce {

                /** Namespace phoenix. */
                namespace phoenix {

                    /** Represents a Phoenix */
                    class Phoenix extends $protobuf.rpc.Service {

                        /**
                         * Constructs a new Phoenix service.
                         * @param rpcImpl RPC implementation
                         * @param [requestDelimited=false] Whether requests are length-delimited
                         * @param [responseDelimited=false] Whether responses are length-delimited
                         */
                        constructor(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean);

                        /**
                         * Creates new Phoenix service using the specified rpc implementation.
                         * @param rpcImpl RPC implementation
                         * @param [requestDelimited=false] Whether requests are length-delimited
                         * @param [responseDelimited=false] Whether responses are length-delimited
                         * @returns RPC service. Useful where requests and/or responses are streamed.
                         */
                        public static create(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean): Phoenix;

                        /**
                         * Calls HealthCheck.
                         * @param request HealthCheckReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and HealthCheckResp
                         */
                        public healthCheck(request: code.justin.tv.commerce.phoenix.IHealthCheckReq, callback: code.justin.tv.commerce.phoenix.Phoenix.HealthCheckCallback): void;

                        /**
                         * Calls HealthCheck.
                         * @param request HealthCheckReq message or plain object
                         * @returns Promise
                         */
                        public healthCheck(request: code.justin.tv.commerce.phoenix.IHealthCheckReq): Promise<code.justin.tv.commerce.phoenix.HealthCheckResp>;

                        /**
                         * Calls PhantoGenerateKeyBatch.
                         * @param request GenerateKeyBatchReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GenerateKeyBatchResp
                         */
                        public phantoGenerateKeyBatch(request: code.justin.tv.commerce.phanto.IGenerateKeyBatchReq, callback: code.justin.tv.commerce.phoenix.Phoenix.PhantoGenerateKeyBatchCallback): void;

                        /**
                         * Calls PhantoGenerateKeyBatch.
                         * @param request GenerateKeyBatchReq message or plain object
                         * @returns Promise
                         */
                        public phantoGenerateKeyBatch(request: code.justin.tv.commerce.phanto.IGenerateKeyBatchReq): Promise<code.justin.tv.commerce.phanto.GenerateKeyBatchResp>;

                        /**
                         * Calls PhantoGetAllKeyPools.
                         * @param request GetAllKeyPoolsReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetAllKeyPoolsResp
                         */
                        public phantoGetAllKeyPools(request: code.justin.tv.commerce.phanto.IGetAllKeyPoolsReq, callback: code.justin.tv.commerce.phoenix.Phoenix.PhantoGetAllKeyPoolsCallback): void;

                        /**
                         * Calls PhantoGetAllKeyPools.
                         * @param request GetAllKeyPoolsReq message or plain object
                         * @returns Promise
                         */
                        public phantoGetAllKeyPools(request: code.justin.tv.commerce.phanto.IGetAllKeyPoolsReq): Promise<code.justin.tv.commerce.phanto.GetAllKeyPoolsResp>;

                        /**
                         * Calls PhantoGetKeyBatches.
                         * @param request GetKeyBatchesReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyBatchesResp
                         */
                        public phantoGetKeyBatches(request: code.justin.tv.commerce.phanto.IGetKeyBatchesReq, callback: code.justin.tv.commerce.phoenix.Phoenix.PhantoGetKeyBatchesCallback): void;

                        /**
                         * Calls PhantoGetKeyBatches.
                         * @param request GetKeyBatchesReq message or plain object
                         * @returns Promise
                         */
                        public phantoGetKeyBatches(request: code.justin.tv.commerce.phanto.IGetKeyBatchesReq): Promise<code.justin.tv.commerce.phanto.GetKeyBatchesResp>;
                    }

                    namespace Phoenix {

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phoenix.Phoenix#healthCheck}.
                         * @param error Error, if any
                         * @param [response] HealthCheckResp
                         */
                        type HealthCheckCallback = (error: (Error|null), response?: code.justin.tv.commerce.phoenix.HealthCheckResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phoenix.Phoenix#phantoGenerateKeyBatch}.
                         * @param error Error, if any
                         * @param [response] GenerateKeyBatchResp
                         */
                        type PhantoGenerateKeyBatchCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GenerateKeyBatchResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phoenix.Phoenix#phantoGetAllKeyPools}.
                         * @param error Error, if any
                         * @param [response] GetAllKeyPoolsResp
                         */
                        type PhantoGetAllKeyPoolsCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetAllKeyPoolsResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phoenix.Phoenix#phantoGetKeyBatches}.
                         * @param error Error, if any
                         * @param [response] GetKeyBatchesResp
                         */
                        type PhantoGetKeyBatchesCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyBatchesResp) => void;
                    }

                    /** Properties of a HealthCheckReq. */
                    interface IHealthCheckReq {
                    }

                    /** Represents a HealthCheckReq. */
                    class HealthCheckReq implements IHealthCheckReq {

                        /**
                         * Constructs a new HealthCheckReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phoenix.IHealthCheckReq);

                        /**
                         * Creates a new HealthCheckReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HealthCheckReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phoenix.IHealthCheckReq): code.justin.tv.commerce.phoenix.HealthCheckReq;

                        /**
                         * Encodes the specified HealthCheckReq message. Does not implicitly {@link code.justin.tv.commerce.phoenix.HealthCheckReq.verify|verify} messages.
                         * @param message HealthCheckReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phoenix.IHealthCheckReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HealthCheckReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phoenix.HealthCheckReq.verify|verify} messages.
                         * @param message HealthCheckReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phoenix.IHealthCheckReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HealthCheckReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HealthCheckReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phoenix.HealthCheckReq;

                        /**
                         * Decodes a HealthCheckReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HealthCheckReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phoenix.HealthCheckReq;

                        /**
                         * Verifies a HealthCheckReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HealthCheckReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HealthCheckReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phoenix.HealthCheckReq;

                        /**
                         * Creates a plain object from a HealthCheckReq message. Also converts values to other types if specified.
                         * @param message HealthCheckReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phoenix.HealthCheckReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HealthCheckReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a HealthCheckResp. */
                    interface IHealthCheckResp {

                        /** HealthCheckResp response */
                        response?: (string|null);
                    }

                    /** Represents a HealthCheckResp. */
                    class HealthCheckResp implements IHealthCheckResp {

                        /**
                         * Constructs a new HealthCheckResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phoenix.IHealthCheckResp);

                        /** HealthCheckResp response. */
                        public response: string;

                        /**
                         * Creates a new HealthCheckResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HealthCheckResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phoenix.IHealthCheckResp): code.justin.tv.commerce.phoenix.HealthCheckResp;

                        /**
                         * Encodes the specified HealthCheckResp message. Does not implicitly {@link code.justin.tv.commerce.phoenix.HealthCheckResp.verify|verify} messages.
                         * @param message HealthCheckResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phoenix.IHealthCheckResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HealthCheckResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phoenix.HealthCheckResp.verify|verify} messages.
                         * @param message HealthCheckResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phoenix.IHealthCheckResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HealthCheckResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HealthCheckResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phoenix.HealthCheckResp;

                        /**
                         * Decodes a HealthCheckResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HealthCheckResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phoenix.HealthCheckResp;

                        /**
                         * Verifies a HealthCheckResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HealthCheckResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HealthCheckResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phoenix.HealthCheckResp;

                        /**
                         * Creates a plain object from a HealthCheckResp message. Also converts values to other types if specified.
                         * @param message HealthCheckResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phoenix.HealthCheckResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HealthCheckResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }
                }

                /** Namespace phanto. */
                namespace phanto {

                    /** Represents a Phanto */
                    class Phanto extends $protobuf.rpc.Service {

                        /**
                         * Constructs a new Phanto service.
                         * @param rpcImpl RPC implementation
                         * @param [requestDelimited=false] Whether requests are length-delimited
                         * @param [responseDelimited=false] Whether responses are length-delimited
                         */
                        constructor(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean);

                        /**
                         * Creates new Phanto service using the specified rpc implementation.
                         * @param rpcImpl RPC implementation
                         * @param [requestDelimited=false] Whether requests are length-delimited
                         * @param [responseDelimited=false] Whether responses are length-delimited
                         * @returns RPC service. Useful where requests and/or responses are streamed.
                         */
                        public static create(rpcImpl: $protobuf.RPCImpl, requestDelimited?: boolean, responseDelimited?: boolean): Phanto;

                        /**
                         * Calls HealthCheck.
                         * @param request HealthCheckReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and HealthCheckResp
                         */
                        public healthCheck(request: code.justin.tv.commerce.phanto.IHealthCheckReq, callback: code.justin.tv.commerce.phanto.Phanto.HealthCheckCallback): void;

                        /**
                         * Calls HealthCheck.
                         * @param request HealthCheckReq message or plain object
                         * @returns Promise
                         */
                        public healthCheck(request: code.justin.tv.commerce.phanto.IHealthCheckReq): Promise<code.justin.tv.commerce.phanto.HealthCheckResp>;

                        /**
                         * Calls CreateProductType.
                         * @param request CreateProductTypeReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and CreateProductTypeResp
                         */
                        public createProductType(request: code.justin.tv.commerce.phanto.ICreateProductTypeReq, callback: code.justin.tv.commerce.phanto.Phanto.CreateProductTypeCallback): void;

                        /**
                         * Calls CreateProductType.
                         * @param request CreateProductTypeReq message or plain object
                         * @returns Promise
                         */
                        public createProductType(request: code.justin.tv.commerce.phanto.ICreateProductTypeReq): Promise<code.justin.tv.commerce.phanto.CreateProductTypeResp>;

                        /**
                         * Calls CreateProduct.
                         * @param request CreateProductReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and CreateProductResp
                         */
                        public createProduct(request: code.justin.tv.commerce.phanto.ICreateProductReq, callback: code.justin.tv.commerce.phanto.Phanto.CreateProductCallback): void;

                        /**
                         * Calls CreateProduct.
                         * @param request CreateProductReq message or plain object
                         * @returns Promise
                         */
                        public createProduct(request: code.justin.tv.commerce.phanto.ICreateProductReq): Promise<code.justin.tv.commerce.phanto.CreateProductResp>;

                        /**
                         * Calls CreateKeyPool.
                         * @param request CreateKeyPoolReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and CreateKeyPoolResp
                         */
                        public createKeyPool(request: code.justin.tv.commerce.phanto.ICreateKeyPoolReq, callback: code.justin.tv.commerce.phanto.Phanto.CreateKeyPoolCallback): void;

                        /**
                         * Calls CreateKeyPool.
                         * @param request CreateKeyPoolReq message or plain object
                         * @returns Promise
                         */
                        public createKeyPool(request: code.justin.tv.commerce.phanto.ICreateKeyPoolReq): Promise<code.justin.tv.commerce.phanto.CreateKeyPoolResp>;

                        /**
                         * Calls GenerateKeyBatch.
                         * @param request GenerateKeyBatchReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GenerateKeyBatchResp
                         */
                        public generateKeyBatch(request: code.justin.tv.commerce.phanto.IGenerateKeyBatchReq, callback: code.justin.tv.commerce.phanto.Phanto.GenerateKeyBatchCallback): void;

                        /**
                         * Calls GenerateKeyBatch.
                         * @param request GenerateKeyBatchReq message or plain object
                         * @returns Promise
                         */
                        public generateKeyBatch(request: code.justin.tv.commerce.phanto.IGenerateKeyBatchReq): Promise<code.justin.tv.commerce.phanto.GenerateKeyBatchResp>;

                        /**
                         * Calls GetKeyBatchStatus.
                         * @param request GetKeyBatchStatusReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyBatchStatusResp
                         */
                        public getKeyBatchStatus(request: code.justin.tv.commerce.phanto.IGetKeyBatchStatusReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyBatchStatusCallback): void;

                        /**
                         * Calls GetKeyBatchStatus.
                         * @param request GetKeyBatchStatusReq message or plain object
                         * @returns Promise
                         */
                        public getKeyBatchStatus(request: code.justin.tv.commerce.phanto.IGetKeyBatchStatusReq): Promise<code.justin.tv.commerce.phanto.GetKeyBatchStatusResp>;

                        /**
                         * Calls GetKeyBatchDownloadInfo.
                         * @param request GetKeyBatchDownloadInfoReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyBatchDownloadInfoResp
                         */
                        public getKeyBatchDownloadInfo(request: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyBatchDownloadInfoCallback): void;

                        /**
                         * Calls GetKeyBatchDownloadInfo.
                         * @param request GetKeyBatchDownloadInfoReq message or plain object
                         * @returns Promise
                         */
                        public getKeyBatchDownloadInfo(request: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoReq): Promise<code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoResp>;

                        /**
                         * Calls AdminGetKeyStatus.
                         * @param request AdminGetKeyStatusReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and AdminGetKeyStatusResp
                         */
                        public adminGetKeyStatus(request: code.justin.tv.commerce.phanto.IAdminGetKeyStatusReq, callback: code.justin.tv.commerce.phanto.Phanto.AdminGetKeyStatusCallback): void;

                        /**
                         * Calls AdminGetKeyStatus.
                         * @param request AdminGetKeyStatusReq message or plain object
                         * @returns Promise
                         */
                        public adminGetKeyStatus(request: code.justin.tv.commerce.phanto.IAdminGetKeyStatusReq): Promise<code.justin.tv.commerce.phanto.AdminGetKeyStatusResp>;

                        /**
                         * Calls GetKeyStatus.
                         * @param request GetKeyStatusReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyStatusResp
                         */
                        public getKeyStatus(request: code.justin.tv.commerce.phanto.IGetKeyStatusReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyStatusCallback): void;

                        /**
                         * Calls GetKeyStatus.
                         * @param request GetKeyStatusReq message or plain object
                         * @returns Promise
                         */
                        public getKeyStatus(request: code.justin.tv.commerce.phanto.IGetKeyStatusReq): Promise<code.justin.tv.commerce.phanto.GetKeyStatusResp>;

                        /**
                         * Calls RedeemKey.
                         * @param request RedeemKeyReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and RedeemKeyResp
                         */
                        public redeemKey(request: code.justin.tv.commerce.phanto.IRedeemKeyReq, callback: code.justin.tv.commerce.phanto.Phanto.RedeemKeyCallback): void;

                        /**
                         * Calls RedeemKey.
                         * @param request RedeemKeyReq message or plain object
                         * @returns Promise
                         */
                        public redeemKey(request: code.justin.tv.commerce.phanto.IRedeemKeyReq): Promise<code.justin.tv.commerce.phanto.RedeemKeyResp>;

                        /**
                         * Calls RedeemKeyForLoadTesting.
                         * @param request RedeemKeyReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and RedeemKeyResp
                         */
                        public redeemKeyForLoadTesting(request: code.justin.tv.commerce.phanto.IRedeemKeyReq, callback: code.justin.tv.commerce.phanto.Phanto.RedeemKeyForLoadTestingCallback): void;

                        /**
                         * Calls RedeemKeyForLoadTesting.
                         * @param request RedeemKeyReq message or plain object
                         * @returns Promise
                         */
                        public redeemKeyForLoadTesting(request: code.justin.tv.commerce.phanto.IRedeemKeyReq): Promise<code.justin.tv.commerce.phanto.RedeemKeyResp>;

                        /**
                         * Calls InvalidateKeys.
                         * @param request InvalidateKeysReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and InvalidateKeysResp
                         */
                        public invalidateKeys(request: code.justin.tv.commerce.phanto.IInvalidateKeysReq, callback: code.justin.tv.commerce.phanto.Phanto.InvalidateKeysCallback): void;

                        /**
                         * Calls InvalidateKeys.
                         * @param request InvalidateKeysReq message or plain object
                         * @returns Promise
                         */
                        public invalidateKeys(request: code.justin.tv.commerce.phanto.IInvalidateKeysReq): Promise<code.justin.tv.commerce.phanto.InvalidateKeysResp>;

                        /**
                         * Calls InvalidateKeyBatch.
                         * @param request InvalidateKeyBatchReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and InvalidateKeyBatchResp
                         */
                        public invalidateKeyBatch(request: code.justin.tv.commerce.phanto.IInvalidateKeyBatchReq, callback: code.justin.tv.commerce.phanto.Phanto.InvalidateKeyBatchCallback): void;

                        /**
                         * Calls InvalidateKeyBatch.
                         * @param request InvalidateKeyBatchReq message or plain object
                         * @returns Promise
                         */
                        public invalidateKeyBatch(request: code.justin.tv.commerce.phanto.IInvalidateKeyBatchReq): Promise<code.justin.tv.commerce.phanto.InvalidateKeyBatchResp>;

                        /**
                         * Calls InvalidateKeyPool.
                         * @param request InvalidateKeyPoolReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and InvalidateKeyPoolResp
                         */
                        public invalidateKeyPool(request: code.justin.tv.commerce.phanto.IInvalidateKeyPoolReq, callback: code.justin.tv.commerce.phanto.Phanto.InvalidateKeyPoolCallback): void;

                        /**
                         * Calls InvalidateKeyPool.
                         * @param request InvalidateKeyPoolReq message or plain object
                         * @returns Promise
                         */
                        public invalidateKeyPool(request: code.justin.tv.commerce.phanto.IInvalidateKeyPoolReq): Promise<code.justin.tv.commerce.phanto.InvalidateKeyPoolResp>;

                        /**
                         * Calls GetAllKeyPools.
                         * @param request GetAllKeyPoolsReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetAllKeyPoolsResp
                         */
                        public getAllKeyPools(request: code.justin.tv.commerce.phanto.IGetAllKeyPoolsReq, callback: code.justin.tv.commerce.phanto.Phanto.GetAllKeyPoolsCallback): void;

                        /**
                         * Calls GetAllKeyPools.
                         * @param request GetAllKeyPoolsReq message or plain object
                         * @returns Promise
                         */
                        public getAllKeyPools(request: code.justin.tv.commerce.phanto.IGetAllKeyPoolsReq): Promise<code.justin.tv.commerce.phanto.GetAllKeyPoolsResp>;

                        /**
                         * Calls GetKeyBatches.
                         * @param request GetKeyBatchesReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyBatchesResp
                         */
                        public getKeyBatches(request: code.justin.tv.commerce.phanto.IGetKeyBatchesReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyBatchesCallback): void;

                        /**
                         * Calls GetKeyBatches.
                         * @param request GetKeyBatchesReq message or plain object
                         * @returns Promise
                         */
                        public getKeyBatches(request: code.justin.tv.commerce.phanto.IGetKeyBatchesReq): Promise<code.justin.tv.commerce.phanto.GetKeyBatchesResp>;

                        /**
                         * Calls GetKeyBatchesS2S.
                         * @param request GetKeyBatchesReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyBatchesResp
                         */
                        public getKeyBatchesS2S(request: code.justin.tv.commerce.phanto.IGetKeyBatchesReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyBatchesS2SCallback): void;

                        /**
                         * Calls GetKeyBatchesS2S.
                         * @param request GetKeyBatchesReq message or plain object
                         * @returns Promise
                         */
                        public getKeyBatchesS2S(request: code.justin.tv.commerce.phanto.IGetKeyBatchesReq): Promise<code.justin.tv.commerce.phanto.GetKeyBatchesResp>;

                        /**
                         * Calls GetKeyPoolsByHandler.
                         * @param request GetKeyPoolsByHandlerReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyPoolsByHandlerResp
                         */
                        public getKeyPoolsByHandler(request: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyPoolsByHandlerCallback): void;

                        /**
                         * Calls GetKeyPoolsByHandler.
                         * @param request GetKeyPoolsByHandlerReq message or plain object
                         * @returns Promise
                         */
                        public getKeyPoolsByHandler(request: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerReq): Promise<code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerResp>;

                        /**
                         * Calls GetAllProductTypes.
                         * @param request GetAllProductTypesReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetAllProductTypesResp
                         */
                        public getAllProductTypes(request: code.justin.tv.commerce.phanto.IGetAllProductTypesReq, callback: code.justin.tv.commerce.phanto.Phanto.GetAllProductTypesCallback): void;

                        /**
                         * Calls GetAllProductTypes.
                         * @param request GetAllProductTypesReq message or plain object
                         * @returns Promise
                         */
                        public getAllProductTypes(request: code.justin.tv.commerce.phanto.IGetAllProductTypesReq): Promise<code.justin.tv.commerce.phanto.GetAllProductTypesResp>;

                        /**
                         * Calls GetProducts.
                         * @param request GetProductsReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetProductsResp
                         */
                        public getProducts(request: code.justin.tv.commerce.phanto.IGetProductsReq, callback: code.justin.tv.commerce.phanto.Phanto.GetProductsCallback): void;

                        /**
                         * Calls GetProducts.
                         * @param request GetProductsReq message or plain object
                         * @returns Promise
                         */
                        public getProducts(request: code.justin.tv.commerce.phanto.IGetProductsReq): Promise<code.justin.tv.commerce.phanto.GetProductsResp>;

                        /**
                         * Calls GetKeyPoolReports.
                         * @param request GetKeyPoolReportsReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyPoolReportsResp
                         */
                        public getKeyPoolReports(request: code.justin.tv.commerce.phanto.IGetKeyPoolReportsReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyPoolReportsCallback): void;

                        /**
                         * Calls GetKeyPoolReports.
                         * @param request GetKeyPoolReportsReq message or plain object
                         * @returns Promise
                         */
                        public getKeyPoolReports(request: code.justin.tv.commerce.phanto.IGetKeyPoolReportsReq): Promise<code.justin.tv.commerce.phanto.GetKeyPoolReportsResp>;

                        /**
                         * Calls GenerateKeyPoolReport.
                         * @param request GenerateKeyPoolReportReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GenerateKeyPoolReportResp
                         */
                        public generateKeyPoolReport(request: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportReq, callback: code.justin.tv.commerce.phanto.Phanto.GenerateKeyPoolReportCallback): void;

                        /**
                         * Calls GenerateKeyPoolReport.
                         * @param request GenerateKeyPoolReportReq message or plain object
                         * @returns Promise
                         */
                        public generateKeyPoolReport(request: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportReq): Promise<code.justin.tv.commerce.phanto.GenerateKeyPoolReportResp>;

                        /**
                         * Calls GetKeyPoolReportDownloadInfo.
                         * @param request GetKeyPoolReportDownloadInfoReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyPoolReportDownloadInfoResp
                         */
                        public getKeyPoolReportDownloadInfo(request: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyPoolReportDownloadInfoCallback): void;

                        /**
                         * Calls GetKeyPoolReportDownloadInfo.
                         * @param request GetKeyPoolReportDownloadInfoReq message or plain object
                         * @returns Promise
                         */
                        public getKeyPoolReportDownloadInfo(request: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoReq): Promise<code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoResp>;

                        /**
                         * Calls GetKeyPoolMetadata.
                         * @param request GetKeyPoolMetadataReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyPoolMetadataResp
                         */
                        public getKeyPoolMetadata(request: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyPoolMetadataCallback): void;

                        /**
                         * Calls GetKeyPoolMetadata.
                         * @param request GetKeyPoolMetadataReq message or plain object
                         * @returns Promise
                         */
                        public getKeyPoolMetadata(request: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataReq): Promise<code.justin.tv.commerce.phanto.GetKeyPoolMetadataResp>;

                        /**
                         * Calls CreateKeyPoolMetadata.
                         * @param request CreateKeyPoolMetadataReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and CreateKeyPoolMetadataResp
                         */
                        public createKeyPoolMetadata(request: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataReq, callback: code.justin.tv.commerce.phanto.Phanto.CreateKeyPoolMetadataCallback): void;

                        /**
                         * Calls CreateKeyPoolMetadata.
                         * @param request CreateKeyPoolMetadataReq message or plain object
                         * @returns Promise
                         */
                        public createKeyPoolMetadata(request: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataReq): Promise<code.justin.tv.commerce.phanto.CreateKeyPoolMetadataResp>;

                        /**
                         * Calls DeleteKeyPoolMetadata.
                         * @param request DeleteKeyPoolMetadataReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and DeleteKeyPoolMetadataResp
                         */
                        public deleteKeyPoolMetadata(request: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataReq, callback: code.justin.tv.commerce.phanto.Phanto.DeleteKeyPoolMetadataCallback): void;

                        /**
                         * Calls DeleteKeyPoolMetadata.
                         * @param request DeleteKeyPoolMetadataReq message or plain object
                         * @returns Promise
                         */
                        public deleteKeyPoolMetadata(request: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataReq): Promise<code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataResp>;

                        /**
                         * Calls UpdateKeyPool.
                         * @param request UpdateKeyPoolReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and UpdateKeyPoolResp
                         */
                        public updateKeyPool(request: code.justin.tv.commerce.phanto.IUpdateKeyPoolReq, callback: code.justin.tv.commerce.phanto.Phanto.UpdateKeyPoolCallback): void;

                        /**
                         * Calls UpdateKeyPool.
                         * @param request UpdateKeyPoolReq message or plain object
                         * @returns Promise
                         */
                        public updateKeyPool(request: code.justin.tv.commerce.phanto.IUpdateKeyPoolReq): Promise<code.justin.tv.commerce.phanto.UpdateKeyPoolResp>;

                        /**
                         * Calls UpdateKeyBatch.
                         * @param request UpdateKeyBatchReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and UpdateKeyBatchResp
                         */
                        public updateKeyBatch(request: code.justin.tv.commerce.phanto.IUpdateKeyBatchReq, callback: code.justin.tv.commerce.phanto.Phanto.UpdateKeyBatchCallback): void;

                        /**
                         * Calls UpdateKeyBatch.
                         * @param request UpdateKeyBatchReq message or plain object
                         * @returns Promise
                         */
                        public updateKeyBatch(request: code.justin.tv.commerce.phanto.IUpdateKeyBatchReq): Promise<code.justin.tv.commerce.phanto.UpdateKeyBatchResp>;

                        /**
                         * Calls HelixGetKeyStatus.
                         * @param request HelixGetKeyStatusReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and HelixGetKeyStatusResp
                         */
                        public helixGetKeyStatus(request: code.justin.tv.commerce.phanto.IHelixGetKeyStatusReq, callback: code.justin.tv.commerce.phanto.Phanto.HelixGetKeyStatusCallback): void;

                        /**
                         * Calls HelixGetKeyStatus.
                         * @param request HelixGetKeyStatusReq message or plain object
                         * @returns Promise
                         */
                        public helixGetKeyStatus(request: code.justin.tv.commerce.phanto.IHelixGetKeyStatusReq): Promise<code.justin.tv.commerce.phanto.HelixGetKeyStatusResp>;

                        /**
                         * Calls HelixRedeemKey.
                         * @param request HelixRedeemKeyReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and HelixRedeemKeyResp
                         */
                        public helixRedeemKey(request: code.justin.tv.commerce.phanto.IHelixRedeemKeyReq, callback: code.justin.tv.commerce.phanto.Phanto.HelixRedeemKeyCallback): void;

                        /**
                         * Calls HelixRedeemKey.
                         * @param request HelixRedeemKeyReq message or plain object
                         * @returns Promise
                         */
                        public helixRedeemKey(request: code.justin.tv.commerce.phanto.IHelixRedeemKeyReq): Promise<code.justin.tv.commerce.phanto.HelixRedeemKeyResp>;

                        /**
                         * Calls GetKeyHandlerGroup.
                         * @param request GetKeyHandlerGroupReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyHandlerGroupResp
                         */
                        public getKeyHandlerGroup(request: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyHandlerGroupCallback): void;

                        /**
                         * Calls GetKeyHandlerGroup.
                         * @param request GetKeyHandlerGroupReq message or plain object
                         * @returns Promise
                         */
                        public getKeyHandlerGroup(request: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupReq): Promise<code.justin.tv.commerce.phanto.GetKeyHandlerGroupResp>;

                        /**
                         * Calls GetKeyHandlerGroups.
                         * @param request GetKeyHandlerGroupsReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and GetKeyHandlerGroupsResp
                         */
                        public getKeyHandlerGroups(request: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsReq, callback: code.justin.tv.commerce.phanto.Phanto.GetKeyHandlerGroupsCallback): void;

                        /**
                         * Calls GetKeyHandlerGroups.
                         * @param request GetKeyHandlerGroupsReq message or plain object
                         * @returns Promise
                         */
                        public getKeyHandlerGroups(request: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsReq): Promise<code.justin.tv.commerce.phanto.GetKeyHandlerGroupsResp>;

                        /**
                         * Calls UpdateKeyHandlerGroup.
                         * @param request UpdateKeyHandlerGroupsReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and UpdateKeyHandlerGroupsResp
                         */
                        public updateKeyHandlerGroup(request: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsReq, callback: code.justin.tv.commerce.phanto.Phanto.UpdateKeyHandlerGroupCallback): void;

                        /**
                         * Calls UpdateKeyHandlerGroup.
                         * @param request UpdateKeyHandlerGroupsReq message or plain object
                         * @returns Promise
                         */
                        public updateKeyHandlerGroup(request: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsReq): Promise<code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsResp>;

                        /**
                         * Calls HelixRedeemKeyByBatch.
                         * @param request HelixRedeemKeyByBatchReq message or plain object
                         * @param callback Node-style callback called with the error, if any, and HelixRedeemKeyByBatchResp
                         */
                        public helixRedeemKeyByBatch(request: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchReq, callback: code.justin.tv.commerce.phanto.Phanto.HelixRedeemKeyByBatchCallback): void;

                        /**
                         * Calls HelixRedeemKeyByBatch.
                         * @param request HelixRedeemKeyByBatchReq message or plain object
                         * @returns Promise
                         */
                        public helixRedeemKeyByBatch(request: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchReq): Promise<code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchResp>;
                    }

                    namespace Phanto {

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#healthCheck}.
                         * @param error Error, if any
                         * @param [response] HealthCheckResp
                         */
                        type HealthCheckCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.HealthCheckResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#createProductType}.
                         * @param error Error, if any
                         * @param [response] CreateProductTypeResp
                         */
                        type CreateProductTypeCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.CreateProductTypeResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#createProduct}.
                         * @param error Error, if any
                         * @param [response] CreateProductResp
                         */
                        type CreateProductCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.CreateProductResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#createKeyPool}.
                         * @param error Error, if any
                         * @param [response] CreateKeyPoolResp
                         */
                        type CreateKeyPoolCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.CreateKeyPoolResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#generateKeyBatch}.
                         * @param error Error, if any
                         * @param [response] GenerateKeyBatchResp
                         */
                        type GenerateKeyBatchCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GenerateKeyBatchResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyBatchStatus}.
                         * @param error Error, if any
                         * @param [response] GetKeyBatchStatusResp
                         */
                        type GetKeyBatchStatusCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyBatchStatusResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyBatchDownloadInfo}.
                         * @param error Error, if any
                         * @param [response] GetKeyBatchDownloadInfoResp
                         */
                        type GetKeyBatchDownloadInfoCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#adminGetKeyStatus}.
                         * @param error Error, if any
                         * @param [response] AdminGetKeyStatusResp
                         */
                        type AdminGetKeyStatusCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.AdminGetKeyStatusResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyStatus}.
                         * @param error Error, if any
                         * @param [response] GetKeyStatusResp
                         */
                        type GetKeyStatusCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyStatusResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#redeemKey}.
                         * @param error Error, if any
                         * @param [response] RedeemKeyResp
                         */
                        type RedeemKeyCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.RedeemKeyResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#redeemKeyForLoadTesting}.
                         * @param error Error, if any
                         * @param [response] RedeemKeyResp
                         */
                        type RedeemKeyForLoadTestingCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.RedeemKeyResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#invalidateKeys}.
                         * @param error Error, if any
                         * @param [response] InvalidateKeysResp
                         */
                        type InvalidateKeysCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.InvalidateKeysResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#invalidateKeyBatch}.
                         * @param error Error, if any
                         * @param [response] InvalidateKeyBatchResp
                         */
                        type InvalidateKeyBatchCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.InvalidateKeyBatchResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#invalidateKeyPool}.
                         * @param error Error, if any
                         * @param [response] InvalidateKeyPoolResp
                         */
                        type InvalidateKeyPoolCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.InvalidateKeyPoolResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getAllKeyPools}.
                         * @param error Error, if any
                         * @param [response] GetAllKeyPoolsResp
                         */
                        type GetAllKeyPoolsCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetAllKeyPoolsResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyBatches}.
                         * @param error Error, if any
                         * @param [response] GetKeyBatchesResp
                         */
                        type GetKeyBatchesCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyBatchesResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyBatchesS2S}.
                         * @param error Error, if any
                         * @param [response] GetKeyBatchesResp
                         */
                        type GetKeyBatchesS2SCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyBatchesResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyPoolsByHandler}.
                         * @param error Error, if any
                         * @param [response] GetKeyPoolsByHandlerResp
                         */
                        type GetKeyPoolsByHandlerCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getAllProductTypes}.
                         * @param error Error, if any
                         * @param [response] GetAllProductTypesResp
                         */
                        type GetAllProductTypesCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetAllProductTypesResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getProducts}.
                         * @param error Error, if any
                         * @param [response] GetProductsResp
                         */
                        type GetProductsCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetProductsResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyPoolReports}.
                         * @param error Error, if any
                         * @param [response] GetKeyPoolReportsResp
                         */
                        type GetKeyPoolReportsCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyPoolReportsResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#generateKeyPoolReport}.
                         * @param error Error, if any
                         * @param [response] GenerateKeyPoolReportResp
                         */
                        type GenerateKeyPoolReportCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GenerateKeyPoolReportResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyPoolReportDownloadInfo}.
                         * @param error Error, if any
                         * @param [response] GetKeyPoolReportDownloadInfoResp
                         */
                        type GetKeyPoolReportDownloadInfoCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyPoolMetadata}.
                         * @param error Error, if any
                         * @param [response] GetKeyPoolMetadataResp
                         */
                        type GetKeyPoolMetadataCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyPoolMetadataResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#createKeyPoolMetadata}.
                         * @param error Error, if any
                         * @param [response] CreateKeyPoolMetadataResp
                         */
                        type CreateKeyPoolMetadataCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.CreateKeyPoolMetadataResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#deleteKeyPoolMetadata}.
                         * @param error Error, if any
                         * @param [response] DeleteKeyPoolMetadataResp
                         */
                        type DeleteKeyPoolMetadataCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#updateKeyPool}.
                         * @param error Error, if any
                         * @param [response] UpdateKeyPoolResp
                         */
                        type UpdateKeyPoolCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.UpdateKeyPoolResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#updateKeyBatch}.
                         * @param error Error, if any
                         * @param [response] UpdateKeyBatchResp
                         */
                        type UpdateKeyBatchCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.UpdateKeyBatchResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#helixGetKeyStatus}.
                         * @param error Error, if any
                         * @param [response] HelixGetKeyStatusResp
                         */
                        type HelixGetKeyStatusCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.HelixGetKeyStatusResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#helixRedeemKey}.
                         * @param error Error, if any
                         * @param [response] HelixRedeemKeyResp
                         */
                        type HelixRedeemKeyCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.HelixRedeemKeyResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyHandlerGroup}.
                         * @param error Error, if any
                         * @param [response] GetKeyHandlerGroupResp
                         */
                        type GetKeyHandlerGroupCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyHandlerGroupResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#getKeyHandlerGroups}.
                         * @param error Error, if any
                         * @param [response] GetKeyHandlerGroupsResp
                         */
                        type GetKeyHandlerGroupsCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.GetKeyHandlerGroupsResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#updateKeyHandlerGroup}.
                         * @param error Error, if any
                         * @param [response] UpdateKeyHandlerGroupsResp
                         */
                        type UpdateKeyHandlerGroupCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsResp) => void;

                        /**
                         * Callback as used by {@link code.justin.tv.commerce.phanto.Phanto#helixRedeemKeyByBatch}.
                         * @param error Error, if any
                         * @param [response] HelixRedeemKeyByBatchResp
                         */
                        type HelixRedeemKeyByBatchCallback = (error: (Error|null), response?: code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchResp) => void;
                    }

                    /** Properties of a HealthCheckReq. */
                    interface IHealthCheckReq {
                    }

                    /** Represents a HealthCheckReq. */
                    class HealthCheckReq implements IHealthCheckReq {

                        /**
                         * Constructs a new HealthCheckReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IHealthCheckReq);

                        /**
                         * Creates a new HealthCheckReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HealthCheckReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IHealthCheckReq): code.justin.tv.commerce.phanto.HealthCheckReq;

                        /**
                         * Encodes the specified HealthCheckReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.HealthCheckReq.verify|verify} messages.
                         * @param message HealthCheckReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IHealthCheckReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HealthCheckReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.HealthCheckReq.verify|verify} messages.
                         * @param message HealthCheckReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IHealthCheckReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HealthCheckReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HealthCheckReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.HealthCheckReq;

                        /**
                         * Decodes a HealthCheckReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HealthCheckReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.HealthCheckReq;

                        /**
                         * Verifies a HealthCheckReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HealthCheckReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HealthCheckReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.HealthCheckReq;

                        /**
                         * Creates a plain object from a HealthCheckReq message. Also converts values to other types if specified.
                         * @param message HealthCheckReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.HealthCheckReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HealthCheckReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a HealthCheckResp. */
                    interface IHealthCheckResp {
                    }

                    /** Represents a HealthCheckResp. */
                    class HealthCheckResp implements IHealthCheckResp {

                        /**
                         * Constructs a new HealthCheckResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IHealthCheckResp);

                        /**
                         * Creates a new HealthCheckResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HealthCheckResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IHealthCheckResp): code.justin.tv.commerce.phanto.HealthCheckResp;

                        /**
                         * Encodes the specified HealthCheckResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.HealthCheckResp.verify|verify} messages.
                         * @param message HealthCheckResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IHealthCheckResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HealthCheckResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.HealthCheckResp.verify|verify} messages.
                         * @param message HealthCheckResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IHealthCheckResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HealthCheckResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HealthCheckResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.HealthCheckResp;

                        /**
                         * Decodes a HealthCheckResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HealthCheckResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.HealthCheckResp;

                        /**
                         * Verifies a HealthCheckResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HealthCheckResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HealthCheckResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.HealthCheckResp;

                        /**
                         * Creates a plain object from a HealthCheckResp message. Also converts values to other types if specified.
                         * @param message HealthCheckResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.HealthCheckResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HealthCheckResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GenerateKeyBatchReq. */
                    interface IGenerateKeyBatchReq {

                        /** GenerateKeyBatchReq batchId */
                        batchId?: (string|null);

                        /** GenerateKeyBatchReq poolId */
                        poolId?: (string|null);

                        /** GenerateKeyBatchReq numberOfKeys */
                        numberOfKeys?: (number|Long|null);
                    }

                    /** Represents a GenerateKeyBatchReq. */
                    class GenerateKeyBatchReq implements IGenerateKeyBatchReq {

                        /**
                         * Constructs a new GenerateKeyBatchReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGenerateKeyBatchReq);

                        /** GenerateKeyBatchReq batchId. */
                        public batchId: string;

                        /** GenerateKeyBatchReq poolId. */
                        public poolId: string;

                        /** GenerateKeyBatchReq numberOfKeys. */
                        public numberOfKeys: (number|Long);

                        /**
                         * Creates a new GenerateKeyBatchReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GenerateKeyBatchReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGenerateKeyBatchReq): code.justin.tv.commerce.phanto.GenerateKeyBatchReq;

                        /**
                         * Encodes the specified GenerateKeyBatchReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GenerateKeyBatchReq.verify|verify} messages.
                         * @param message GenerateKeyBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGenerateKeyBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GenerateKeyBatchReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GenerateKeyBatchReq.verify|verify} messages.
                         * @param message GenerateKeyBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGenerateKeyBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GenerateKeyBatchReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GenerateKeyBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GenerateKeyBatchReq;

                        /**
                         * Decodes a GenerateKeyBatchReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GenerateKeyBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GenerateKeyBatchReq;

                        /**
                         * Verifies a GenerateKeyBatchReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GenerateKeyBatchReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GenerateKeyBatchReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GenerateKeyBatchReq;

                        /**
                         * Creates a plain object from a GenerateKeyBatchReq message. Also converts values to other types if specified.
                         * @param message GenerateKeyBatchReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GenerateKeyBatchReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GenerateKeyBatchReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GenerateKeyBatchResp. */
                    interface IGenerateKeyBatchResp {

                        /** GenerateKeyBatchResp batchId */
                        batchId?: (string|null);
                    }

                    /** Represents a GenerateKeyBatchResp. */
                    class GenerateKeyBatchResp implements IGenerateKeyBatchResp {

                        /**
                         * Constructs a new GenerateKeyBatchResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGenerateKeyBatchResp);

                        /** GenerateKeyBatchResp batchId. */
                        public batchId: string;

                        /**
                         * Creates a new GenerateKeyBatchResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GenerateKeyBatchResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGenerateKeyBatchResp): code.justin.tv.commerce.phanto.GenerateKeyBatchResp;

                        /**
                         * Encodes the specified GenerateKeyBatchResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GenerateKeyBatchResp.verify|verify} messages.
                         * @param message GenerateKeyBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGenerateKeyBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GenerateKeyBatchResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GenerateKeyBatchResp.verify|verify} messages.
                         * @param message GenerateKeyBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGenerateKeyBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GenerateKeyBatchResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GenerateKeyBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GenerateKeyBatchResp;

                        /**
                         * Decodes a GenerateKeyBatchResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GenerateKeyBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GenerateKeyBatchResp;

                        /**
                         * Verifies a GenerateKeyBatchResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GenerateKeyBatchResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GenerateKeyBatchResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GenerateKeyBatchResp;

                        /**
                         * Creates a plain object from a GenerateKeyBatchResp message. Also converts values to other types if specified.
                         * @param message GenerateKeyBatchResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GenerateKeyBatchResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GenerateKeyBatchResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyBatchStatusReq. */
                    interface IGetKeyBatchStatusReq {

                        /** GetKeyBatchStatusReq batchId */
                        batchId?: (string|null);
                    }

                    /** Represents a GetKeyBatchStatusReq. */
                    class GetKeyBatchStatusReq implements IGetKeyBatchStatusReq {

                        /**
                         * Constructs a new GetKeyBatchStatusReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchStatusReq);

                        /** GetKeyBatchStatusReq batchId. */
                        public batchId: string;

                        /**
                         * Creates a new GetKeyBatchStatusReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyBatchStatusReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchStatusReq): code.justin.tv.commerce.phanto.GetKeyBatchStatusReq;

                        /**
                         * Encodes the specified GetKeyBatchStatusReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchStatusReq.verify|verify} messages.
                         * @param message GetKeyBatchStatusReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyBatchStatusReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyBatchStatusReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchStatusReq.verify|verify} messages.
                         * @param message GetKeyBatchStatusReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyBatchStatusReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyBatchStatusReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyBatchStatusReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyBatchStatusReq;

                        /**
                         * Decodes a GetKeyBatchStatusReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyBatchStatusReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyBatchStatusReq;

                        /**
                         * Verifies a GetKeyBatchStatusReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyBatchStatusReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyBatchStatusReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyBatchStatusReq;

                        /**
                         * Creates a plain object from a GetKeyBatchStatusReq message. Also converts values to other types if specified.
                         * @param message GetKeyBatchStatusReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyBatchStatusReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyBatchStatusReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyBatchStatusResp. */
                    interface IGetKeyBatchStatusResp {

                        /** GetKeyBatchStatusResp status */
                        status?: (string|null);

                        /** GetKeyBatchStatusResp workflowStatus */
                        workflowStatus?: (string|null);

                        /** GetKeyBatchStatusResp progress */
                        progress?: (number|null);
                    }

                    /** Represents a GetKeyBatchStatusResp. */
                    class GetKeyBatchStatusResp implements IGetKeyBatchStatusResp {

                        /**
                         * Constructs a new GetKeyBatchStatusResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchStatusResp);

                        /** GetKeyBatchStatusResp status. */
                        public status: string;

                        /** GetKeyBatchStatusResp workflowStatus. */
                        public workflowStatus: string;

                        /** GetKeyBatchStatusResp progress. */
                        public progress: number;

                        /**
                         * Creates a new GetKeyBatchStatusResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyBatchStatusResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchStatusResp): code.justin.tv.commerce.phanto.GetKeyBatchStatusResp;

                        /**
                         * Encodes the specified GetKeyBatchStatusResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchStatusResp.verify|verify} messages.
                         * @param message GetKeyBatchStatusResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyBatchStatusResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyBatchStatusResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchStatusResp.verify|verify} messages.
                         * @param message GetKeyBatchStatusResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyBatchStatusResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyBatchStatusResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyBatchStatusResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyBatchStatusResp;

                        /**
                         * Decodes a GetKeyBatchStatusResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyBatchStatusResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyBatchStatusResp;

                        /**
                         * Verifies a GetKeyBatchStatusResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyBatchStatusResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyBatchStatusResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyBatchStatusResp;

                        /**
                         * Creates a plain object from a GetKeyBatchStatusResp message. Also converts values to other types if specified.
                         * @param message GetKeyBatchStatusResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyBatchStatusResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyBatchStatusResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyBatchDownloadInfoReq. */
                    interface IGetKeyBatchDownloadInfoReq {

                        /** GetKeyBatchDownloadInfoReq batchId */
                        batchId?: (string|null);
                    }

                    /** Represents a GetKeyBatchDownloadInfoReq. */
                    class GetKeyBatchDownloadInfoReq implements IGetKeyBatchDownloadInfoReq {

                        /**
                         * Constructs a new GetKeyBatchDownloadInfoReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoReq);

                        /** GetKeyBatchDownloadInfoReq batchId. */
                        public batchId: string;

                        /**
                         * Creates a new GetKeyBatchDownloadInfoReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyBatchDownloadInfoReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoReq): code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoReq;

                        /**
                         * Encodes the specified GetKeyBatchDownloadInfoReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoReq.verify|verify} messages.
                         * @param message GetKeyBatchDownloadInfoReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyBatchDownloadInfoReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoReq.verify|verify} messages.
                         * @param message GetKeyBatchDownloadInfoReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyBatchDownloadInfoReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyBatchDownloadInfoReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoReq;

                        /**
                         * Decodes a GetKeyBatchDownloadInfoReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyBatchDownloadInfoReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoReq;

                        /**
                         * Verifies a GetKeyBatchDownloadInfoReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyBatchDownloadInfoReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyBatchDownloadInfoReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoReq;

                        /**
                         * Creates a plain object from a GetKeyBatchDownloadInfoReq message. Also converts values to other types if specified.
                         * @param message GetKeyBatchDownloadInfoReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyBatchDownloadInfoReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyBatchDownloadInfoResp. */
                    interface IGetKeyBatchDownloadInfoResp {

                        /** GetKeyBatchDownloadInfoResp downloadUrl */
                        downloadUrl?: (string|null);

                        /** GetKeyBatchDownloadInfoResp decryptionKey */
                        decryptionKey?: (string|null);
                    }

                    /** Represents a GetKeyBatchDownloadInfoResp. */
                    class GetKeyBatchDownloadInfoResp implements IGetKeyBatchDownloadInfoResp {

                        /**
                         * Constructs a new GetKeyBatchDownloadInfoResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoResp);

                        /** GetKeyBatchDownloadInfoResp downloadUrl. */
                        public downloadUrl: string;

                        /** GetKeyBatchDownloadInfoResp decryptionKey. */
                        public decryptionKey: string;

                        /**
                         * Creates a new GetKeyBatchDownloadInfoResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyBatchDownloadInfoResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoResp): code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoResp;

                        /**
                         * Encodes the specified GetKeyBatchDownloadInfoResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoResp.verify|verify} messages.
                         * @param message GetKeyBatchDownloadInfoResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyBatchDownloadInfoResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoResp.verify|verify} messages.
                         * @param message GetKeyBatchDownloadInfoResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyBatchDownloadInfoResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyBatchDownloadInfoResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyBatchDownloadInfoResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoResp;

                        /**
                         * Decodes a GetKeyBatchDownloadInfoResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyBatchDownloadInfoResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoResp;

                        /**
                         * Verifies a GetKeyBatchDownloadInfoResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyBatchDownloadInfoResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyBatchDownloadInfoResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoResp;

                        /**
                         * Creates a plain object from a GetKeyBatchDownloadInfoResp message. Also converts values to other types if specified.
                         * @param message GetKeyBatchDownloadInfoResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyBatchDownloadInfoResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyBatchDownloadInfoResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an AdminGetKeyStatusReq. */
                    interface IAdminGetKeyStatusReq {

                        /** AdminGetKeyStatusReq key */
                        key?: (string|null);
                    }

                    /** Represents an AdminGetKeyStatusReq. */
                    class AdminGetKeyStatusReq implements IAdminGetKeyStatusReq {

                        /**
                         * Constructs a new AdminGetKeyStatusReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IAdminGetKeyStatusReq);

                        /** AdminGetKeyStatusReq key. */
                        public key: string;

                        /**
                         * Creates a new AdminGetKeyStatusReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns AdminGetKeyStatusReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IAdminGetKeyStatusReq): code.justin.tv.commerce.phanto.AdminGetKeyStatusReq;

                        /**
                         * Encodes the specified AdminGetKeyStatusReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.AdminGetKeyStatusReq.verify|verify} messages.
                         * @param message AdminGetKeyStatusReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IAdminGetKeyStatusReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified AdminGetKeyStatusReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.AdminGetKeyStatusReq.verify|verify} messages.
                         * @param message AdminGetKeyStatusReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IAdminGetKeyStatusReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an AdminGetKeyStatusReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns AdminGetKeyStatusReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.AdminGetKeyStatusReq;

                        /**
                         * Decodes an AdminGetKeyStatusReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns AdminGetKeyStatusReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.AdminGetKeyStatusReq;

                        /**
                         * Verifies an AdminGetKeyStatusReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an AdminGetKeyStatusReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns AdminGetKeyStatusReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.AdminGetKeyStatusReq;

                        /**
                         * Creates a plain object from an AdminGetKeyStatusReq message. Also converts values to other types if specified.
                         * @param message AdminGetKeyStatusReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.AdminGetKeyStatusReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this AdminGetKeyStatusReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an AdminGetKeyStatusResp. */
                    interface IAdminGetKeyStatusResp {

                        /** AdminGetKeyStatusResp claimedBy */
                        claimedBy?: (string|null);

                        /** AdminGetKeyStatusResp claimId */
                        claimId?: (string|null);
                    }

                    /** Represents an AdminGetKeyStatusResp. */
                    class AdminGetKeyStatusResp implements IAdminGetKeyStatusResp {

                        /**
                         * Constructs a new AdminGetKeyStatusResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IAdminGetKeyStatusResp);

                        /** AdminGetKeyStatusResp claimedBy. */
                        public claimedBy: string;

                        /** AdminGetKeyStatusResp claimId. */
                        public claimId: string;

                        /**
                         * Creates a new AdminGetKeyStatusResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns AdminGetKeyStatusResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IAdminGetKeyStatusResp): code.justin.tv.commerce.phanto.AdminGetKeyStatusResp;

                        /**
                         * Encodes the specified AdminGetKeyStatusResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.AdminGetKeyStatusResp.verify|verify} messages.
                         * @param message AdminGetKeyStatusResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IAdminGetKeyStatusResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified AdminGetKeyStatusResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.AdminGetKeyStatusResp.verify|verify} messages.
                         * @param message AdminGetKeyStatusResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IAdminGetKeyStatusResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an AdminGetKeyStatusResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns AdminGetKeyStatusResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.AdminGetKeyStatusResp;

                        /**
                         * Decodes an AdminGetKeyStatusResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns AdminGetKeyStatusResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.AdminGetKeyStatusResp;

                        /**
                         * Verifies an AdminGetKeyStatusResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an AdminGetKeyStatusResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns AdminGetKeyStatusResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.AdminGetKeyStatusResp;

                        /**
                         * Creates a plain object from an AdminGetKeyStatusResp message. Also converts values to other types if specified.
                         * @param message AdminGetKeyStatusResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.AdminGetKeyStatusResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this AdminGetKeyStatusResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyStatusReq. */
                    interface IGetKeyStatusReq {

                        /** GetKeyStatusReq key */
                        key?: (string|null);
                    }

                    /** Represents a GetKeyStatusReq. */
                    class GetKeyStatusReq implements IGetKeyStatusReq {

                        /**
                         * Constructs a new GetKeyStatusReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyStatusReq);

                        /** GetKeyStatusReq key. */
                        public key: string;

                        /**
                         * Creates a new GetKeyStatusReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyStatusReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyStatusReq): code.justin.tv.commerce.phanto.GetKeyStatusReq;

                        /**
                         * Encodes the specified GetKeyStatusReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyStatusReq.verify|verify} messages.
                         * @param message GetKeyStatusReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyStatusReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyStatusReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyStatusReq.verify|verify} messages.
                         * @param message GetKeyStatusReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyStatusReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyStatusReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyStatusReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyStatusReq;

                        /**
                         * Decodes a GetKeyStatusReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyStatusReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyStatusReq;

                        /**
                         * Verifies a GetKeyStatusReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyStatusReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyStatusReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyStatusReq;

                        /**
                         * Creates a plain object from a GetKeyStatusReq message. Also converts values to other types if specified.
                         * @param message GetKeyStatusReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyStatusReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyStatusReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyStatusResp. */
                    interface IGetKeyStatusResp {

                        /** GetKeyStatusResp productType */
                        productType?: (string|null);

                        /** GetKeyStatusResp sku */
                        sku?: (string|null);

                        /** GetKeyStatusResp description */
                        description?: (string|null);

                        /** GetKeyStatusResp batchId */
                        batchId?: (string|null);

                        /** GetKeyStatusResp poolId */
                        poolId?: (string|null);

                        /** GetKeyStatusResp status */
                        status?: (string|null);
                    }

                    /** Represents a GetKeyStatusResp. */
                    class GetKeyStatusResp implements IGetKeyStatusResp {

                        /**
                         * Constructs a new GetKeyStatusResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyStatusResp);

                        /** GetKeyStatusResp productType. */
                        public productType: string;

                        /** GetKeyStatusResp sku. */
                        public sku: string;

                        /** GetKeyStatusResp description. */
                        public description: string;

                        /** GetKeyStatusResp batchId. */
                        public batchId: string;

                        /** GetKeyStatusResp poolId. */
                        public poolId: string;

                        /** GetKeyStatusResp status. */
                        public status: string;

                        /**
                         * Creates a new GetKeyStatusResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyStatusResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyStatusResp): code.justin.tv.commerce.phanto.GetKeyStatusResp;

                        /**
                         * Encodes the specified GetKeyStatusResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyStatusResp.verify|verify} messages.
                         * @param message GetKeyStatusResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyStatusResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyStatusResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyStatusResp.verify|verify} messages.
                         * @param message GetKeyStatusResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyStatusResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyStatusResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyStatusResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyStatusResp;

                        /**
                         * Decodes a GetKeyStatusResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyStatusResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyStatusResp;

                        /**
                         * Verifies a GetKeyStatusResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyStatusResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyStatusResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyStatusResp;

                        /**
                         * Creates a plain object from a GetKeyStatusResp message. Also converts values to other types if specified.
                         * @param message GetKeyStatusResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyStatusResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyStatusResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a RedeemKeyReq. */
                    interface IRedeemKeyReq {

                        /** RedeemKeyReq key */
                        key?: (string|null);

                        /** RedeemKeyReq userId */
                        userId?: (string|null);

                        /** RedeemKeyReq clientIp */
                        clientIp?: (string|null);
                    }

                    /** Represents a RedeemKeyReq. */
                    class RedeemKeyReq implements IRedeemKeyReq {

                        /**
                         * Constructs a new RedeemKeyReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IRedeemKeyReq);

                        /** RedeemKeyReq key. */
                        public key: string;

                        /** RedeemKeyReq userId. */
                        public userId: string;

                        /** RedeemKeyReq clientIp. */
                        public clientIp: string;

                        /**
                         * Creates a new RedeemKeyReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns RedeemKeyReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IRedeemKeyReq): code.justin.tv.commerce.phanto.RedeemKeyReq;

                        /**
                         * Encodes the specified RedeemKeyReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.RedeemKeyReq.verify|verify} messages.
                         * @param message RedeemKeyReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IRedeemKeyReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified RedeemKeyReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.RedeemKeyReq.verify|verify} messages.
                         * @param message RedeemKeyReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IRedeemKeyReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a RedeemKeyReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns RedeemKeyReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.RedeemKeyReq;

                        /**
                         * Decodes a RedeemKeyReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns RedeemKeyReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.RedeemKeyReq;

                        /**
                         * Verifies a RedeemKeyReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a RedeemKeyReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns RedeemKeyReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.RedeemKeyReq;

                        /**
                         * Creates a plain object from a RedeemKeyReq message. Also converts values to other types if specified.
                         * @param message RedeemKeyReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.RedeemKeyReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this RedeemKeyReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a RedeemKeyResp. */
                    interface IRedeemKeyResp {
                    }

                    /** Represents a RedeemKeyResp. */
                    class RedeemKeyResp implements IRedeemKeyResp {

                        /**
                         * Constructs a new RedeemKeyResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IRedeemKeyResp);

                        /**
                         * Creates a new RedeemKeyResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns RedeemKeyResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IRedeemKeyResp): code.justin.tv.commerce.phanto.RedeemKeyResp;

                        /**
                         * Encodes the specified RedeemKeyResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.RedeemKeyResp.verify|verify} messages.
                         * @param message RedeemKeyResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IRedeemKeyResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified RedeemKeyResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.RedeemKeyResp.verify|verify} messages.
                         * @param message RedeemKeyResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IRedeemKeyResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a RedeemKeyResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns RedeemKeyResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.RedeemKeyResp;

                        /**
                         * Decodes a RedeemKeyResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns RedeemKeyResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.RedeemKeyResp;

                        /**
                         * Verifies a RedeemKeyResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a RedeemKeyResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns RedeemKeyResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.RedeemKeyResp;

                        /**
                         * Creates a plain object from a RedeemKeyResp message. Also converts values to other types if specified.
                         * @param message RedeemKeyResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.RedeemKeyResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this RedeemKeyResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an InvalidateKeysReq. */
                    interface IInvalidateKeysReq {

                        /** InvalidateKeysReq keys */
                        keys?: (string[]|null);
                    }

                    /** Represents an InvalidateKeysReq. */
                    class InvalidateKeysReq implements IInvalidateKeysReq {

                        /**
                         * Constructs a new InvalidateKeysReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IInvalidateKeysReq);

                        /** InvalidateKeysReq keys. */
                        public keys: string[];

                        /**
                         * Creates a new InvalidateKeysReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns InvalidateKeysReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IInvalidateKeysReq): code.justin.tv.commerce.phanto.InvalidateKeysReq;

                        /**
                         * Encodes the specified InvalidateKeysReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeysReq.verify|verify} messages.
                         * @param message InvalidateKeysReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IInvalidateKeysReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified InvalidateKeysReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeysReq.verify|verify} messages.
                         * @param message InvalidateKeysReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IInvalidateKeysReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an InvalidateKeysReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns InvalidateKeysReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.InvalidateKeysReq;

                        /**
                         * Decodes an InvalidateKeysReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns InvalidateKeysReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.InvalidateKeysReq;

                        /**
                         * Verifies an InvalidateKeysReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an InvalidateKeysReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns InvalidateKeysReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.InvalidateKeysReq;

                        /**
                         * Creates a plain object from an InvalidateKeysReq message. Also converts values to other types if specified.
                         * @param message InvalidateKeysReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.InvalidateKeysReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this InvalidateKeysReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an InvalidateKeysResp. */
                    interface IInvalidateKeysResp {
                    }

                    /** Represents an InvalidateKeysResp. */
                    class InvalidateKeysResp implements IInvalidateKeysResp {

                        /**
                         * Constructs a new InvalidateKeysResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IInvalidateKeysResp);

                        /**
                         * Creates a new InvalidateKeysResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns InvalidateKeysResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IInvalidateKeysResp): code.justin.tv.commerce.phanto.InvalidateKeysResp;

                        /**
                         * Encodes the specified InvalidateKeysResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeysResp.verify|verify} messages.
                         * @param message InvalidateKeysResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IInvalidateKeysResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified InvalidateKeysResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeysResp.verify|verify} messages.
                         * @param message InvalidateKeysResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IInvalidateKeysResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an InvalidateKeysResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns InvalidateKeysResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.InvalidateKeysResp;

                        /**
                         * Decodes an InvalidateKeysResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns InvalidateKeysResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.InvalidateKeysResp;

                        /**
                         * Verifies an InvalidateKeysResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an InvalidateKeysResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns InvalidateKeysResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.InvalidateKeysResp;

                        /**
                         * Creates a plain object from an InvalidateKeysResp message. Also converts values to other types if specified.
                         * @param message InvalidateKeysResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.InvalidateKeysResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this InvalidateKeysResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an InvalidateKeyBatchReq. */
                    interface IInvalidateKeyBatchReq {

                        /** InvalidateKeyBatchReq batchId */
                        batchId?: (string|null);
                    }

                    /** Represents an InvalidateKeyBatchReq. */
                    class InvalidateKeyBatchReq implements IInvalidateKeyBatchReq {

                        /**
                         * Constructs a new InvalidateKeyBatchReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IInvalidateKeyBatchReq);

                        /** InvalidateKeyBatchReq batchId. */
                        public batchId: string;

                        /**
                         * Creates a new InvalidateKeyBatchReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns InvalidateKeyBatchReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IInvalidateKeyBatchReq): code.justin.tv.commerce.phanto.InvalidateKeyBatchReq;

                        /**
                         * Encodes the specified InvalidateKeyBatchReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeyBatchReq.verify|verify} messages.
                         * @param message InvalidateKeyBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IInvalidateKeyBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified InvalidateKeyBatchReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeyBatchReq.verify|verify} messages.
                         * @param message InvalidateKeyBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IInvalidateKeyBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an InvalidateKeyBatchReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns InvalidateKeyBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.InvalidateKeyBatchReq;

                        /**
                         * Decodes an InvalidateKeyBatchReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns InvalidateKeyBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.InvalidateKeyBatchReq;

                        /**
                         * Verifies an InvalidateKeyBatchReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an InvalidateKeyBatchReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns InvalidateKeyBatchReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.InvalidateKeyBatchReq;

                        /**
                         * Creates a plain object from an InvalidateKeyBatchReq message. Also converts values to other types if specified.
                         * @param message InvalidateKeyBatchReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.InvalidateKeyBatchReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this InvalidateKeyBatchReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an InvalidateKeyBatchResp. */
                    interface IInvalidateKeyBatchResp {
                    }

                    /** Represents an InvalidateKeyBatchResp. */
                    class InvalidateKeyBatchResp implements IInvalidateKeyBatchResp {

                        /**
                         * Constructs a new InvalidateKeyBatchResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IInvalidateKeyBatchResp);

                        /**
                         * Creates a new InvalidateKeyBatchResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns InvalidateKeyBatchResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IInvalidateKeyBatchResp): code.justin.tv.commerce.phanto.InvalidateKeyBatchResp;

                        /**
                         * Encodes the specified InvalidateKeyBatchResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeyBatchResp.verify|verify} messages.
                         * @param message InvalidateKeyBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IInvalidateKeyBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified InvalidateKeyBatchResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeyBatchResp.verify|verify} messages.
                         * @param message InvalidateKeyBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IInvalidateKeyBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an InvalidateKeyBatchResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns InvalidateKeyBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.InvalidateKeyBatchResp;

                        /**
                         * Decodes an InvalidateKeyBatchResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns InvalidateKeyBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.InvalidateKeyBatchResp;

                        /**
                         * Verifies an InvalidateKeyBatchResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an InvalidateKeyBatchResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns InvalidateKeyBatchResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.InvalidateKeyBatchResp;

                        /**
                         * Creates a plain object from an InvalidateKeyBatchResp message. Also converts values to other types if specified.
                         * @param message InvalidateKeyBatchResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.InvalidateKeyBatchResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this InvalidateKeyBatchResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an InvalidateKeyPoolReq. */
                    interface IInvalidateKeyPoolReq {

                        /** InvalidateKeyPoolReq poolId */
                        poolId?: (string|null);
                    }

                    /** Represents an InvalidateKeyPoolReq. */
                    class InvalidateKeyPoolReq implements IInvalidateKeyPoolReq {

                        /**
                         * Constructs a new InvalidateKeyPoolReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IInvalidateKeyPoolReq);

                        /** InvalidateKeyPoolReq poolId. */
                        public poolId: string;

                        /**
                         * Creates a new InvalidateKeyPoolReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns InvalidateKeyPoolReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IInvalidateKeyPoolReq): code.justin.tv.commerce.phanto.InvalidateKeyPoolReq;

                        /**
                         * Encodes the specified InvalidateKeyPoolReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeyPoolReq.verify|verify} messages.
                         * @param message InvalidateKeyPoolReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IInvalidateKeyPoolReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified InvalidateKeyPoolReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeyPoolReq.verify|verify} messages.
                         * @param message InvalidateKeyPoolReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IInvalidateKeyPoolReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an InvalidateKeyPoolReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns InvalidateKeyPoolReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.InvalidateKeyPoolReq;

                        /**
                         * Decodes an InvalidateKeyPoolReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns InvalidateKeyPoolReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.InvalidateKeyPoolReq;

                        /**
                         * Verifies an InvalidateKeyPoolReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an InvalidateKeyPoolReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns InvalidateKeyPoolReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.InvalidateKeyPoolReq;

                        /**
                         * Creates a plain object from an InvalidateKeyPoolReq message. Also converts values to other types if specified.
                         * @param message InvalidateKeyPoolReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.InvalidateKeyPoolReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this InvalidateKeyPoolReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an InvalidateKeyPoolResp. */
                    interface IInvalidateKeyPoolResp {
                    }

                    /** Represents an InvalidateKeyPoolResp. */
                    class InvalidateKeyPoolResp implements IInvalidateKeyPoolResp {

                        /**
                         * Constructs a new InvalidateKeyPoolResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IInvalidateKeyPoolResp);

                        /**
                         * Creates a new InvalidateKeyPoolResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns InvalidateKeyPoolResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IInvalidateKeyPoolResp): code.justin.tv.commerce.phanto.InvalidateKeyPoolResp;

                        /**
                         * Encodes the specified InvalidateKeyPoolResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeyPoolResp.verify|verify} messages.
                         * @param message InvalidateKeyPoolResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IInvalidateKeyPoolResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified InvalidateKeyPoolResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.InvalidateKeyPoolResp.verify|verify} messages.
                         * @param message InvalidateKeyPoolResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IInvalidateKeyPoolResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an InvalidateKeyPoolResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns InvalidateKeyPoolResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.InvalidateKeyPoolResp;

                        /**
                         * Decodes an InvalidateKeyPoolResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns InvalidateKeyPoolResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.InvalidateKeyPoolResp;

                        /**
                         * Verifies an InvalidateKeyPoolResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an InvalidateKeyPoolResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns InvalidateKeyPoolResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.InvalidateKeyPoolResp;

                        /**
                         * Creates a plain object from an InvalidateKeyPoolResp message. Also converts values to other types if specified.
                         * @param message InvalidateKeyPoolResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.InvalidateKeyPoolResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this InvalidateKeyPoolResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a CreateProductTypeReq. */
                    interface ICreateProductTypeReq {

                        /** CreateProductTypeReq productType */
                        productType?: (string|null);

                        /** CreateProductTypeReq snsTopic */
                        snsTopic?: (string|null);

                        /** CreateProductTypeReq description */
                        description?: (string|null);

                        /** CreateProductTypeReq tenantUrl */
                        tenantUrl?: (string|null);
                    }

                    /** Represents a CreateProductTypeReq. */
                    class CreateProductTypeReq implements ICreateProductTypeReq {

                        /**
                         * Constructs a new CreateProductTypeReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.ICreateProductTypeReq);

                        /** CreateProductTypeReq productType. */
                        public productType: string;

                        /** CreateProductTypeReq snsTopic. */
                        public snsTopic: string;

                        /** CreateProductTypeReq description. */
                        public description: string;

                        /** CreateProductTypeReq tenantUrl. */
                        public tenantUrl: string;

                        /**
                         * Creates a new CreateProductTypeReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns CreateProductTypeReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.ICreateProductTypeReq): code.justin.tv.commerce.phanto.CreateProductTypeReq;

                        /**
                         * Encodes the specified CreateProductTypeReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateProductTypeReq.verify|verify} messages.
                         * @param message CreateProductTypeReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.ICreateProductTypeReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified CreateProductTypeReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateProductTypeReq.verify|verify} messages.
                         * @param message CreateProductTypeReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.ICreateProductTypeReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a CreateProductTypeReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns CreateProductTypeReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.CreateProductTypeReq;

                        /**
                         * Decodes a CreateProductTypeReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns CreateProductTypeReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.CreateProductTypeReq;

                        /**
                         * Verifies a CreateProductTypeReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a CreateProductTypeReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns CreateProductTypeReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.CreateProductTypeReq;

                        /**
                         * Creates a plain object from a CreateProductTypeReq message. Also converts values to other types if specified.
                         * @param message CreateProductTypeReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.CreateProductTypeReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this CreateProductTypeReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a CreateProductTypeResp. */
                    interface ICreateProductTypeResp {
                    }

                    /** Represents a CreateProductTypeResp. */
                    class CreateProductTypeResp implements ICreateProductTypeResp {

                        /**
                         * Constructs a new CreateProductTypeResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.ICreateProductTypeResp);

                        /**
                         * Creates a new CreateProductTypeResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns CreateProductTypeResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.ICreateProductTypeResp): code.justin.tv.commerce.phanto.CreateProductTypeResp;

                        /**
                         * Encodes the specified CreateProductTypeResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateProductTypeResp.verify|verify} messages.
                         * @param message CreateProductTypeResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.ICreateProductTypeResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified CreateProductTypeResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateProductTypeResp.verify|verify} messages.
                         * @param message CreateProductTypeResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.ICreateProductTypeResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a CreateProductTypeResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns CreateProductTypeResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.CreateProductTypeResp;

                        /**
                         * Decodes a CreateProductTypeResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns CreateProductTypeResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.CreateProductTypeResp;

                        /**
                         * Verifies a CreateProductTypeResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a CreateProductTypeResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns CreateProductTypeResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.CreateProductTypeResp;

                        /**
                         * Creates a plain object from a CreateProductTypeResp message. Also converts values to other types if specified.
                         * @param message CreateProductTypeResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.CreateProductTypeResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this CreateProductTypeResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a CreateProductReq. */
                    interface ICreateProductReq {

                        /** CreateProductReq productType */
                        productType?: (string|null);

                        /** CreateProductReq sku */
                        sku?: (string|null);

                        /** CreateProductReq description */
                        description?: (string|null);
                    }

                    /** Represents a CreateProductReq. */
                    class CreateProductReq implements ICreateProductReq {

                        /**
                         * Constructs a new CreateProductReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.ICreateProductReq);

                        /** CreateProductReq productType. */
                        public productType: string;

                        /** CreateProductReq sku. */
                        public sku: string;

                        /** CreateProductReq description. */
                        public description: string;

                        /**
                         * Creates a new CreateProductReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns CreateProductReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.ICreateProductReq): code.justin.tv.commerce.phanto.CreateProductReq;

                        /**
                         * Encodes the specified CreateProductReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateProductReq.verify|verify} messages.
                         * @param message CreateProductReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.ICreateProductReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified CreateProductReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateProductReq.verify|verify} messages.
                         * @param message CreateProductReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.ICreateProductReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a CreateProductReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns CreateProductReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.CreateProductReq;

                        /**
                         * Decodes a CreateProductReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns CreateProductReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.CreateProductReq;

                        /**
                         * Verifies a CreateProductReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a CreateProductReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns CreateProductReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.CreateProductReq;

                        /**
                         * Creates a plain object from a CreateProductReq message. Also converts values to other types if specified.
                         * @param message CreateProductReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.CreateProductReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this CreateProductReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a CreateProductResp. */
                    interface ICreateProductResp {
                    }

                    /** Represents a CreateProductResp. */
                    class CreateProductResp implements ICreateProductResp {

                        /**
                         * Constructs a new CreateProductResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.ICreateProductResp);

                        /**
                         * Creates a new CreateProductResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns CreateProductResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.ICreateProductResp): code.justin.tv.commerce.phanto.CreateProductResp;

                        /**
                         * Encodes the specified CreateProductResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateProductResp.verify|verify} messages.
                         * @param message CreateProductResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.ICreateProductResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified CreateProductResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateProductResp.verify|verify} messages.
                         * @param message CreateProductResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.ICreateProductResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a CreateProductResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns CreateProductResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.CreateProductResp;

                        /**
                         * Decodes a CreateProductResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns CreateProductResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.CreateProductResp;

                        /**
                         * Verifies a CreateProductResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a CreateProductResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns CreateProductResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.CreateProductResp;

                        /**
                         * Creates a plain object from a CreateProductResp message. Also converts values to other types if specified.
                         * @param message CreateProductResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.CreateProductResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this CreateProductResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a CreateKeyPoolReq. */
                    interface ICreateKeyPoolReq {

                        /** CreateKeyPoolReq poolId */
                        poolId?: (string|null);

                        /** CreateKeyPoolReq productType */
                        productType?: (string|null);

                        /** CreateKeyPoolReq sku */
                        sku?: (string|null);

                        /** CreateKeyPoolReq description */
                        description?: (string|null);

                        /** CreateKeyPoolReq handler */
                        handler?: (string|null);

                        /** CreateKeyPoolReq startDate */
                        startDate?: (google.protobuf.ITimestamp|null);

                        /** CreateKeyPoolReq endDate */
                        endDate?: (google.protobuf.ITimestamp|null);

                        /** CreateKeyPoolReq handlerGroupId */
                        handlerGroupId?: (string|null);

                        /** CreateKeyPoolReq redemptionMode */
                        redemptionMode?: (code.justin.tv.commerce.phanto.RedemptionMode|null);
                    }

                    /** Represents a CreateKeyPoolReq. */
                    class CreateKeyPoolReq implements ICreateKeyPoolReq {

                        /**
                         * Constructs a new CreateKeyPoolReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.ICreateKeyPoolReq);

                        /** CreateKeyPoolReq poolId. */
                        public poolId: string;

                        /** CreateKeyPoolReq productType. */
                        public productType: string;

                        /** CreateKeyPoolReq sku. */
                        public sku: string;

                        /** CreateKeyPoolReq description. */
                        public description: string;

                        /** CreateKeyPoolReq handler. */
                        public handler: string;

                        /** CreateKeyPoolReq startDate. */
                        public startDate?: (google.protobuf.ITimestamp|null);

                        /** CreateKeyPoolReq endDate. */
                        public endDate?: (google.protobuf.ITimestamp|null);

                        /** CreateKeyPoolReq handlerGroupId. */
                        public handlerGroupId: string;

                        /** CreateKeyPoolReq redemptionMode. */
                        public redemptionMode: code.justin.tv.commerce.phanto.RedemptionMode;

                        /**
                         * Creates a new CreateKeyPoolReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns CreateKeyPoolReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.ICreateKeyPoolReq): code.justin.tv.commerce.phanto.CreateKeyPoolReq;

                        /**
                         * Encodes the specified CreateKeyPoolReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateKeyPoolReq.verify|verify} messages.
                         * @param message CreateKeyPoolReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.ICreateKeyPoolReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified CreateKeyPoolReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateKeyPoolReq.verify|verify} messages.
                         * @param message CreateKeyPoolReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.ICreateKeyPoolReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a CreateKeyPoolReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns CreateKeyPoolReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.CreateKeyPoolReq;

                        /**
                         * Decodes a CreateKeyPoolReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns CreateKeyPoolReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.CreateKeyPoolReq;

                        /**
                         * Verifies a CreateKeyPoolReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a CreateKeyPoolReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns CreateKeyPoolReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.CreateKeyPoolReq;

                        /**
                         * Creates a plain object from a CreateKeyPoolReq message. Also converts values to other types if specified.
                         * @param message CreateKeyPoolReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.CreateKeyPoolReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this CreateKeyPoolReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** RedemptionMode enum. */
                    enum RedemptionMode {
                        REDEEM_BY_KEY = 0,
                        REDEEM_BY_BATCH_ID = 1
                    }

                    /** Properties of a CreateKeyPoolResp. */
                    interface ICreateKeyPoolResp {
                    }

                    /** Represents a CreateKeyPoolResp. */
                    class CreateKeyPoolResp implements ICreateKeyPoolResp {

                        /**
                         * Constructs a new CreateKeyPoolResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.ICreateKeyPoolResp);

                        /**
                         * Creates a new CreateKeyPoolResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns CreateKeyPoolResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.ICreateKeyPoolResp): code.justin.tv.commerce.phanto.CreateKeyPoolResp;

                        /**
                         * Encodes the specified CreateKeyPoolResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateKeyPoolResp.verify|verify} messages.
                         * @param message CreateKeyPoolResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.ICreateKeyPoolResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified CreateKeyPoolResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateKeyPoolResp.verify|verify} messages.
                         * @param message CreateKeyPoolResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.ICreateKeyPoolResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a CreateKeyPoolResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns CreateKeyPoolResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.CreateKeyPoolResp;

                        /**
                         * Decodes a CreateKeyPoolResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns CreateKeyPoolResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.CreateKeyPoolResp;

                        /**
                         * Verifies a CreateKeyPoolResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a CreateKeyPoolResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns CreateKeyPoolResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.CreateKeyPoolResp;

                        /**
                         * Creates a plain object from a CreateKeyPoolResp message. Also converts values to other types if specified.
                         * @param message CreateKeyPoolResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.CreateKeyPoolResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this CreateKeyPoolResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a KeyPool. */
                    interface IKeyPool {

                        /** KeyPool productType */
                        productType?: (string|null);

                        /** KeyPool sku */
                        sku?: (string|null);

                        /** KeyPool description */
                        description?: (string|null);

                        /** KeyPool poolId */
                        poolId?: (string|null);

                        /** KeyPool status */
                        status?: (string|null);

                        /** KeyPool handler */
                        handler?: (string|null);

                        /** KeyPool startDate */
                        startDate?: (google.protobuf.ITimestamp|null);

                        /** KeyPool endDate */
                        endDate?: (google.protobuf.ITimestamp|null);

                        /** KeyPool handlerGroupId */
                        handlerGroupId?: (string|null);

                        /** KeyPool redemptionMode */
                        redemptionMode?: (code.justin.tv.commerce.phanto.RedemptionMode|null);
                    }

                    /** Represents a KeyPool. */
                    class KeyPool implements IKeyPool {

                        /**
                         * Constructs a new KeyPool.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IKeyPool);

                        /** KeyPool productType. */
                        public productType: string;

                        /** KeyPool sku. */
                        public sku: string;

                        /** KeyPool description. */
                        public description: string;

                        /** KeyPool poolId. */
                        public poolId: string;

                        /** KeyPool status. */
                        public status: string;

                        /** KeyPool handler. */
                        public handler: string;

                        /** KeyPool startDate. */
                        public startDate?: (google.protobuf.ITimestamp|null);

                        /** KeyPool endDate. */
                        public endDate?: (google.protobuf.ITimestamp|null);

                        /** KeyPool handlerGroupId. */
                        public handlerGroupId: string;

                        /** KeyPool redemptionMode. */
                        public redemptionMode: code.justin.tv.commerce.phanto.RedemptionMode;

                        /**
                         * Creates a new KeyPool instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns KeyPool instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IKeyPool): code.justin.tv.commerce.phanto.KeyPool;

                        /**
                         * Encodes the specified KeyPool message. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyPool.verify|verify} messages.
                         * @param message KeyPool message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IKeyPool, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified KeyPool message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyPool.verify|verify} messages.
                         * @param message KeyPool message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IKeyPool, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a KeyPool message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns KeyPool
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.KeyPool;

                        /**
                         * Decodes a KeyPool message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns KeyPool
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.KeyPool;

                        /**
                         * Verifies a KeyPool message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a KeyPool message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns KeyPool
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.KeyPool;

                        /**
                         * Creates a plain object from a KeyPool message. Also converts values to other types if specified.
                         * @param message KeyPool
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.KeyPool, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this KeyPool to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a KeyBatch. */
                    interface IKeyBatch {

                        /** KeyBatch poolId */
                        poolId?: (string|null);

                        /** KeyBatch batchId */
                        batchId?: (string|null);

                        /** KeyBatch numberOfKeys */
                        numberOfKeys?: (number|Long|null);

                        /** KeyBatch status */
                        status?: (string|null);

                        /** KeyBatch workflowStatus */
                        workflowStatus?: (string|null);
                    }

                    /** Represents a KeyBatch. */
                    class KeyBatch implements IKeyBatch {

                        /**
                         * Constructs a new KeyBatch.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IKeyBatch);

                        /** KeyBatch poolId. */
                        public poolId: string;

                        /** KeyBatch batchId. */
                        public batchId: string;

                        /** KeyBatch numberOfKeys. */
                        public numberOfKeys: (number|Long);

                        /** KeyBatch status. */
                        public status: string;

                        /** KeyBatch workflowStatus. */
                        public workflowStatus: string;

                        /**
                         * Creates a new KeyBatch instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns KeyBatch instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IKeyBatch): code.justin.tv.commerce.phanto.KeyBatch;

                        /**
                         * Encodes the specified KeyBatch message. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyBatch.verify|verify} messages.
                         * @param message KeyBatch message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IKeyBatch, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified KeyBatch message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyBatch.verify|verify} messages.
                         * @param message KeyBatch message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IKeyBatch, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a KeyBatch message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns KeyBatch
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.KeyBatch;

                        /**
                         * Decodes a KeyBatch message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns KeyBatch
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.KeyBatch;

                        /**
                         * Verifies a KeyBatch message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a KeyBatch message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns KeyBatch
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.KeyBatch;

                        /**
                         * Creates a plain object from a KeyBatch message. Also converts values to other types if specified.
                         * @param message KeyBatch
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.KeyBatch, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this KeyBatch to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetAllKeyPoolsReq. */
                    interface IGetAllKeyPoolsReq {

                        /** GetAllKeyPoolsReq cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetAllKeyPoolsReq. */
                    class GetAllKeyPoolsReq implements IGetAllKeyPoolsReq {

                        /**
                         * Constructs a new GetAllKeyPoolsReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetAllKeyPoolsReq);

                        /** GetAllKeyPoolsReq cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetAllKeyPoolsReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetAllKeyPoolsReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetAllKeyPoolsReq): code.justin.tv.commerce.phanto.GetAllKeyPoolsReq;

                        /**
                         * Encodes the specified GetAllKeyPoolsReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetAllKeyPoolsReq.verify|verify} messages.
                         * @param message GetAllKeyPoolsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetAllKeyPoolsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetAllKeyPoolsReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetAllKeyPoolsReq.verify|verify} messages.
                         * @param message GetAllKeyPoolsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetAllKeyPoolsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetAllKeyPoolsReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetAllKeyPoolsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetAllKeyPoolsReq;

                        /**
                         * Decodes a GetAllKeyPoolsReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetAllKeyPoolsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetAllKeyPoolsReq;

                        /**
                         * Verifies a GetAllKeyPoolsReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetAllKeyPoolsReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetAllKeyPoolsReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetAllKeyPoolsReq;

                        /**
                         * Creates a plain object from a GetAllKeyPoolsReq message. Also converts values to other types if specified.
                         * @param message GetAllKeyPoolsReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetAllKeyPoolsReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetAllKeyPoolsReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetAllKeyPoolsResp. */
                    interface IGetAllKeyPoolsResp {

                        /** GetAllKeyPoolsResp keyPools */
                        keyPools?: (code.justin.tv.commerce.phanto.IKeyPool[]|null);

                        /** GetAllKeyPoolsResp cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetAllKeyPoolsResp. */
                    class GetAllKeyPoolsResp implements IGetAllKeyPoolsResp {

                        /**
                         * Constructs a new GetAllKeyPoolsResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetAllKeyPoolsResp);

                        /** GetAllKeyPoolsResp keyPools. */
                        public keyPools: code.justin.tv.commerce.phanto.IKeyPool[];

                        /** GetAllKeyPoolsResp cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetAllKeyPoolsResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetAllKeyPoolsResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetAllKeyPoolsResp): code.justin.tv.commerce.phanto.GetAllKeyPoolsResp;

                        /**
                         * Encodes the specified GetAllKeyPoolsResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetAllKeyPoolsResp.verify|verify} messages.
                         * @param message GetAllKeyPoolsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetAllKeyPoolsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetAllKeyPoolsResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetAllKeyPoolsResp.verify|verify} messages.
                         * @param message GetAllKeyPoolsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetAllKeyPoolsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetAllKeyPoolsResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetAllKeyPoolsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetAllKeyPoolsResp;

                        /**
                         * Decodes a GetAllKeyPoolsResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetAllKeyPoolsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetAllKeyPoolsResp;

                        /**
                         * Verifies a GetAllKeyPoolsResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetAllKeyPoolsResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetAllKeyPoolsResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetAllKeyPoolsResp;

                        /**
                         * Creates a plain object from a GetAllKeyPoolsResp message. Also converts values to other types if specified.
                         * @param message GetAllKeyPoolsResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetAllKeyPoolsResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetAllKeyPoolsResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyBatchesReq. */
                    interface IGetKeyBatchesReq {

                        /** GetKeyBatchesReq poolId */
                        poolId?: (string|null);

                        /** GetKeyBatchesReq cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyBatchesReq. */
                    class GetKeyBatchesReq implements IGetKeyBatchesReq {

                        /**
                         * Constructs a new GetKeyBatchesReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchesReq);

                        /** GetKeyBatchesReq poolId. */
                        public poolId: string;

                        /** GetKeyBatchesReq cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyBatchesReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyBatchesReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchesReq): code.justin.tv.commerce.phanto.GetKeyBatchesReq;

                        /**
                         * Encodes the specified GetKeyBatchesReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchesReq.verify|verify} messages.
                         * @param message GetKeyBatchesReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyBatchesReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyBatchesReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchesReq.verify|verify} messages.
                         * @param message GetKeyBatchesReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyBatchesReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyBatchesReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyBatchesReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyBatchesReq;

                        /**
                         * Decodes a GetKeyBatchesReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyBatchesReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyBatchesReq;

                        /**
                         * Verifies a GetKeyBatchesReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyBatchesReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyBatchesReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyBatchesReq;

                        /**
                         * Creates a plain object from a GetKeyBatchesReq message. Also converts values to other types if specified.
                         * @param message GetKeyBatchesReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyBatchesReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyBatchesReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyBatchesResp. */
                    interface IGetKeyBatchesResp {

                        /** GetKeyBatchesResp keyBatches */
                        keyBatches?: (code.justin.tv.commerce.phanto.IKeyBatch[]|null);

                        /** GetKeyBatchesResp cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyBatchesResp. */
                    class GetKeyBatchesResp implements IGetKeyBatchesResp {

                        /**
                         * Constructs a new GetKeyBatchesResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchesResp);

                        /** GetKeyBatchesResp keyBatches. */
                        public keyBatches: code.justin.tv.commerce.phanto.IKeyBatch[];

                        /** GetKeyBatchesResp cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyBatchesResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyBatchesResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyBatchesResp): code.justin.tv.commerce.phanto.GetKeyBatchesResp;

                        /**
                         * Encodes the specified GetKeyBatchesResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchesResp.verify|verify} messages.
                         * @param message GetKeyBatchesResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyBatchesResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyBatchesResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyBatchesResp.verify|verify} messages.
                         * @param message GetKeyBatchesResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyBatchesResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyBatchesResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyBatchesResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyBatchesResp;

                        /**
                         * Decodes a GetKeyBatchesResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyBatchesResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyBatchesResp;

                        /**
                         * Verifies a GetKeyBatchesResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyBatchesResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyBatchesResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyBatchesResp;

                        /**
                         * Creates a plain object from a GetKeyBatchesResp message. Also converts values to other types if specified.
                         * @param message GetKeyBatchesResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyBatchesResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyBatchesResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyPoolsByHandlerReq. */
                    interface IGetKeyPoolsByHandlerReq {

                        /** GetKeyPoolsByHandlerReq handler */
                        handler?: (string|null);

                        /** GetKeyPoolsByHandlerReq cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyPoolsByHandlerReq. */
                    class GetKeyPoolsByHandlerReq implements IGetKeyPoolsByHandlerReq {

                        /**
                         * Constructs a new GetKeyPoolsByHandlerReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerReq);

                        /** GetKeyPoolsByHandlerReq handler. */
                        public handler: string;

                        /** GetKeyPoolsByHandlerReq cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyPoolsByHandlerReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyPoolsByHandlerReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerReq): code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerReq;

                        /**
                         * Encodes the specified GetKeyPoolsByHandlerReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerReq.verify|verify} messages.
                         * @param message GetKeyPoolsByHandlerReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyPoolsByHandlerReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerReq.verify|verify} messages.
                         * @param message GetKeyPoolsByHandlerReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyPoolsByHandlerReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyPoolsByHandlerReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerReq;

                        /**
                         * Decodes a GetKeyPoolsByHandlerReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyPoolsByHandlerReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerReq;

                        /**
                         * Verifies a GetKeyPoolsByHandlerReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyPoolsByHandlerReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyPoolsByHandlerReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerReq;

                        /**
                         * Creates a plain object from a GetKeyPoolsByHandlerReq message. Also converts values to other types if specified.
                         * @param message GetKeyPoolsByHandlerReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyPoolsByHandlerReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyPoolsByHandlerResp. */
                    interface IGetKeyPoolsByHandlerResp {

                        /** GetKeyPoolsByHandlerResp keyPools */
                        keyPools?: (code.justin.tv.commerce.phanto.IKeyPool[]|null);

                        /** GetKeyPoolsByHandlerResp cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyPoolsByHandlerResp. */
                    class GetKeyPoolsByHandlerResp implements IGetKeyPoolsByHandlerResp {

                        /**
                         * Constructs a new GetKeyPoolsByHandlerResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerResp);

                        /** GetKeyPoolsByHandlerResp keyPools. */
                        public keyPools: code.justin.tv.commerce.phanto.IKeyPool[];

                        /** GetKeyPoolsByHandlerResp cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyPoolsByHandlerResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyPoolsByHandlerResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerResp): code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerResp;

                        /**
                         * Encodes the specified GetKeyPoolsByHandlerResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerResp.verify|verify} messages.
                         * @param message GetKeyPoolsByHandlerResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyPoolsByHandlerResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerResp.verify|verify} messages.
                         * @param message GetKeyPoolsByHandlerResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyPoolsByHandlerResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyPoolsByHandlerResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyPoolsByHandlerResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerResp;

                        /**
                         * Decodes a GetKeyPoolsByHandlerResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyPoolsByHandlerResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerResp;

                        /**
                         * Verifies a GetKeyPoolsByHandlerResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyPoolsByHandlerResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyPoolsByHandlerResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerResp;

                        /**
                         * Creates a plain object from a GetKeyPoolsByHandlerResp message. Also converts values to other types if specified.
                         * @param message GetKeyPoolsByHandlerResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyPoolsByHandlerResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyPoolsByHandlerResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a ProductType. */
                    interface IProductType {

                        /** ProductType productType */
                        productType?: (string|null);

                        /** ProductType snsTopic */
                        snsTopic?: (string|null);

                        /** ProductType description */
                        description?: (string|null);

                        /** ProductType tenantUrl */
                        tenantUrl?: (string|null);
                    }

                    /** Represents a ProductType. */
                    class ProductType implements IProductType {

                        /**
                         * Constructs a new ProductType.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IProductType);

                        /** ProductType productType. */
                        public productType: string;

                        /** ProductType snsTopic. */
                        public snsTopic: string;

                        /** ProductType description. */
                        public description: string;

                        /** ProductType tenantUrl. */
                        public tenantUrl: string;

                        /**
                         * Creates a new ProductType instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns ProductType instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IProductType): code.justin.tv.commerce.phanto.ProductType;

                        /**
                         * Encodes the specified ProductType message. Does not implicitly {@link code.justin.tv.commerce.phanto.ProductType.verify|verify} messages.
                         * @param message ProductType message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IProductType, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified ProductType message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.ProductType.verify|verify} messages.
                         * @param message ProductType message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IProductType, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a ProductType message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns ProductType
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.ProductType;

                        /**
                         * Decodes a ProductType message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns ProductType
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.ProductType;

                        /**
                         * Verifies a ProductType message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a ProductType message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns ProductType
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.ProductType;

                        /**
                         * Creates a plain object from a ProductType message. Also converts values to other types if specified.
                         * @param message ProductType
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.ProductType, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this ProductType to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetAllProductTypesReq. */
                    interface IGetAllProductTypesReq {

                        /** GetAllProductTypesReq cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetAllProductTypesReq. */
                    class GetAllProductTypesReq implements IGetAllProductTypesReq {

                        /**
                         * Constructs a new GetAllProductTypesReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetAllProductTypesReq);

                        /** GetAllProductTypesReq cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetAllProductTypesReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetAllProductTypesReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetAllProductTypesReq): code.justin.tv.commerce.phanto.GetAllProductTypesReq;

                        /**
                         * Encodes the specified GetAllProductTypesReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetAllProductTypesReq.verify|verify} messages.
                         * @param message GetAllProductTypesReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetAllProductTypesReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetAllProductTypesReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetAllProductTypesReq.verify|verify} messages.
                         * @param message GetAllProductTypesReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetAllProductTypesReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetAllProductTypesReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetAllProductTypesReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetAllProductTypesReq;

                        /**
                         * Decodes a GetAllProductTypesReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetAllProductTypesReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetAllProductTypesReq;

                        /**
                         * Verifies a GetAllProductTypesReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetAllProductTypesReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetAllProductTypesReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetAllProductTypesReq;

                        /**
                         * Creates a plain object from a GetAllProductTypesReq message. Also converts values to other types if specified.
                         * @param message GetAllProductTypesReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetAllProductTypesReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetAllProductTypesReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetAllProductTypesResp. */
                    interface IGetAllProductTypesResp {

                        /** GetAllProductTypesResp productTypes */
                        productTypes?: (code.justin.tv.commerce.phanto.IProductType[]|null);

                        /** GetAllProductTypesResp cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetAllProductTypesResp. */
                    class GetAllProductTypesResp implements IGetAllProductTypesResp {

                        /**
                         * Constructs a new GetAllProductTypesResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetAllProductTypesResp);

                        /** GetAllProductTypesResp productTypes. */
                        public productTypes: code.justin.tv.commerce.phanto.IProductType[];

                        /** GetAllProductTypesResp cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetAllProductTypesResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetAllProductTypesResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetAllProductTypesResp): code.justin.tv.commerce.phanto.GetAllProductTypesResp;

                        /**
                         * Encodes the specified GetAllProductTypesResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetAllProductTypesResp.verify|verify} messages.
                         * @param message GetAllProductTypesResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetAllProductTypesResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetAllProductTypesResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetAllProductTypesResp.verify|verify} messages.
                         * @param message GetAllProductTypesResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetAllProductTypesResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetAllProductTypesResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetAllProductTypesResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetAllProductTypesResp;

                        /**
                         * Decodes a GetAllProductTypesResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetAllProductTypesResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetAllProductTypesResp;

                        /**
                         * Verifies a GetAllProductTypesResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetAllProductTypesResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetAllProductTypesResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetAllProductTypesResp;

                        /**
                         * Creates a plain object from a GetAllProductTypesResp message. Also converts values to other types if specified.
                         * @param message GetAllProductTypesResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetAllProductTypesResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetAllProductTypesResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a Product. */
                    interface IProduct {

                        /** Product productType */
                        productType?: (string|null);

                        /** Product sku */
                        sku?: (string|null);

                        /** Product description */
                        description?: (string|null);
                    }

                    /** Represents a Product. */
                    class Product implements IProduct {

                        /**
                         * Constructs a new Product.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IProduct);

                        /** Product productType. */
                        public productType: string;

                        /** Product sku. */
                        public sku: string;

                        /** Product description. */
                        public description: string;

                        /**
                         * Creates a new Product instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns Product instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IProduct): code.justin.tv.commerce.phanto.Product;

                        /**
                         * Encodes the specified Product message. Does not implicitly {@link code.justin.tv.commerce.phanto.Product.verify|verify} messages.
                         * @param message Product message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IProduct, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified Product message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.Product.verify|verify} messages.
                         * @param message Product message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IProduct, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a Product message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns Product
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.Product;

                        /**
                         * Decodes a Product message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns Product
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.Product;

                        /**
                         * Verifies a Product message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a Product message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns Product
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.Product;

                        /**
                         * Creates a plain object from a Product message. Also converts values to other types if specified.
                         * @param message Product
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.Product, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this Product to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetProductsReq. */
                    interface IGetProductsReq {

                        /** GetProductsReq productType */
                        productType?: (string|null);

                        /** GetProductsReq cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetProductsReq. */
                    class GetProductsReq implements IGetProductsReq {

                        /**
                         * Constructs a new GetProductsReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetProductsReq);

                        /** GetProductsReq productType. */
                        public productType: string;

                        /** GetProductsReq cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetProductsReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetProductsReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetProductsReq): code.justin.tv.commerce.phanto.GetProductsReq;

                        /**
                         * Encodes the specified GetProductsReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetProductsReq.verify|verify} messages.
                         * @param message GetProductsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetProductsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetProductsReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetProductsReq.verify|verify} messages.
                         * @param message GetProductsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetProductsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetProductsReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetProductsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetProductsReq;

                        /**
                         * Decodes a GetProductsReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetProductsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetProductsReq;

                        /**
                         * Verifies a GetProductsReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetProductsReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetProductsReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetProductsReq;

                        /**
                         * Creates a plain object from a GetProductsReq message. Also converts values to other types if specified.
                         * @param message GetProductsReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetProductsReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetProductsReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetProductsResp. */
                    interface IGetProductsResp {

                        /** GetProductsResp products */
                        products?: (code.justin.tv.commerce.phanto.IProduct[]|null);

                        /** GetProductsResp cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetProductsResp. */
                    class GetProductsResp implements IGetProductsResp {

                        /**
                         * Constructs a new GetProductsResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetProductsResp);

                        /** GetProductsResp products. */
                        public products: code.justin.tv.commerce.phanto.IProduct[];

                        /** GetProductsResp cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetProductsResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetProductsResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetProductsResp): code.justin.tv.commerce.phanto.GetProductsResp;

                        /**
                         * Encodes the specified GetProductsResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetProductsResp.verify|verify} messages.
                         * @param message GetProductsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetProductsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetProductsResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetProductsResp.verify|verify} messages.
                         * @param message GetProductsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetProductsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetProductsResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetProductsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetProductsResp;

                        /**
                         * Decodes a GetProductsResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetProductsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetProductsResp;

                        /**
                         * Verifies a GetProductsResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetProductsResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetProductsResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetProductsResp;

                        /**
                         * Creates a plain object from a GetProductsResp message. Also converts values to other types if specified.
                         * @param message GetProductsResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetProductsResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetProductsResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyPoolReportsReq. */
                    interface IGetKeyPoolReportsReq {

                        /** GetKeyPoolReportsReq poolId */
                        poolId?: (string|null);

                        /** GetKeyPoolReportsReq cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyPoolReportsReq. */
                    class GetKeyPoolReportsReq implements IGetKeyPoolReportsReq {

                        /**
                         * Constructs a new GetKeyPoolReportsReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolReportsReq);

                        /** GetKeyPoolReportsReq poolId. */
                        public poolId: string;

                        /** GetKeyPoolReportsReq cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyPoolReportsReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyPoolReportsReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolReportsReq): code.justin.tv.commerce.phanto.GetKeyPoolReportsReq;

                        /**
                         * Encodes the specified GetKeyPoolReportsReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolReportsReq.verify|verify} messages.
                         * @param message GetKeyPoolReportsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyPoolReportsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyPoolReportsReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolReportsReq.verify|verify} messages.
                         * @param message GetKeyPoolReportsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyPoolReportsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyPoolReportsReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyPoolReportsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyPoolReportsReq;

                        /**
                         * Decodes a GetKeyPoolReportsReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyPoolReportsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyPoolReportsReq;

                        /**
                         * Verifies a GetKeyPoolReportsReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyPoolReportsReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyPoolReportsReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyPoolReportsReq;

                        /**
                         * Creates a plain object from a GetKeyPoolReportsReq message. Also converts values to other types if specified.
                         * @param message GetKeyPoolReportsReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyPoolReportsReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyPoolReportsReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyPoolReportsResp. */
                    interface IGetKeyPoolReportsResp {

                        /** GetKeyPoolReportsResp keyPoolReports */
                        keyPoolReports?: (code.justin.tv.commerce.phanto.IKeyPoolReport[]|null);

                        /** GetKeyPoolReportsResp cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyPoolReportsResp. */
                    class GetKeyPoolReportsResp implements IGetKeyPoolReportsResp {

                        /**
                         * Constructs a new GetKeyPoolReportsResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolReportsResp);

                        /** GetKeyPoolReportsResp keyPoolReports. */
                        public keyPoolReports: code.justin.tv.commerce.phanto.IKeyPoolReport[];

                        /** GetKeyPoolReportsResp cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyPoolReportsResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyPoolReportsResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolReportsResp): code.justin.tv.commerce.phanto.GetKeyPoolReportsResp;

                        /**
                         * Encodes the specified GetKeyPoolReportsResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolReportsResp.verify|verify} messages.
                         * @param message GetKeyPoolReportsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyPoolReportsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyPoolReportsResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolReportsResp.verify|verify} messages.
                         * @param message GetKeyPoolReportsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyPoolReportsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyPoolReportsResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyPoolReportsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyPoolReportsResp;

                        /**
                         * Decodes a GetKeyPoolReportsResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyPoolReportsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyPoolReportsResp;

                        /**
                         * Verifies a GetKeyPoolReportsResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyPoolReportsResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyPoolReportsResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyPoolReportsResp;

                        /**
                         * Creates a plain object from a GetKeyPoolReportsResp message. Also converts values to other types if specified.
                         * @param message GetKeyPoolReportsResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyPoolReportsResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyPoolReportsResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a KeyPoolReport. */
                    interface IKeyPoolReport {

                        /** KeyPoolReport poolId */
                        poolId?: (string|null);

                        /** KeyPoolReport reportId */
                        reportId?: (string|null);

                        /** KeyPoolReport requestedAt */
                        requestedAt?: (google.protobuf.ITimestamp|null);

                        /** KeyPoolReport requestedBy */
                        requestedBy?: (string|null);

                        /** KeyPoolReport status */
                        status?: (string|null);
                    }

                    /** Represents a KeyPoolReport. */
                    class KeyPoolReport implements IKeyPoolReport {

                        /**
                         * Constructs a new KeyPoolReport.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IKeyPoolReport);

                        /** KeyPoolReport poolId. */
                        public poolId: string;

                        /** KeyPoolReport reportId. */
                        public reportId: string;

                        /** KeyPoolReport requestedAt. */
                        public requestedAt?: (google.protobuf.ITimestamp|null);

                        /** KeyPoolReport requestedBy. */
                        public requestedBy: string;

                        /** KeyPoolReport status. */
                        public status: string;

                        /**
                         * Creates a new KeyPoolReport instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns KeyPoolReport instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IKeyPoolReport): code.justin.tv.commerce.phanto.KeyPoolReport;

                        /**
                         * Encodes the specified KeyPoolReport message. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyPoolReport.verify|verify} messages.
                         * @param message KeyPoolReport message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IKeyPoolReport, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified KeyPoolReport message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyPoolReport.verify|verify} messages.
                         * @param message KeyPoolReport message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IKeyPoolReport, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a KeyPoolReport message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns KeyPoolReport
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.KeyPoolReport;

                        /**
                         * Decodes a KeyPoolReport message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns KeyPoolReport
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.KeyPoolReport;

                        /**
                         * Verifies a KeyPoolReport message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a KeyPoolReport message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns KeyPoolReport
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.KeyPoolReport;

                        /**
                         * Creates a plain object from a KeyPoolReport message. Also converts values to other types if specified.
                         * @param message KeyPoolReport
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.KeyPoolReport, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this KeyPoolReport to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GenerateKeyPoolReportReq. */
                    interface IGenerateKeyPoolReportReq {

                        /** GenerateKeyPoolReportReq poolId */
                        poolId?: (string|null);
                    }

                    /** Represents a GenerateKeyPoolReportReq. */
                    class GenerateKeyPoolReportReq implements IGenerateKeyPoolReportReq {

                        /**
                         * Constructs a new GenerateKeyPoolReportReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportReq);

                        /** GenerateKeyPoolReportReq poolId. */
                        public poolId: string;

                        /**
                         * Creates a new GenerateKeyPoolReportReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GenerateKeyPoolReportReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportReq): code.justin.tv.commerce.phanto.GenerateKeyPoolReportReq;

                        /**
                         * Encodes the specified GenerateKeyPoolReportReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GenerateKeyPoolReportReq.verify|verify} messages.
                         * @param message GenerateKeyPoolReportReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GenerateKeyPoolReportReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GenerateKeyPoolReportReq.verify|verify} messages.
                         * @param message GenerateKeyPoolReportReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GenerateKeyPoolReportReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GenerateKeyPoolReportReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GenerateKeyPoolReportReq;

                        /**
                         * Decodes a GenerateKeyPoolReportReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GenerateKeyPoolReportReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GenerateKeyPoolReportReq;

                        /**
                         * Verifies a GenerateKeyPoolReportReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GenerateKeyPoolReportReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GenerateKeyPoolReportReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GenerateKeyPoolReportReq;

                        /**
                         * Creates a plain object from a GenerateKeyPoolReportReq message. Also converts values to other types if specified.
                         * @param message GenerateKeyPoolReportReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GenerateKeyPoolReportReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GenerateKeyPoolReportReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GenerateKeyPoolReportResp. */
                    interface IGenerateKeyPoolReportResp {

                        /** GenerateKeyPoolReportResp poolId */
                        poolId?: (string|null);

                        /** GenerateKeyPoolReportResp reportId */
                        reportId?: (string|null);
                    }

                    /** Represents a GenerateKeyPoolReportResp. */
                    class GenerateKeyPoolReportResp implements IGenerateKeyPoolReportResp {

                        /**
                         * Constructs a new GenerateKeyPoolReportResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportResp);

                        /** GenerateKeyPoolReportResp poolId. */
                        public poolId: string;

                        /** GenerateKeyPoolReportResp reportId. */
                        public reportId: string;

                        /**
                         * Creates a new GenerateKeyPoolReportResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GenerateKeyPoolReportResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportResp): code.justin.tv.commerce.phanto.GenerateKeyPoolReportResp;

                        /**
                         * Encodes the specified GenerateKeyPoolReportResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GenerateKeyPoolReportResp.verify|verify} messages.
                         * @param message GenerateKeyPoolReportResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GenerateKeyPoolReportResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GenerateKeyPoolReportResp.verify|verify} messages.
                         * @param message GenerateKeyPoolReportResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGenerateKeyPoolReportResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GenerateKeyPoolReportResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GenerateKeyPoolReportResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GenerateKeyPoolReportResp;

                        /**
                         * Decodes a GenerateKeyPoolReportResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GenerateKeyPoolReportResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GenerateKeyPoolReportResp;

                        /**
                         * Verifies a GenerateKeyPoolReportResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GenerateKeyPoolReportResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GenerateKeyPoolReportResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GenerateKeyPoolReportResp;

                        /**
                         * Creates a plain object from a GenerateKeyPoolReportResp message. Also converts values to other types if specified.
                         * @param message GenerateKeyPoolReportResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GenerateKeyPoolReportResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GenerateKeyPoolReportResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyPoolReportDownloadInfoReq. */
                    interface IGetKeyPoolReportDownloadInfoReq {

                        /** GetKeyPoolReportDownloadInfoReq reportId */
                        reportId?: (string|null);
                    }

                    /** Represents a GetKeyPoolReportDownloadInfoReq. */
                    class GetKeyPoolReportDownloadInfoReq implements IGetKeyPoolReportDownloadInfoReq {

                        /**
                         * Constructs a new GetKeyPoolReportDownloadInfoReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoReq);

                        /** GetKeyPoolReportDownloadInfoReq reportId. */
                        public reportId: string;

                        /**
                         * Creates a new GetKeyPoolReportDownloadInfoReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyPoolReportDownloadInfoReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoReq): code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoReq;

                        /**
                         * Encodes the specified GetKeyPoolReportDownloadInfoReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoReq.verify|verify} messages.
                         * @param message GetKeyPoolReportDownloadInfoReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyPoolReportDownloadInfoReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoReq.verify|verify} messages.
                         * @param message GetKeyPoolReportDownloadInfoReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyPoolReportDownloadInfoReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyPoolReportDownloadInfoReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoReq;

                        /**
                         * Decodes a GetKeyPoolReportDownloadInfoReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyPoolReportDownloadInfoReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoReq;

                        /**
                         * Verifies a GetKeyPoolReportDownloadInfoReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyPoolReportDownloadInfoReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyPoolReportDownloadInfoReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoReq;

                        /**
                         * Creates a plain object from a GetKeyPoolReportDownloadInfoReq message. Also converts values to other types if specified.
                         * @param message GetKeyPoolReportDownloadInfoReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyPoolReportDownloadInfoReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyPoolReportDownloadInfoResp. */
                    interface IGetKeyPoolReportDownloadInfoResp {

                        /** GetKeyPoolReportDownloadInfoResp downloadUrl */
                        downloadUrl?: (string|null);

                        /** GetKeyPoolReportDownloadInfoResp decryptionKey */
                        decryptionKey?: (string|null);
                    }

                    /** Represents a GetKeyPoolReportDownloadInfoResp. */
                    class GetKeyPoolReportDownloadInfoResp implements IGetKeyPoolReportDownloadInfoResp {

                        /**
                         * Constructs a new GetKeyPoolReportDownloadInfoResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoResp);

                        /** GetKeyPoolReportDownloadInfoResp downloadUrl. */
                        public downloadUrl: string;

                        /** GetKeyPoolReportDownloadInfoResp decryptionKey. */
                        public decryptionKey: string;

                        /**
                         * Creates a new GetKeyPoolReportDownloadInfoResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyPoolReportDownloadInfoResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoResp): code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoResp;

                        /**
                         * Encodes the specified GetKeyPoolReportDownloadInfoResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoResp.verify|verify} messages.
                         * @param message GetKeyPoolReportDownloadInfoResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyPoolReportDownloadInfoResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoResp.verify|verify} messages.
                         * @param message GetKeyPoolReportDownloadInfoResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyPoolReportDownloadInfoResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyPoolReportDownloadInfoResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyPoolReportDownloadInfoResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoResp;

                        /**
                         * Decodes a GetKeyPoolReportDownloadInfoResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyPoolReportDownloadInfoResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoResp;

                        /**
                         * Verifies a GetKeyPoolReportDownloadInfoResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyPoolReportDownloadInfoResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyPoolReportDownloadInfoResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoResp;

                        /**
                         * Creates a plain object from a GetKeyPoolReportDownloadInfoResp message. Also converts values to other types if specified.
                         * @param message GetKeyPoolReportDownloadInfoResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyPoolReportDownloadInfoResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyPoolReportDownloadInfoResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyPoolMetadataReq. */
                    interface IGetKeyPoolMetadataReq {

                        /** GetKeyPoolMetadataReq poolId */
                        poolId?: (string|null);

                        /** GetKeyPoolMetadataReq cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyPoolMetadataReq. */
                    class GetKeyPoolMetadataReq implements IGetKeyPoolMetadataReq {

                        /**
                         * Constructs a new GetKeyPoolMetadataReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataReq);

                        /** GetKeyPoolMetadataReq poolId. */
                        public poolId: string;

                        /** GetKeyPoolMetadataReq cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyPoolMetadataReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyPoolMetadataReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataReq): code.justin.tv.commerce.phanto.GetKeyPoolMetadataReq;

                        /**
                         * Encodes the specified GetKeyPoolMetadataReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolMetadataReq.verify|verify} messages.
                         * @param message GetKeyPoolMetadataReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyPoolMetadataReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolMetadataReq.verify|verify} messages.
                         * @param message GetKeyPoolMetadataReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyPoolMetadataReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyPoolMetadataReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyPoolMetadataReq;

                        /**
                         * Decodes a GetKeyPoolMetadataReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyPoolMetadataReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyPoolMetadataReq;

                        /**
                         * Verifies a GetKeyPoolMetadataReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyPoolMetadataReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyPoolMetadataReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyPoolMetadataReq;

                        /**
                         * Creates a plain object from a GetKeyPoolMetadataReq message. Also converts values to other types if specified.
                         * @param message GetKeyPoolMetadataReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyPoolMetadataReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyPoolMetadataReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a KeyPoolMetadata. */
                    interface IKeyPoolMetadata {

                        /** KeyPoolMetadata metadataKey */
                        metadataKey?: (string|null);

                        /** KeyPoolMetadata metadataValue */
                        metadataValue?: (string|null);
                    }

                    /** Represents a KeyPoolMetadata. */
                    class KeyPoolMetadata implements IKeyPoolMetadata {

                        /**
                         * Constructs a new KeyPoolMetadata.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IKeyPoolMetadata);

                        /** KeyPoolMetadata metadataKey. */
                        public metadataKey: string;

                        /** KeyPoolMetadata metadataValue. */
                        public metadataValue: string;

                        /**
                         * Creates a new KeyPoolMetadata instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns KeyPoolMetadata instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IKeyPoolMetadata): code.justin.tv.commerce.phanto.KeyPoolMetadata;

                        /**
                         * Encodes the specified KeyPoolMetadata message. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyPoolMetadata.verify|verify} messages.
                         * @param message KeyPoolMetadata message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IKeyPoolMetadata, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified KeyPoolMetadata message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyPoolMetadata.verify|verify} messages.
                         * @param message KeyPoolMetadata message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IKeyPoolMetadata, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a KeyPoolMetadata message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns KeyPoolMetadata
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.KeyPoolMetadata;

                        /**
                         * Decodes a KeyPoolMetadata message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns KeyPoolMetadata
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.KeyPoolMetadata;

                        /**
                         * Verifies a KeyPoolMetadata message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a KeyPoolMetadata message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns KeyPoolMetadata
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.KeyPoolMetadata;

                        /**
                         * Creates a plain object from a KeyPoolMetadata message. Also converts values to other types if specified.
                         * @param message KeyPoolMetadata
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.KeyPoolMetadata, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this KeyPoolMetadata to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyPoolMetadataResp. */
                    interface IGetKeyPoolMetadataResp {

                        /** GetKeyPoolMetadataResp keyPoolMetadata */
                        keyPoolMetadata?: (code.justin.tv.commerce.phanto.IKeyPoolMetadata[]|null);

                        /** GetKeyPoolMetadataResp cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyPoolMetadataResp. */
                    class GetKeyPoolMetadataResp implements IGetKeyPoolMetadataResp {

                        /**
                         * Constructs a new GetKeyPoolMetadataResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataResp);

                        /** GetKeyPoolMetadataResp keyPoolMetadata. */
                        public keyPoolMetadata: code.justin.tv.commerce.phanto.IKeyPoolMetadata[];

                        /** GetKeyPoolMetadataResp cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyPoolMetadataResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyPoolMetadataResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataResp): code.justin.tv.commerce.phanto.GetKeyPoolMetadataResp;

                        /**
                         * Encodes the specified GetKeyPoolMetadataResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolMetadataResp.verify|verify} messages.
                         * @param message GetKeyPoolMetadataResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyPoolMetadataResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyPoolMetadataResp.verify|verify} messages.
                         * @param message GetKeyPoolMetadataResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyPoolMetadataResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyPoolMetadataResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyPoolMetadataResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyPoolMetadataResp;

                        /**
                         * Decodes a GetKeyPoolMetadataResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyPoolMetadataResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyPoolMetadataResp;

                        /**
                         * Verifies a GetKeyPoolMetadataResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyPoolMetadataResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyPoolMetadataResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyPoolMetadataResp;

                        /**
                         * Creates a plain object from a GetKeyPoolMetadataResp message. Also converts values to other types if specified.
                         * @param message GetKeyPoolMetadataResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyPoolMetadataResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyPoolMetadataResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a CreateKeyPoolMetadataReq. */
                    interface ICreateKeyPoolMetadataReq {

                        /** CreateKeyPoolMetadataReq poolId */
                        poolId?: (string|null);

                        /** CreateKeyPoolMetadataReq metadataKey */
                        metadataKey?: (string|null);

                        /** CreateKeyPoolMetadataReq metadataValue */
                        metadataValue?: (string|null);
                    }

                    /** Represents a CreateKeyPoolMetadataReq. */
                    class CreateKeyPoolMetadataReq implements ICreateKeyPoolMetadataReq {

                        /**
                         * Constructs a new CreateKeyPoolMetadataReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataReq);

                        /** CreateKeyPoolMetadataReq poolId. */
                        public poolId: string;

                        /** CreateKeyPoolMetadataReq metadataKey. */
                        public metadataKey: string;

                        /** CreateKeyPoolMetadataReq metadataValue. */
                        public metadataValue: string;

                        /**
                         * Creates a new CreateKeyPoolMetadataReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns CreateKeyPoolMetadataReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataReq): code.justin.tv.commerce.phanto.CreateKeyPoolMetadataReq;

                        /**
                         * Encodes the specified CreateKeyPoolMetadataReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateKeyPoolMetadataReq.verify|verify} messages.
                         * @param message CreateKeyPoolMetadataReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified CreateKeyPoolMetadataReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateKeyPoolMetadataReq.verify|verify} messages.
                         * @param message CreateKeyPoolMetadataReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a CreateKeyPoolMetadataReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns CreateKeyPoolMetadataReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.CreateKeyPoolMetadataReq;

                        /**
                         * Decodes a CreateKeyPoolMetadataReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns CreateKeyPoolMetadataReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.CreateKeyPoolMetadataReq;

                        /**
                         * Verifies a CreateKeyPoolMetadataReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a CreateKeyPoolMetadataReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns CreateKeyPoolMetadataReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.CreateKeyPoolMetadataReq;

                        /**
                         * Creates a plain object from a CreateKeyPoolMetadataReq message. Also converts values to other types if specified.
                         * @param message CreateKeyPoolMetadataReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.CreateKeyPoolMetadataReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this CreateKeyPoolMetadataReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a CreateKeyPoolMetadataResp. */
                    interface ICreateKeyPoolMetadataResp {
                    }

                    /** Represents a CreateKeyPoolMetadataResp. */
                    class CreateKeyPoolMetadataResp implements ICreateKeyPoolMetadataResp {

                        /**
                         * Constructs a new CreateKeyPoolMetadataResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataResp);

                        /**
                         * Creates a new CreateKeyPoolMetadataResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns CreateKeyPoolMetadataResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataResp): code.justin.tv.commerce.phanto.CreateKeyPoolMetadataResp;

                        /**
                         * Encodes the specified CreateKeyPoolMetadataResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateKeyPoolMetadataResp.verify|verify} messages.
                         * @param message CreateKeyPoolMetadataResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified CreateKeyPoolMetadataResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.CreateKeyPoolMetadataResp.verify|verify} messages.
                         * @param message CreateKeyPoolMetadataResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.ICreateKeyPoolMetadataResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a CreateKeyPoolMetadataResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns CreateKeyPoolMetadataResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.CreateKeyPoolMetadataResp;

                        /**
                         * Decodes a CreateKeyPoolMetadataResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns CreateKeyPoolMetadataResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.CreateKeyPoolMetadataResp;

                        /**
                         * Verifies a CreateKeyPoolMetadataResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a CreateKeyPoolMetadataResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns CreateKeyPoolMetadataResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.CreateKeyPoolMetadataResp;

                        /**
                         * Creates a plain object from a CreateKeyPoolMetadataResp message. Also converts values to other types if specified.
                         * @param message CreateKeyPoolMetadataResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.CreateKeyPoolMetadataResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this CreateKeyPoolMetadataResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a DeleteKeyPoolMetadataReq. */
                    interface IDeleteKeyPoolMetadataReq {

                        /** DeleteKeyPoolMetadataReq poolId */
                        poolId?: (string|null);

                        /** DeleteKeyPoolMetadataReq metadataKey */
                        metadataKey?: (string|null);
                    }

                    /** Represents a DeleteKeyPoolMetadataReq. */
                    class DeleteKeyPoolMetadataReq implements IDeleteKeyPoolMetadataReq {

                        /**
                         * Constructs a new DeleteKeyPoolMetadataReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataReq);

                        /** DeleteKeyPoolMetadataReq poolId. */
                        public poolId: string;

                        /** DeleteKeyPoolMetadataReq metadataKey. */
                        public metadataKey: string;

                        /**
                         * Creates a new DeleteKeyPoolMetadataReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns DeleteKeyPoolMetadataReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataReq): code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataReq;

                        /**
                         * Encodes the specified DeleteKeyPoolMetadataReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataReq.verify|verify} messages.
                         * @param message DeleteKeyPoolMetadataReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified DeleteKeyPoolMetadataReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataReq.verify|verify} messages.
                         * @param message DeleteKeyPoolMetadataReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a DeleteKeyPoolMetadataReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns DeleteKeyPoolMetadataReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataReq;

                        /**
                         * Decodes a DeleteKeyPoolMetadataReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns DeleteKeyPoolMetadataReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataReq;

                        /**
                         * Verifies a DeleteKeyPoolMetadataReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a DeleteKeyPoolMetadataReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns DeleteKeyPoolMetadataReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataReq;

                        /**
                         * Creates a plain object from a DeleteKeyPoolMetadataReq message. Also converts values to other types if specified.
                         * @param message DeleteKeyPoolMetadataReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this DeleteKeyPoolMetadataReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a DeleteKeyPoolMetadataResp. */
                    interface IDeleteKeyPoolMetadataResp {
                    }

                    /** Represents a DeleteKeyPoolMetadataResp. */
                    class DeleteKeyPoolMetadataResp implements IDeleteKeyPoolMetadataResp {

                        /**
                         * Constructs a new DeleteKeyPoolMetadataResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataResp);

                        /**
                         * Creates a new DeleteKeyPoolMetadataResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns DeleteKeyPoolMetadataResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataResp): code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataResp;

                        /**
                         * Encodes the specified DeleteKeyPoolMetadataResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataResp.verify|verify} messages.
                         * @param message DeleteKeyPoolMetadataResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified DeleteKeyPoolMetadataResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataResp.verify|verify} messages.
                         * @param message DeleteKeyPoolMetadataResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IDeleteKeyPoolMetadataResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a DeleteKeyPoolMetadataResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns DeleteKeyPoolMetadataResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataResp;

                        /**
                         * Decodes a DeleteKeyPoolMetadataResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns DeleteKeyPoolMetadataResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataResp;

                        /**
                         * Verifies a DeleteKeyPoolMetadataResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a DeleteKeyPoolMetadataResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns DeleteKeyPoolMetadataResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataResp;

                        /**
                         * Creates a plain object from a DeleteKeyPoolMetadataResp message. Also converts values to other types if specified.
                         * @param message DeleteKeyPoolMetadataResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.DeleteKeyPoolMetadataResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this DeleteKeyPoolMetadataResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an UpdateKeyPoolReq. */
                    interface IUpdateKeyPoolReq {

                        /** UpdateKeyPoolReq poolId */
                        poolId?: (string|null);

                        /** UpdateKeyPoolReq status */
                        status?: (string|null);

                        /** UpdateKeyPoolReq description */
                        description?: (string|null);

                        /** UpdateKeyPoolReq startDate */
                        startDate?: (google.protobuf.ITimestamp|null);

                        /** UpdateKeyPoolReq endDate */
                        endDate?: (google.protobuf.ITimestamp|null);
                    }

                    /** Represents an UpdateKeyPoolReq. */
                    class UpdateKeyPoolReq implements IUpdateKeyPoolReq {

                        /**
                         * Constructs a new UpdateKeyPoolReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IUpdateKeyPoolReq);

                        /** UpdateKeyPoolReq poolId. */
                        public poolId: string;

                        /** UpdateKeyPoolReq status. */
                        public status: string;

                        /** UpdateKeyPoolReq description. */
                        public description: string;

                        /** UpdateKeyPoolReq startDate. */
                        public startDate?: (google.protobuf.ITimestamp|null);

                        /** UpdateKeyPoolReq endDate. */
                        public endDate?: (google.protobuf.ITimestamp|null);

                        /**
                         * Creates a new UpdateKeyPoolReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns UpdateKeyPoolReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IUpdateKeyPoolReq): code.justin.tv.commerce.phanto.UpdateKeyPoolReq;

                        /**
                         * Encodes the specified UpdateKeyPoolReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyPoolReq.verify|verify} messages.
                         * @param message UpdateKeyPoolReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IUpdateKeyPoolReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified UpdateKeyPoolReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyPoolReq.verify|verify} messages.
                         * @param message UpdateKeyPoolReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IUpdateKeyPoolReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an UpdateKeyPoolReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns UpdateKeyPoolReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.UpdateKeyPoolReq;

                        /**
                         * Decodes an UpdateKeyPoolReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns UpdateKeyPoolReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.UpdateKeyPoolReq;

                        /**
                         * Verifies an UpdateKeyPoolReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an UpdateKeyPoolReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns UpdateKeyPoolReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.UpdateKeyPoolReq;

                        /**
                         * Creates a plain object from an UpdateKeyPoolReq message. Also converts values to other types if specified.
                         * @param message UpdateKeyPoolReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.UpdateKeyPoolReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this UpdateKeyPoolReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an UpdateKeyPoolResp. */
                    interface IUpdateKeyPoolResp {
                    }

                    /** Represents an UpdateKeyPoolResp. */
                    class UpdateKeyPoolResp implements IUpdateKeyPoolResp {

                        /**
                         * Constructs a new UpdateKeyPoolResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IUpdateKeyPoolResp);

                        /**
                         * Creates a new UpdateKeyPoolResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns UpdateKeyPoolResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IUpdateKeyPoolResp): code.justin.tv.commerce.phanto.UpdateKeyPoolResp;

                        /**
                         * Encodes the specified UpdateKeyPoolResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyPoolResp.verify|verify} messages.
                         * @param message UpdateKeyPoolResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IUpdateKeyPoolResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified UpdateKeyPoolResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyPoolResp.verify|verify} messages.
                         * @param message UpdateKeyPoolResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IUpdateKeyPoolResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an UpdateKeyPoolResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns UpdateKeyPoolResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.UpdateKeyPoolResp;

                        /**
                         * Decodes an UpdateKeyPoolResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns UpdateKeyPoolResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.UpdateKeyPoolResp;

                        /**
                         * Verifies an UpdateKeyPoolResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an UpdateKeyPoolResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns UpdateKeyPoolResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.UpdateKeyPoolResp;

                        /**
                         * Creates a plain object from an UpdateKeyPoolResp message. Also converts values to other types if specified.
                         * @param message UpdateKeyPoolResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.UpdateKeyPoolResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this UpdateKeyPoolResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an UpdateKeyBatchReq. */
                    interface IUpdateKeyBatchReq {

                        /** UpdateKeyBatchReq batchId */
                        batchId?: (string|null);

                        /** UpdateKeyBatchReq status */
                        status?: (string|null);
                    }

                    /** Represents an UpdateKeyBatchReq. */
                    class UpdateKeyBatchReq implements IUpdateKeyBatchReq {

                        /**
                         * Constructs a new UpdateKeyBatchReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IUpdateKeyBatchReq);

                        /** UpdateKeyBatchReq batchId. */
                        public batchId: string;

                        /** UpdateKeyBatchReq status. */
                        public status: string;

                        /**
                         * Creates a new UpdateKeyBatchReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns UpdateKeyBatchReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IUpdateKeyBatchReq): code.justin.tv.commerce.phanto.UpdateKeyBatchReq;

                        /**
                         * Encodes the specified UpdateKeyBatchReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyBatchReq.verify|verify} messages.
                         * @param message UpdateKeyBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IUpdateKeyBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified UpdateKeyBatchReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyBatchReq.verify|verify} messages.
                         * @param message UpdateKeyBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IUpdateKeyBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an UpdateKeyBatchReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns UpdateKeyBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.UpdateKeyBatchReq;

                        /**
                         * Decodes an UpdateKeyBatchReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns UpdateKeyBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.UpdateKeyBatchReq;

                        /**
                         * Verifies an UpdateKeyBatchReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an UpdateKeyBatchReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns UpdateKeyBatchReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.UpdateKeyBatchReq;

                        /**
                         * Creates a plain object from an UpdateKeyBatchReq message. Also converts values to other types if specified.
                         * @param message UpdateKeyBatchReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.UpdateKeyBatchReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this UpdateKeyBatchReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an UpdateKeyBatchResp. */
                    interface IUpdateKeyBatchResp {
                    }

                    /** Represents an UpdateKeyBatchResp. */
                    class UpdateKeyBatchResp implements IUpdateKeyBatchResp {

                        /**
                         * Constructs a new UpdateKeyBatchResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IUpdateKeyBatchResp);

                        /**
                         * Creates a new UpdateKeyBatchResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns UpdateKeyBatchResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IUpdateKeyBatchResp): code.justin.tv.commerce.phanto.UpdateKeyBatchResp;

                        /**
                         * Encodes the specified UpdateKeyBatchResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyBatchResp.verify|verify} messages.
                         * @param message UpdateKeyBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IUpdateKeyBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified UpdateKeyBatchResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyBatchResp.verify|verify} messages.
                         * @param message UpdateKeyBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IUpdateKeyBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an UpdateKeyBatchResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns UpdateKeyBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.UpdateKeyBatchResp;

                        /**
                         * Decodes an UpdateKeyBatchResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns UpdateKeyBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.UpdateKeyBatchResp;

                        /**
                         * Verifies an UpdateKeyBatchResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an UpdateKeyBatchResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns UpdateKeyBatchResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.UpdateKeyBatchResp;

                        /**
                         * Creates a plain object from an UpdateKeyBatchResp message. Also converts values to other types if specified.
                         * @param message UpdateKeyBatchResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.UpdateKeyBatchResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this UpdateKeyBatchResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a HelixKeyResp. */
                    interface IHelixKeyResp {

                        /** HelixKeyResp key */
                        key?: (string|null);

                        /** HelixKeyResp status */
                        status?: (code.justin.tv.commerce.phanto.KeyStatus|null);
                    }

                    /** Represents a HelixKeyResp. */
                    class HelixKeyResp implements IHelixKeyResp {

                        /**
                         * Constructs a new HelixKeyResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IHelixKeyResp);

                        /** HelixKeyResp key. */
                        public key: string;

                        /** HelixKeyResp status. */
                        public status: code.justin.tv.commerce.phanto.KeyStatus;

                        /**
                         * Creates a new HelixKeyResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HelixKeyResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IHelixKeyResp): code.justin.tv.commerce.phanto.HelixKeyResp;

                        /**
                         * Encodes the specified HelixKeyResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixKeyResp.verify|verify} messages.
                         * @param message HelixKeyResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IHelixKeyResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HelixKeyResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixKeyResp.verify|verify} messages.
                         * @param message HelixKeyResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IHelixKeyResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HelixKeyResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HelixKeyResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.HelixKeyResp;

                        /**
                         * Decodes a HelixKeyResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HelixKeyResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.HelixKeyResp;

                        /**
                         * Verifies a HelixKeyResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HelixKeyResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HelixKeyResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.HelixKeyResp;

                        /**
                         * Creates a plain object from a HelixKeyResp message. Also converts values to other types if specified.
                         * @param message HelixKeyResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.HelixKeyResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HelixKeyResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a HelixGetKeyStatusReq. */
                    interface IHelixGetKeyStatusReq {

                        /** HelixGetKeyStatusReq userId */
                        userId?: (string|null);

                        /** HelixGetKeyStatusReq keys */
                        keys?: (string[]|null);
                    }

                    /** Represents a HelixGetKeyStatusReq. */
                    class HelixGetKeyStatusReq implements IHelixGetKeyStatusReq {

                        /**
                         * Constructs a new HelixGetKeyStatusReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IHelixGetKeyStatusReq);

                        /** HelixGetKeyStatusReq userId. */
                        public userId: string;

                        /** HelixGetKeyStatusReq keys. */
                        public keys: string[];

                        /**
                         * Creates a new HelixGetKeyStatusReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HelixGetKeyStatusReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IHelixGetKeyStatusReq): code.justin.tv.commerce.phanto.HelixGetKeyStatusReq;

                        /**
                         * Encodes the specified HelixGetKeyStatusReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixGetKeyStatusReq.verify|verify} messages.
                         * @param message HelixGetKeyStatusReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IHelixGetKeyStatusReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HelixGetKeyStatusReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixGetKeyStatusReq.verify|verify} messages.
                         * @param message HelixGetKeyStatusReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IHelixGetKeyStatusReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HelixGetKeyStatusReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HelixGetKeyStatusReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.HelixGetKeyStatusReq;

                        /**
                         * Decodes a HelixGetKeyStatusReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HelixGetKeyStatusReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.HelixGetKeyStatusReq;

                        /**
                         * Verifies a HelixGetKeyStatusReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HelixGetKeyStatusReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HelixGetKeyStatusReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.HelixGetKeyStatusReq;

                        /**
                         * Creates a plain object from a HelixGetKeyStatusReq message. Also converts values to other types if specified.
                         * @param message HelixGetKeyStatusReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.HelixGetKeyStatusReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HelixGetKeyStatusReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a HelixGetKeyStatusResp. */
                    interface IHelixGetKeyStatusResp {

                        /** HelixGetKeyStatusResp results */
                        results?: (code.justin.tv.commerce.phanto.IHelixKeyResp[]|null);
                    }

                    /** Represents a HelixGetKeyStatusResp. */
                    class HelixGetKeyStatusResp implements IHelixGetKeyStatusResp {

                        /**
                         * Constructs a new HelixGetKeyStatusResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IHelixGetKeyStatusResp);

                        /** HelixGetKeyStatusResp results. */
                        public results: code.justin.tv.commerce.phanto.IHelixKeyResp[];

                        /**
                         * Creates a new HelixGetKeyStatusResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HelixGetKeyStatusResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IHelixGetKeyStatusResp): code.justin.tv.commerce.phanto.HelixGetKeyStatusResp;

                        /**
                         * Encodes the specified HelixGetKeyStatusResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixGetKeyStatusResp.verify|verify} messages.
                         * @param message HelixGetKeyStatusResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IHelixGetKeyStatusResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HelixGetKeyStatusResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixGetKeyStatusResp.verify|verify} messages.
                         * @param message HelixGetKeyStatusResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IHelixGetKeyStatusResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HelixGetKeyStatusResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HelixGetKeyStatusResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.HelixGetKeyStatusResp;

                        /**
                         * Decodes a HelixGetKeyStatusResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HelixGetKeyStatusResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.HelixGetKeyStatusResp;

                        /**
                         * Verifies a HelixGetKeyStatusResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HelixGetKeyStatusResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HelixGetKeyStatusResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.HelixGetKeyStatusResp;

                        /**
                         * Creates a plain object from a HelixGetKeyStatusResp message. Also converts values to other types if specified.
                         * @param message HelixGetKeyStatusResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.HelixGetKeyStatusResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HelixGetKeyStatusResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a HelixRedeemKeyReq. */
                    interface IHelixRedeemKeyReq {

                        /** HelixRedeemKeyReq userId */
                        userId?: (string|null);

                        /** HelixRedeemKeyReq clientIp */
                        clientIp?: (string|null);

                        /** HelixRedeemKeyReq keys */
                        keys?: (string[]|null);
                    }

                    /** Represents a HelixRedeemKeyReq. */
                    class HelixRedeemKeyReq implements IHelixRedeemKeyReq {

                        /**
                         * Constructs a new HelixRedeemKeyReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IHelixRedeemKeyReq);

                        /** HelixRedeemKeyReq userId. */
                        public userId: string;

                        /** HelixRedeemKeyReq clientIp. */
                        public clientIp: string;

                        /** HelixRedeemKeyReq keys. */
                        public keys: string[];

                        /**
                         * Creates a new HelixRedeemKeyReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HelixRedeemKeyReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IHelixRedeemKeyReq): code.justin.tv.commerce.phanto.HelixRedeemKeyReq;

                        /**
                         * Encodes the specified HelixRedeemKeyReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixRedeemKeyReq.verify|verify} messages.
                         * @param message HelixRedeemKeyReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IHelixRedeemKeyReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HelixRedeemKeyReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixRedeemKeyReq.verify|verify} messages.
                         * @param message HelixRedeemKeyReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IHelixRedeemKeyReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HelixRedeemKeyReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HelixRedeemKeyReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.HelixRedeemKeyReq;

                        /**
                         * Decodes a HelixRedeemKeyReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HelixRedeemKeyReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.HelixRedeemKeyReq;

                        /**
                         * Verifies a HelixRedeemKeyReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HelixRedeemKeyReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HelixRedeemKeyReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.HelixRedeemKeyReq;

                        /**
                         * Creates a plain object from a HelixRedeemKeyReq message. Also converts values to other types if specified.
                         * @param message HelixRedeemKeyReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.HelixRedeemKeyReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HelixRedeemKeyReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a HelixRedeemKeyResp. */
                    interface IHelixRedeemKeyResp {

                        /** HelixRedeemKeyResp results */
                        results?: (code.justin.tv.commerce.phanto.IHelixKeyResp[]|null);
                    }

                    /** Represents a HelixRedeemKeyResp. */
                    class HelixRedeemKeyResp implements IHelixRedeemKeyResp {

                        /**
                         * Constructs a new HelixRedeemKeyResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IHelixRedeemKeyResp);

                        /** HelixRedeemKeyResp results. */
                        public results: code.justin.tv.commerce.phanto.IHelixKeyResp[];

                        /**
                         * Creates a new HelixRedeemKeyResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HelixRedeemKeyResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IHelixRedeemKeyResp): code.justin.tv.commerce.phanto.HelixRedeemKeyResp;

                        /**
                         * Encodes the specified HelixRedeemKeyResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixRedeemKeyResp.verify|verify} messages.
                         * @param message HelixRedeemKeyResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IHelixRedeemKeyResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HelixRedeemKeyResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixRedeemKeyResp.verify|verify} messages.
                         * @param message HelixRedeemKeyResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IHelixRedeemKeyResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HelixRedeemKeyResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HelixRedeemKeyResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.HelixRedeemKeyResp;

                        /**
                         * Decodes a HelixRedeemKeyResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HelixRedeemKeyResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.HelixRedeemKeyResp;

                        /**
                         * Verifies a HelixRedeemKeyResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HelixRedeemKeyResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HelixRedeemKeyResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.HelixRedeemKeyResp;

                        /**
                         * Creates a plain object from a HelixRedeemKeyResp message. Also converts values to other types if specified.
                         * @param message HelixRedeemKeyResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.HelixRedeemKeyResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HelixRedeemKeyResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** KeyStatus enum. */
                    enum KeyStatus {
                        SUCCESSFULLY_REDEEMED = 0,
                        ALREADY_CLAIMED = 1,
                        EXPIRED = 2,
                        USER_NOT_ELIGIBLE = 3,
                        NOT_FOUND = 4,
                        INACTIVE = 5,
                        UNUSED = 6,
                        INCORRECT_FORMAT = 7,
                        INTERNAL_ERROR = 8
                    }

                    /** Properties of a GetKeyHandlerGroupReq. */
                    interface IGetKeyHandlerGroupReq {

                        /** GetKeyHandlerGroupReq handlerGroupId */
                        handlerGroupId?: (string|null);
                    }

                    /** Represents a GetKeyHandlerGroupReq. */
                    class GetKeyHandlerGroupReq implements IGetKeyHandlerGroupReq {

                        /**
                         * Constructs a new GetKeyHandlerGroupReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupReq);

                        /** GetKeyHandlerGroupReq handlerGroupId. */
                        public handlerGroupId: string;

                        /**
                         * Creates a new GetKeyHandlerGroupReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyHandlerGroupReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupReq): code.justin.tv.commerce.phanto.GetKeyHandlerGroupReq;

                        /**
                         * Encodes the specified GetKeyHandlerGroupReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyHandlerGroupReq.verify|verify} messages.
                         * @param message GetKeyHandlerGroupReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyHandlerGroupReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyHandlerGroupReq.verify|verify} messages.
                         * @param message GetKeyHandlerGroupReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyHandlerGroupReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyHandlerGroupReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyHandlerGroupReq;

                        /**
                         * Decodes a GetKeyHandlerGroupReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyHandlerGroupReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyHandlerGroupReq;

                        /**
                         * Verifies a GetKeyHandlerGroupReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyHandlerGroupReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyHandlerGroupReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyHandlerGroupReq;

                        /**
                         * Creates a plain object from a GetKeyHandlerGroupReq message. Also converts values to other types if specified.
                         * @param message GetKeyHandlerGroupReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyHandlerGroupReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyHandlerGroupReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyHandlerGroupResp. */
                    interface IGetKeyHandlerGroupResp {

                        /** GetKeyHandlerGroupResp keyHandlerGroup */
                        keyHandlerGroup?: (code.justin.tv.commerce.phanto.IKeyHandlerGroup|null);
                    }

                    /** Represents a GetKeyHandlerGroupResp. */
                    class GetKeyHandlerGroupResp implements IGetKeyHandlerGroupResp {

                        /**
                         * Constructs a new GetKeyHandlerGroupResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupResp);

                        /** GetKeyHandlerGroupResp keyHandlerGroup. */
                        public keyHandlerGroup?: (code.justin.tv.commerce.phanto.IKeyHandlerGroup|null);

                        /**
                         * Creates a new GetKeyHandlerGroupResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyHandlerGroupResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupResp): code.justin.tv.commerce.phanto.GetKeyHandlerGroupResp;

                        /**
                         * Encodes the specified GetKeyHandlerGroupResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyHandlerGroupResp.verify|verify} messages.
                         * @param message GetKeyHandlerGroupResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyHandlerGroupResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyHandlerGroupResp.verify|verify} messages.
                         * @param message GetKeyHandlerGroupResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyHandlerGroupResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyHandlerGroupResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyHandlerGroupResp;

                        /**
                         * Decodes a GetKeyHandlerGroupResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyHandlerGroupResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyHandlerGroupResp;

                        /**
                         * Verifies a GetKeyHandlerGroupResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyHandlerGroupResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyHandlerGroupResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyHandlerGroupResp;

                        /**
                         * Creates a plain object from a GetKeyHandlerGroupResp message. Also converts values to other types if specified.
                         * @param message GetKeyHandlerGroupResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyHandlerGroupResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyHandlerGroupResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyHandlerGroupsReq. */
                    interface IGetKeyHandlerGroupsReq {

                        /** GetKeyHandlerGroupsReq cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyHandlerGroupsReq. */
                    class GetKeyHandlerGroupsReq implements IGetKeyHandlerGroupsReq {

                        /**
                         * Constructs a new GetKeyHandlerGroupsReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsReq);

                        /** GetKeyHandlerGroupsReq cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyHandlerGroupsReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyHandlerGroupsReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsReq): code.justin.tv.commerce.phanto.GetKeyHandlerGroupsReq;

                        /**
                         * Encodes the specified GetKeyHandlerGroupsReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyHandlerGroupsReq.verify|verify} messages.
                         * @param message GetKeyHandlerGroupsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyHandlerGroupsReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyHandlerGroupsReq.verify|verify} messages.
                         * @param message GetKeyHandlerGroupsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyHandlerGroupsReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyHandlerGroupsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyHandlerGroupsReq;

                        /**
                         * Decodes a GetKeyHandlerGroupsReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyHandlerGroupsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyHandlerGroupsReq;

                        /**
                         * Verifies a GetKeyHandlerGroupsReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyHandlerGroupsReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyHandlerGroupsReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyHandlerGroupsReq;

                        /**
                         * Creates a plain object from a GetKeyHandlerGroupsReq message. Also converts values to other types if specified.
                         * @param message GetKeyHandlerGroupsReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyHandlerGroupsReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyHandlerGroupsReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a GetKeyHandlerGroupsResp. */
                    interface IGetKeyHandlerGroupsResp {

                        /** GetKeyHandlerGroupsResp keyHandlerGroups */
                        keyHandlerGroups?: (code.justin.tv.commerce.phanto.IKeyHandlerGroup[]|null);

                        /** GetKeyHandlerGroupsResp cursor */
                        cursor?: (string|null);
                    }

                    /** Represents a GetKeyHandlerGroupsResp. */
                    class GetKeyHandlerGroupsResp implements IGetKeyHandlerGroupsResp {

                        /**
                         * Constructs a new GetKeyHandlerGroupsResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsResp);

                        /** GetKeyHandlerGroupsResp keyHandlerGroups. */
                        public keyHandlerGroups: code.justin.tv.commerce.phanto.IKeyHandlerGroup[];

                        /** GetKeyHandlerGroupsResp cursor. */
                        public cursor: string;

                        /**
                         * Creates a new GetKeyHandlerGroupsResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns GetKeyHandlerGroupsResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsResp): code.justin.tv.commerce.phanto.GetKeyHandlerGroupsResp;

                        /**
                         * Encodes the specified GetKeyHandlerGroupsResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyHandlerGroupsResp.verify|verify} messages.
                         * @param message GetKeyHandlerGroupsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified GetKeyHandlerGroupsResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.GetKeyHandlerGroupsResp.verify|verify} messages.
                         * @param message GetKeyHandlerGroupsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IGetKeyHandlerGroupsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a GetKeyHandlerGroupsResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns GetKeyHandlerGroupsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.GetKeyHandlerGroupsResp;

                        /**
                         * Decodes a GetKeyHandlerGroupsResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns GetKeyHandlerGroupsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.GetKeyHandlerGroupsResp;

                        /**
                         * Verifies a GetKeyHandlerGroupsResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a GetKeyHandlerGroupsResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns GetKeyHandlerGroupsResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.GetKeyHandlerGroupsResp;

                        /**
                         * Creates a plain object from a GetKeyHandlerGroupsResp message. Also converts values to other types if specified.
                         * @param message GetKeyHandlerGroupsResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.GetKeyHandlerGroupsResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this GetKeyHandlerGroupsResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a KeyHandlerGroup. */
                    interface IKeyHandlerGroup {

                        /** KeyHandlerGroup handlerGroupId */
                        handlerGroupId?: (string|null);

                        /** KeyHandlerGroup description */
                        description?: (string|null);

                        /** KeyHandlerGroup authorizedTuids */
                        authorizedTuids?: (string[]|null);

                        /** KeyHandlerGroup authorizedClientIds */
                        authorizedClientIds?: (string[]|null);
                    }

                    /** Represents a KeyHandlerGroup. */
                    class KeyHandlerGroup implements IKeyHandlerGroup {

                        /**
                         * Constructs a new KeyHandlerGroup.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IKeyHandlerGroup);

                        /** KeyHandlerGroup handlerGroupId. */
                        public handlerGroupId: string;

                        /** KeyHandlerGroup description. */
                        public description: string;

                        /** KeyHandlerGroup authorizedTuids. */
                        public authorizedTuids: string[];

                        /** KeyHandlerGroup authorizedClientIds. */
                        public authorizedClientIds: string[];

                        /**
                         * Creates a new KeyHandlerGroup instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns KeyHandlerGroup instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IKeyHandlerGroup): code.justin.tv.commerce.phanto.KeyHandlerGroup;

                        /**
                         * Encodes the specified KeyHandlerGroup message. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyHandlerGroup.verify|verify} messages.
                         * @param message KeyHandlerGroup message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IKeyHandlerGroup, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified KeyHandlerGroup message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.KeyHandlerGroup.verify|verify} messages.
                         * @param message KeyHandlerGroup message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IKeyHandlerGroup, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a KeyHandlerGroup message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns KeyHandlerGroup
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.KeyHandlerGroup;

                        /**
                         * Decodes a KeyHandlerGroup message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns KeyHandlerGroup
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.KeyHandlerGroup;

                        /**
                         * Verifies a KeyHandlerGroup message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a KeyHandlerGroup message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns KeyHandlerGroup
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.KeyHandlerGroup;

                        /**
                         * Creates a plain object from a KeyHandlerGroup message. Also converts values to other types if specified.
                         * @param message KeyHandlerGroup
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.KeyHandlerGroup, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this KeyHandlerGroup to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an UpdateKeyHandlerGroupsReq. */
                    interface IUpdateKeyHandlerGroupsReq {

                        /** UpdateKeyHandlerGroupsReq handlerGroupId */
                        handlerGroupId?: (string|null);

                        /** UpdateKeyHandlerGroupsReq description */
                        description?: (string|null);

                        /** UpdateKeyHandlerGroupsReq authorizedTuids */
                        authorizedTuids?: (string[]|null);

                        /** UpdateKeyHandlerGroupsReq authorizedClientIds */
                        authorizedClientIds?: (string[]|null);
                    }

                    /** Represents an UpdateKeyHandlerGroupsReq. */
                    class UpdateKeyHandlerGroupsReq implements IUpdateKeyHandlerGroupsReq {

                        /**
                         * Constructs a new UpdateKeyHandlerGroupsReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsReq);

                        /** UpdateKeyHandlerGroupsReq handlerGroupId. */
                        public handlerGroupId: string;

                        /** UpdateKeyHandlerGroupsReq description. */
                        public description: string;

                        /** UpdateKeyHandlerGroupsReq authorizedTuids. */
                        public authorizedTuids: string[];

                        /** UpdateKeyHandlerGroupsReq authorizedClientIds. */
                        public authorizedClientIds: string[];

                        /**
                         * Creates a new UpdateKeyHandlerGroupsReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns UpdateKeyHandlerGroupsReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsReq): code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsReq;

                        /**
                         * Encodes the specified UpdateKeyHandlerGroupsReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsReq.verify|verify} messages.
                         * @param message UpdateKeyHandlerGroupsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified UpdateKeyHandlerGroupsReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsReq.verify|verify} messages.
                         * @param message UpdateKeyHandlerGroupsReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an UpdateKeyHandlerGroupsReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns UpdateKeyHandlerGroupsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsReq;

                        /**
                         * Decodes an UpdateKeyHandlerGroupsReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns UpdateKeyHandlerGroupsReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsReq;

                        /**
                         * Verifies an UpdateKeyHandlerGroupsReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an UpdateKeyHandlerGroupsReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns UpdateKeyHandlerGroupsReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsReq;

                        /**
                         * Creates a plain object from an UpdateKeyHandlerGroupsReq message. Also converts values to other types if specified.
                         * @param message UpdateKeyHandlerGroupsReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this UpdateKeyHandlerGroupsReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of an UpdateKeyHandlerGroupsResp. */
                    interface IUpdateKeyHandlerGroupsResp {
                    }

                    /** Represents an UpdateKeyHandlerGroupsResp. */
                    class UpdateKeyHandlerGroupsResp implements IUpdateKeyHandlerGroupsResp {

                        /**
                         * Constructs a new UpdateKeyHandlerGroupsResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsResp);

                        /**
                         * Creates a new UpdateKeyHandlerGroupsResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns UpdateKeyHandlerGroupsResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsResp): code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsResp;

                        /**
                         * Encodes the specified UpdateKeyHandlerGroupsResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsResp.verify|verify} messages.
                         * @param message UpdateKeyHandlerGroupsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified UpdateKeyHandlerGroupsResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsResp.verify|verify} messages.
                         * @param message UpdateKeyHandlerGroupsResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IUpdateKeyHandlerGroupsResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes an UpdateKeyHandlerGroupsResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns UpdateKeyHandlerGroupsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsResp;

                        /**
                         * Decodes an UpdateKeyHandlerGroupsResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns UpdateKeyHandlerGroupsResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsResp;

                        /**
                         * Verifies an UpdateKeyHandlerGroupsResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates an UpdateKeyHandlerGroupsResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns UpdateKeyHandlerGroupsResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsResp;

                        /**
                         * Creates a plain object from an UpdateKeyHandlerGroupsResp message. Also converts values to other types if specified.
                         * @param message UpdateKeyHandlerGroupsResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.UpdateKeyHandlerGroupsResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this UpdateKeyHandlerGroupsResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a RedeemKeyByBatchReq. */
                    interface IRedeemKeyByBatchReq {

                        /** RedeemKeyByBatchReq userId */
                        userId?: (string|null);

                        /** RedeemKeyByBatchReq batchId */
                        batchId?: (string|null);

                        /** RedeemKeyByBatchReq userIp */
                        userIp?: (string|null);

                        /** RedeemKeyByBatchReq idempotencyKey */
                        idempotencyKey?: (string|null);
                    }

                    /** Represents a RedeemKeyByBatchReq. */
                    class RedeemKeyByBatchReq implements IRedeemKeyByBatchReq {

                        /**
                         * Constructs a new RedeemKeyByBatchReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IRedeemKeyByBatchReq);

                        /** RedeemKeyByBatchReq userId. */
                        public userId: string;

                        /** RedeemKeyByBatchReq batchId. */
                        public batchId: string;

                        /** RedeemKeyByBatchReq userIp. */
                        public userIp: string;

                        /** RedeemKeyByBatchReq idempotencyKey. */
                        public idempotencyKey: string;

                        /**
                         * Creates a new RedeemKeyByBatchReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns RedeemKeyByBatchReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IRedeemKeyByBatchReq): code.justin.tv.commerce.phanto.RedeemKeyByBatchReq;

                        /**
                         * Encodes the specified RedeemKeyByBatchReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.RedeemKeyByBatchReq.verify|verify} messages.
                         * @param message RedeemKeyByBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IRedeemKeyByBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified RedeemKeyByBatchReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.RedeemKeyByBatchReq.verify|verify} messages.
                         * @param message RedeemKeyByBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IRedeemKeyByBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a RedeemKeyByBatchReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns RedeemKeyByBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.RedeemKeyByBatchReq;

                        /**
                         * Decodes a RedeemKeyByBatchReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns RedeemKeyByBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.RedeemKeyByBatchReq;

                        /**
                         * Verifies a RedeemKeyByBatchReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a RedeemKeyByBatchReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns RedeemKeyByBatchReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.RedeemKeyByBatchReq;

                        /**
                         * Creates a plain object from a RedeemKeyByBatchReq message. Also converts values to other types if specified.
                         * @param message RedeemKeyByBatchReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.RedeemKeyByBatchReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this RedeemKeyByBatchReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a RedeemKeyByBatchResp. */
                    interface IRedeemKeyByBatchResp {
                    }

                    /** Represents a RedeemKeyByBatchResp. */
                    class RedeemKeyByBatchResp implements IRedeemKeyByBatchResp {

                        /**
                         * Constructs a new RedeemKeyByBatchResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IRedeemKeyByBatchResp);

                        /**
                         * Creates a new RedeemKeyByBatchResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns RedeemKeyByBatchResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IRedeemKeyByBatchResp): code.justin.tv.commerce.phanto.RedeemKeyByBatchResp;

                        /**
                         * Encodes the specified RedeemKeyByBatchResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.RedeemKeyByBatchResp.verify|verify} messages.
                         * @param message RedeemKeyByBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IRedeemKeyByBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified RedeemKeyByBatchResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.RedeemKeyByBatchResp.verify|verify} messages.
                         * @param message RedeemKeyByBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IRedeemKeyByBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a RedeemKeyByBatchResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns RedeemKeyByBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.RedeemKeyByBatchResp;

                        /**
                         * Decodes a RedeemKeyByBatchResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns RedeemKeyByBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.RedeemKeyByBatchResp;

                        /**
                         * Verifies a RedeemKeyByBatchResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a RedeemKeyByBatchResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns RedeemKeyByBatchResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.RedeemKeyByBatchResp;

                        /**
                         * Creates a plain object from a RedeemKeyByBatchResp message. Also converts values to other types if specified.
                         * @param message RedeemKeyByBatchResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.RedeemKeyByBatchResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this RedeemKeyByBatchResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a HelixRedeemKeyByBatchReq. */
                    interface IHelixRedeemKeyByBatchReq {

                        /** HelixRedeemKeyByBatchReq userId */
                        userId?: (string|null);

                        /** HelixRedeemKeyByBatchReq batchId */
                        batchId?: (string|null);

                        /** HelixRedeemKeyByBatchReq idempotencyKey */
                        idempotencyKey?: (string|null);
                    }

                    /** Represents a HelixRedeemKeyByBatchReq. */
                    class HelixRedeemKeyByBatchReq implements IHelixRedeemKeyByBatchReq {

                        /**
                         * Constructs a new HelixRedeemKeyByBatchReq.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchReq);

                        /** HelixRedeemKeyByBatchReq userId. */
                        public userId: string;

                        /** HelixRedeemKeyByBatchReq batchId. */
                        public batchId: string;

                        /** HelixRedeemKeyByBatchReq idempotencyKey. */
                        public idempotencyKey: string;

                        /**
                         * Creates a new HelixRedeemKeyByBatchReq instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HelixRedeemKeyByBatchReq instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchReq): code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchReq;

                        /**
                         * Encodes the specified HelixRedeemKeyByBatchReq message. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchReq.verify|verify} messages.
                         * @param message HelixRedeemKeyByBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HelixRedeemKeyByBatchReq message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchReq.verify|verify} messages.
                         * @param message HelixRedeemKeyByBatchReq message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchReq, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HelixRedeemKeyByBatchReq message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HelixRedeemKeyByBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchReq;

                        /**
                         * Decodes a HelixRedeemKeyByBatchReq message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HelixRedeemKeyByBatchReq
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchReq;

                        /**
                         * Verifies a HelixRedeemKeyByBatchReq message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HelixRedeemKeyByBatchReq message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HelixRedeemKeyByBatchReq
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchReq;

                        /**
                         * Creates a plain object from a HelixRedeemKeyByBatchReq message. Also converts values to other types if specified.
                         * @param message HelixRedeemKeyByBatchReq
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchReq, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HelixRedeemKeyByBatchReq to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }

                    /** Properties of a HelixRedeemKeyByBatchResp. */
                    interface IHelixRedeemKeyByBatchResp {

                        /** HelixRedeemKeyByBatchResp status */
                        status?: (code.justin.tv.commerce.phanto.KeyStatus|null);

                        /** HelixRedeemKeyByBatchResp keysRemaining */
                        keysRemaining?: (number|Long|null);
                    }

                    /** Represents a HelixRedeemKeyByBatchResp. */
                    class HelixRedeemKeyByBatchResp implements IHelixRedeemKeyByBatchResp {

                        /**
                         * Constructs a new HelixRedeemKeyByBatchResp.
                         * @param [properties] Properties to set
                         */
                        constructor(properties?: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchResp);

                        /** HelixRedeemKeyByBatchResp status. */
                        public status: code.justin.tv.commerce.phanto.KeyStatus;

                        /** HelixRedeemKeyByBatchResp keysRemaining. */
                        public keysRemaining: (number|Long);

                        /**
                         * Creates a new HelixRedeemKeyByBatchResp instance using the specified properties.
                         * @param [properties] Properties to set
                         * @returns HelixRedeemKeyByBatchResp instance
                         */
                        public static create(properties?: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchResp): code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchResp;

                        /**
                         * Encodes the specified HelixRedeemKeyByBatchResp message. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchResp.verify|verify} messages.
                         * @param message HelixRedeemKeyByBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encode(message: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Encodes the specified HelixRedeemKeyByBatchResp message, length delimited. Does not implicitly {@link code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchResp.verify|verify} messages.
                         * @param message HelixRedeemKeyByBatchResp message or plain object to encode
                         * @param [writer] Writer to encode to
                         * @returns Writer
                         */
                        public static encodeDelimited(message: code.justin.tv.commerce.phanto.IHelixRedeemKeyByBatchResp, writer?: $protobuf.Writer): $protobuf.Writer;

                        /**
                         * Decodes a HelixRedeemKeyByBatchResp message from the specified reader or buffer.
                         * @param reader Reader or buffer to decode from
                         * @param [length] Message length if known beforehand
                         * @returns HelixRedeemKeyByBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchResp;

                        /**
                         * Decodes a HelixRedeemKeyByBatchResp message from the specified reader or buffer, length delimited.
                         * @param reader Reader or buffer to decode from
                         * @returns HelixRedeemKeyByBatchResp
                         * @throws {Error} If the payload is not a reader or valid buffer
                         * @throws {$protobuf.util.ProtocolError} If required fields are missing
                         */
                        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchResp;

                        /**
                         * Verifies a HelixRedeemKeyByBatchResp message.
                         * @param message Plain object to verify
                         * @returns `null` if valid, otherwise the reason why it is not
                         */
                        public static verify(message: { [k: string]: any }): (string|null);

                        /**
                         * Creates a HelixRedeemKeyByBatchResp message from a plain object. Also converts values to their respective internal types.
                         * @param object Plain object
                         * @returns HelixRedeemKeyByBatchResp
                         */
                        public static fromObject(object: { [k: string]: any }): code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchResp;

                        /**
                         * Creates a plain object from a HelixRedeemKeyByBatchResp message. Also converts values to other types if specified.
                         * @param message HelixRedeemKeyByBatchResp
                         * @param [options] Conversion options
                         * @returns Plain object
                         */
                        public static toObject(message: code.justin.tv.commerce.phanto.HelixRedeemKeyByBatchResp, options?: $protobuf.IConversionOptions): { [k: string]: any };

                        /**
                         * Converts this HelixRedeemKeyByBatchResp to JSON.
                         * @returns JSON object
                         */
                        public toJSON(): { [k: string]: any };
                    }
                }
            }
        }
    }
}

/** Namespace google. */
export namespace google {

    /** Namespace protobuf. */
    namespace protobuf {

        /** Properties of a Timestamp. */
        interface ITimestamp {

            /** Timestamp seconds */
            seconds?: (number|Long|null);

            /** Timestamp nanos */
            nanos?: (number|null);
        }

        /** Represents a Timestamp. */
        class Timestamp implements ITimestamp {

            /**
             * Constructs a new Timestamp.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.ITimestamp);

            /** Timestamp seconds. */
            public seconds: (number|Long);

            /** Timestamp nanos. */
            public nanos: number;

            /**
             * Creates a new Timestamp instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Timestamp instance
             */
            public static create(properties?: google.protobuf.ITimestamp): google.protobuf.Timestamp;

            /**
             * Encodes the specified Timestamp message. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
             * @param message Timestamp message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.ITimestamp, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Timestamp message, length delimited. Does not implicitly {@link google.protobuf.Timestamp.verify|verify} messages.
             * @param message Timestamp message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.ITimestamp, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Timestamp message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Timestamp
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Timestamp;

            /**
             * Decodes a Timestamp message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Timestamp
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Timestamp;

            /**
             * Verifies a Timestamp message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Timestamp message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Timestamp
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Timestamp;

            /**
             * Creates a plain object from a Timestamp message. Also converts values to other types if specified.
             * @param message Timestamp
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Timestamp, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Timestamp to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }
    }
}
