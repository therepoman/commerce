import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { RootPage } from './src/pages/root';
import { HashRouter } from 'react-router-dom'

document.title = "Made with luv <3"

document.addEventListener('DOMContentLoaded', (event) => {
ReactDOM.render(
  <HashRouter>
    <RootPage userName={window.userName} userToken={window.token}/>
  </HashRouter>
,
  document.getElementById('root')
);
});