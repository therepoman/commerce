/**
 * Use this when creating a static displayName field in your HOC/wrapped component classes.
 */
export function createHOCDisplayName<P>(
    name: string,
    Component: React.ComponentType<P>
  ) {
    return `${name}(${Component.displayName || Component.name || "Component"})`;
  }
  