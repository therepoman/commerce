import * as React from "react";
import { createHOCDisplayName } from "./hoc";

/**
 * Wrapper for React.createContext
 * Returns a provider component and a HOC component creator to use on consuming components
 * @param name Used to give generated components relevant displayName values
 * @param fallbackState The context value given to consumers that get mounted without a provider parent
 */
export function createContext<Context extends object>(
  name: string,
  fallbackState: Context
) {
  // tslint:disable-next-line: ban
  const context = React.createContext<Context>(fallbackState);
  const { Consumer, Provider } = context;

  /**
   * Consumer HOC for accessing this context
   */
  function withContext<ProvidedProps, P = {}>(
    mapContextToProps: (c: Context, ownProps: P) => ProvidedProps
  ) {
    return (WrappedComponent: React.ComponentType<P>) => {
      const HocContextConsumer: React.FC<P> = props => {
        // tslint:disable-next-line: ban-react-hooks
        const contextValue = React.useContext(context);
        const injectedProps = mapContextToProps(contextValue, props);
        return <WrappedComponent {...props} {...injectedProps} />;
      };
      HocContextConsumer.displayName = createHOCDisplayName(
        `With${name}`,
        WrappedComponent
      );
      // tslint:disable-next-line: no-any
      return (HocContextConsumer as unknown) as React.ComponentClass<any>;
    };
  }

  return {
    context,
    withContext,
    InnerProvider: Provider,
    NativeConsumer: Consumer
  };
}
