import * as React from "react";
import { createContext } from "../utils/context";

export interface User {
    id: string;
}

export interface Routes {
    [key: string]: string;
}

export interface App {
    user?: User;
    routes?: Routes;
}

export interface AppActions {
  loadApp: () => void;
  getUser: () => User;
  getRoutes: () => Routes;
}

export type AppContext = App & AppActions;

export const {
  InnerProvider,
  withContext: withAppContext
} = createContext<AppContext>("EnabledRewardsContext", {
    loadApp: () => null,
    getUser: () => undefined,
    getRoutes: () => undefined
});

export type State = AppContext;

export type Props = {};

export class AppContextProvider extends React.Component<
  Props,
  State
> {
  public constructor(props: Props) {
    super(props);
    this.state = {
        loadApp: this.loadApp,
        getUser: this.getUser,
        getRoutes: this.getRoutes
    };
  }

  public render() {
    return (
      <InnerProvider value={this.state}>{this.props.children}</InnerProvider>
    );
  }

  private loadApp() {
    // TODO call server to load application
  }

  private getUser() {
      return this.state.user;
  }

  private getRoutes() {
    return this.state.routes;
}
}
