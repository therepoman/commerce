import * as React from "react";
import {PoolsPage} from "./component";
import { code } from "../../../../generated/proto";
import { RouteComponentProps } from "react-router";

export interface State {
    pools: code.justin.tv.commerce.phanto.IKeyPool[]
}

export interface Props extends RouteComponentProps {

}

export class PoolsPageContainer extends React.Component<Props, State> {
    public state: State = {
        pools: []
    };

    public componentDidMount() {
        this.getAllPools();
    }

    public render() {
        return <PoolsPage pools={this.state.pools} />;
    }

    private getAllPools = () => {
        const service = code.justin.tv.commerce.phoenix.Phoenix.create(async (_method: any, requestData: BodyInit, callback: { (arg0: any, arg1: Uint8Array): void; (arg0: any, arg1: any): void; }) => {
            try {
                const resp = await fetch("http://localhost:8080/twirp/code.justin.tv.commerce.phoenix.Phoenix/PhantoGetAllKeyPools", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/protobuf",
                        "Authorization": "Bearer " + window.token,
                    },
    
                });
                const buffer = await resp.arrayBuffer();
                callback(null, new Uint8Array(buffer))
            } catch (e) {
                callback(e, null)
            }
    
        }, false, false)
    
        service.phantoGetAllKeyPools({}, (err, response) => {
          this.setState({
            pools: response.keyPools
          });
        });
    };
}