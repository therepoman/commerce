import { CoreText, StyledLayout, Background, Display, FlexDirection, TextType, Layout, Table, TableHeader, TableHeading, TableBody, TableRow, TableCell, Button, ButtonType, FormGroup, Input, InputType, Select, CoreLink } from "twitch-core-ui";
import * as React from "react";
import { code } from "../../../../generated/proto";

export interface Props {
  pools: code.justin.tv.commerce.phanto.IKeyPool[];
}

export interface State {
  poolIDError: boolean;
  poolID: string;

  productTypeError: boolean;
  productType: string;

  skuError: boolean;
  sku: string;

  redemptionMode: string;

  descriptionError: boolean;
  description: string;

  handlerGroupIDError: boolean;
  handlerGroupID: string;

  startDateError: boolean;
  startDate: string;

  endDateError: boolean;
  endDate: string;
}

interface KeyPool {
  productType: string;
  sku: string;
  description: string;
  pollID: string;
  status: string;
}

export class PoolsPage extends React.Component<Props, State> {
  public state: State = {
    poolIDError: false,
    poolID: "",
    productTypeError: false,
    productType: "",
    skuError: false,
    sku: "",
    redemptionMode: "REDEEM_BY_KEY",
    descriptionError: false,
    description: "",
    handlerGroupIDError: false,
    handlerGroupID: "",
    startDateError: false,
    startDate: "",
    endDateError: false,
    endDate: "",
  };

  public render() {
    return (
      <StyledLayout background={Background.Base} flexGrow={1} display={Display.Flex} flexDirection={FlexDirection.Column} padding={2}>
        <Layout margin={{bottom:2}}>
          <CoreText bold type={TextType.H3}>{"Keys Pools"}</CoreText>
          {this.renderKeyPools()}
        </Layout>
        <Layout>
          <CoreText bold type={TextType.H3}>{"Create a New Pool Key"}</CoreText>
          {this.renderCreatePoolForm()}
        </Layout>
      </StyledLayout>
    );
  }

  private renderCreatePoolForm() {
    return (
      <Layout margin={1}>
          <FormGroup label="Pool ID"
            error={this.state.poolIDError}
            errorMessage={"Must provide a pool id."}>
            <Input   
              type={InputType.Text}
              placeholder="Unique id representing the key pool" 
              onChange={this.onPoolIDChange}/>
          </FormGroup>
          <FormGroup label="Product Type"
              error={this.state.productTypeError}
              errorMessage={"Must provide a product type."}>
            <Input   
              type={InputType.Text}
              placeholder="Product type"
              onChange={this.onProductTypeChange} />
          </FormGroup>
          <FormGroup label="SKU"
              error={this.state.skuError}
              errorMessage={"Must provide a SKU."}>
            <Input   
              type={InputType.Text}
              placeholder="SKU" 
              onChange={this.onSKUChange}/>
          </FormGroup>
          <FormGroup label="Redemption Mode">
            <Select 
              value={this.state.redemptionMode}
              onChange={this.onRedemptionModeChange}
            >
              <option value="REDEEM_BY_KEY">REDEEM_BY_KEY</option>
              <option value="REDEEM_BY_BATCH_ID">REDEEM_BY_BATCH_ID</option>
            </Select>
          </FormGroup>
          <FormGroup label="Description"
              error={this.state.descriptionError}
              errorMessage={"Must provide a description."}>
            <Input   
              type={InputType.Text}
              placeholder="Description" 
              onChange={this.onDescriptionChange}/>
          </FormGroup>
          <FormGroup label="Handler Group ID"
              error={this.state.handlerGroupIDError}
              errorMessage={"Must provide a handler group."}>
            <Input   
              type={InputType.Text}
              placeholder="Handler Group ID" 
              onChange={this.onHandlerGroupIDChange}/>
          </FormGroup>
          <FormGroup label="Start Date"
              error={this.state.startDateError}
              errorMessage={"Must provide a start date."}>
            <Input   
              type={InputType.Text}
              placeholder="Start Date" 
              onChange={this.onStartDateChange}/>
          </FormGroup>
          <FormGroup label="End Date"
              error={this.state.endDateError}
              errorMessage={"Must provide a end date."}>
            <Input   
              type={InputType.Text}
              placeholder="End Date" 
              onChange={this.onEndDateChange}/>
          </FormGroup>
          <Layout margin={{top: 1}}>
            <Button
              onClick={this.createPool}>
              {"Create"}
            </Button>
          </Layout>
      </Layout>
    );
  }

  private createPool = () => {
    if (!this.checkState()){
      return;
    }
  }

  private onPoolIDChange = (evt: React.FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.value == "")
    {
      this.setState({
        poolIDError: true
      });
    } else {
      this.setState({
        poolIDError: false
      });
    }

    this.setState({
      poolID: evt.currentTarget.value
    });
  };

  private onProductTypeChange = (evt: React.FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.value == "")
    {
      this.setState({
        productTypeError: true
      });
    } else {
      this.setState({
        productTypeError: false
      });
    }

    this.setState({
      productType: evt.currentTarget.value
    });
  };

  private onSKUChange = (evt: React.FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.value == "")
    {
      this.setState({
        skuError: true
      });
    } else {
      this.setState({
        skuError: false
      });
    }

    this.setState({
      sku: evt.currentTarget.value
    });
  };

  private onDescriptionChange = (evt: React.FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.value == "")
    {
      this.setState({
        descriptionError: true
      });
    } else {
      this.setState({
        descriptionError: false
      });
    }

    this.setState({
      description: evt.currentTarget.value
    });
  };

  private onRedemptionModeChange = (e: React.FormEvent<HTMLSelectElement>) => {
      this.setState({
        redemptionMode: e.currentTarget.value
      });
  };

  private onHandlerGroupIDChange = (evt: React.FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.value == "")
    {
      this.setState({
        handlerGroupIDError: true
      });
    } else {
      this.setState({
        handlerGroupIDError: false
      });
    }

    this.setState({
      handlerGroupID: evt.currentTarget.value
    });
  };

  private onStartDateChange = (evt: React.FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.value == "")
    {
      this.setState({
        startDateError: true
      });
    } else {
      this.setState({
        startDateError: false
      });
    }

    this.setState({
      startDate: evt.currentTarget.value
    });
  };

  private onEndDateChange = (evt: React.FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.value == "")
    {
      this.setState({
        endDateError: true
      });
    } else {
      this.setState({
        endDateError: false
      });
    }

    this.setState({
      endDate: evt.currentTarget.value
    });
  };

  private checkState() {
    let isBad = false;

    if (this.state.poolID == ""){
      this.setState({
        poolIDError: true
      });

      isBad = true;
    }

    if (this.state.productType == ""){
      this.setState({
        productTypeError: true
      });

      isBad = true;
    }

    if (this.state.sku == ""){
      this.setState({
        skuError: true
      });

      isBad = true;
    }

    if (this.state.description == ""){
      this.setState({
        descriptionError: true
      });

      isBad = true;
    }

    if (this.state.handlerGroupID == ""){
      this.setState({
        handlerGroupIDError: true
      });

      isBad = true;
    }

    if (this.state.startDate == ""){
      this.setState({
        startDateError: true
      });

      isBad = true;
    }

    if (this.state.endDate == ""){
      this.setState({
        endDateError: true
      });

      isBad = true;
    }

    return isBad
  }

    private renderKeyPools() {
      return (
        <Layout>
          <Layout margin={1}>
            <Table alternateRows>
              <TableHeader>
                <TableHeading label="Pool ID"/>
                <TableHeading label="Product Type"/>
                <TableHeading label="SKU"/>
                <TableHeading label="Redemption Mode"/>
                <TableHeading label="Description"/>
                <TableHeading label="Handler Group ID"/>
                <TableHeading label="Start Date"/>
                <TableHeading label="End Date"/>
                <TableHeading label="Status"/>
                <TableHeading label="Manage Batches"/>
                <TableHeading label="Manage Reports"/>
                <TableHeading label="Manage Metadata"/>
                <TableHeading label="Actions"/>
              </TableHeader>
              <TableBody>
                {this.props.pools.map(o => this.renderCell(o))}
              </TableBody>
            </Table>
          </Layout>
          <Layout margin={1}>
            <Button>
              {"Next Page"}
            </Button>
          </Layout>
        </Layout>
      );
    }

  private renderCell(pool: code.justin.tv.commerce.phanto.IKeyPool) {

    const link = `/keys/batches/${pool.poolId}`
    return (
          <TableRow key={pool.poolId}>
            <TableCell>
              {pool.poolId || ""}
            </TableCell>
            <TableCell>
              {pool.productType || ""}
            </TableCell>
            <TableCell>
              {pool.sku || ""}
            </TableCell>
            <TableCell>
              {pool.redemptionMode || ""}
            </TableCell>
            <TableCell>
              {pool.description || ""}
            </TableCell>
            <TableCell>
              {pool.handlerGroupId || ""}
            </TableCell>
            <TableCell>
              {""}
            </TableCell>
            <TableCell>
              {""}
            </TableCell>
            <TableCell>
              {pool.status || ""}
            </TableCell>
            <TableCell>
              {<CoreLink to={link}>{"Manage Batches"}</CoreLink>}
            </TableCell>
            <TableCell>
              {<CoreLink>{"Manage Reports"}</CoreLink>}
            </TableCell>
            <TableCell>
              {<CoreLink>{"Manage Metadata"}</CoreLink>}
            </TableCell>
            <TableCell>
              {<Button type={ButtonType.Alert}>{"Deactivate"}</Button>}
            </TableCell>
          </TableRow>
    );
  }
}