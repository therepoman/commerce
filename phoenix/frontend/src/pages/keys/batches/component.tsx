import { CoreText, StyledLayout, Background, Display, FlexDirection, TextType, FormGroup, InputType, Input, SVGAsset, Layout, Table, TableHeader, TableHeading, Position, Tooltip, ButtonIcon, ButtonIconType, TableBody, TableRow, TableCell, CoreLink, Button, ButtonType, ButtonState } from "twitch-core-ui";
import * as React from "react";
import { RouteComponentProps } from "react-router-dom";
import * as proto from "../../../../generated/proto"
import { code } from "../../../../generated/proto";

export type Props = RouteComponentProps<{ poll_id: string }>;

export interface State {
  pollIDError: boolean;
  pollID: string;
  batches: code.justin.tv.commerce.phanto.IKeyBatch[];
  numberOfKeys: number;
  numberOfKeysError: boolean;
  updating: boolean;
}

export class BatchesPage extends React.Component<Props, State> {
  public state: State = {
    pollIDError: false,
    pollID: this.props.match.params.poll_id || "",
    batches: [],
    numberOfKeys: 0,
    numberOfKeysError: false,
    updating: false,
  };

  public componentWillReceiveProps(nextProps: Props) {
    console.log(nextProps)
  }

  public render() {
    return (
      <StyledLayout background={Background.Base} flexGrow={1} display={Display.Flex} flexDirection={FlexDirection.Column} padding={2}>
          <CoreText bold type={TextType.H3}>{"Keys Batches"}</CoreText>

        <Layout padding={{top:1}}>
          <FormGroup label={"Search"}
            error={this.state.pollIDError}
            errorMessage={"Must provide a poll id."}>
            <Layout position={Position.Relative}>
              <Input   
                type={InputType.Text}
                placeholder="Enter Poll ID"
                value={this.props.match.params.poll_id}
                onChange={this.onChange}/>
              <Layout attachRight attachTop position={Position.Absolute}>
                <div>
                  <Tooltip label="Search">
                    <ButtonIcon 
                      icon={SVGAsset.NavSearch}
                      type={ButtonIconType.Secondary}
                      onClick={this.getKeyBatch}
                      disabled={this.state.pollIDError}
                      />
                  </Tooltip>
                </div>
              </Layout>
            </Layout>
          </FormGroup>
        </Layout>
        {this.renderSearchResults()}
        {this.renderGenerateForm()}
      </StyledLayout>
    );
  }

  private getKeyBatch = () => {
    if (this.state.pollID == "") {
      this.setState({
        pollIDError: true
      })

      return;
    }

        const service = code.justin.tv.commerce.phoenix.Phoenix.create(async (_method: any, requestData: BodyInit, callback: { (arg0: any, arg1: Uint8Array): void; (arg0: any, arg1: any): void; }) => {
            try {
                const resp = await fetch("http://localhost:8080/twirp/code.justin.tv.commerce.phoenix.Phoenix/PhantoGetKeyBatches", {
                    method: "POST",
                    body: requestData,
                    headers: {
                        "Content-Type": "application/protobuf",
                        "Authorization": "Bearer " + window.token,
                    },
    
                });
                const buffer = await resp.arrayBuffer();
                callback(null, new Uint8Array(buffer))
            } catch (e) {
                callback(e, null)
            }
    
        }, false, false)
    
        service.phantoGetKeyBatches({poolId: this.state.pollID}, (err, response) => {
          if (err == null) {
            this.setState({
              batches: response.keyBatches
            });
          }
        });
  }

  private generateKeyBatch = () => {
    if (this.state.batches.length <= 0) {
      return
    }

    if (this.state.numberOfKeys <= 0) {
      return
    }

    const batch = this.state.batches[0];
    console.log(batch, this.state.numberOfKeys)

    const service = proto.code.justin.tv.commerce.phoenix.Phoenix.create(async (_method: any, requestData: BodyInit, callback: { (arg0: any, arg1: Uint8Array): void; (arg0: any, arg1: any): void; }) => {
        try {
            const resp = await fetch("http://localhost:8080/twirp/code.justin.tv.commerce.phoenix.Phoenix/PhantoGenerateKeyBatch", {
                method: "POST",
                body: requestData,
                headers: {
                    "Content-Type": "application/protobuf",
                    "Authorization": "Bearer " + window.token,
                },

            });
            const buffer = await resp.arrayBuffer();
            callback(null, new Uint8Array(buffer))
        } catch (e) {
            callback(e, null)
        }

    }, false, false)

    service.phantoGenerateKeyBatch({batchId: batch.batchId, poolId: batch.poolId, numberOfKeys: this.state.numberOfKeys}, (err: any, response: { batchId: any; }) => {
      if (err == null) {
        console.log(response.batchId)
      }
    });
  };

  private onChange = (evt: React.FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.value == "") {
      this.setState({
        pollIDError: true
      });
    } else {
      this.setState({
        pollIDError: false
      });
    }

    this.setState({
      pollID: evt.currentTarget.value
    });
  };

  private renderSearchResults() {
    return (
          <Layout margin={1}>
            <Table alternateRows>
              <TableHeader>
                <TableHeading label="Batch ID"/>
                <TableHeading label="Number of Keys"/>
                <TableHeading label="Workflow Status"/>
                <TableHeading label="Status"/>
                <TableHeading label="Actions"/>
              </TableHeader>
              <TableBody>
                {this.state.batches.map(o => this.renderCell(o))}
              </TableBody>
              </Table>
            </Layout>
    );
  }

  private renderCell(batch: code.justin.tv.commerce.phanto.IKeyBatch) {
    return (
          <TableRow key={batch.poolId}>
            <TableCell>
              {batch.poolId || ""}
            </TableCell>
            <TableCell>
              {batch.numberOfKeys || ""}
            </TableCell>
            <TableCell>
              {batch.workflowStatus || ""}
            </TableCell>
            <TableCell>
              {batch.status || ""}
            </TableCell>
            <TableCell>
              {<Button type={ButtonType.Alert}>{"Deactivate"}</Button>}
            </TableCell>
          </TableRow>
    );
  }

  private renderGenerateForm() {
    if (this.state.batches.length <= 0) {
      return null
    }

    const batch = this.state.batches[0];

    return (
      <Layout margin={{top: 3}} padding={1}>
        <CoreText bold type={TextType.H3}>{"Generate Product Keys"}</CoreText>
        <FormGroup label={"Poll ID"}>
          <Input 
            type={InputType.Text}
            disabled
            value={batch.poolId} />
        </FormGroup>
        <FormGroup label={"Batch ID"}>
          <Input 
            type={InputType.Text}
            disabled
            value={batch.batchId} />
        </FormGroup>
        <FormGroup label={"Number of Keys to Generate"}>
          <Input 
            type={InputType.Text}
            placeholder="Number of Keys to Generate" 
            onChange={this.onNumberOfKeysChanged}/>
        </FormGroup>

        <Layout margin={{top:1}}>
          <Button onClick={this.generateKeyBatch} state={this.state.updating ? ButtonState.Loading : ButtonState.Default}>
            {"Create"}
          </Button>
        </Layout>
      </Layout>
    );
  }

    private onNumberOfKeysChanged = (evt: React.FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.value == "")
    {
      this.setState({
        numberOfKeysError: true
      });
    } else {
      this.setState({
        numberOfKeysError: false
      });
    }

    this.setState({
      numberOfKeys: parseInt(evt.currentTarget.value, 10)
    });
  };
}