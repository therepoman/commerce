import { Background, Display, FlexDirection, StyledLayout, CoreText, Color } from "twitch-core-ui";
import * as React from "react";

export class AccessDeniedPage extends React.Component {
  public render() {
    return (
      <StyledLayout background={Background.Base} flexGrow={1} display={Display.Flex} flexDirection={FlexDirection.Column}>
          <CoreText color={Color.Error}>"Access Denied"</CoreText>
      </StyledLayout>
    );
  }
}