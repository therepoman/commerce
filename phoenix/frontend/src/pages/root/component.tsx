import * as React from "react";
import {
  CoreText,
  Layout,
  FlexDirection,
  Display,
  StyledLayout,
  Background,
  SVGAsset,
  Button,
  ButtonType,
  JustifyContent,
  AlignItems,
  ButtonIcon,
  TextAlign,
  CoreLink,
  TextType,
  Icon} from "twitch-core-ui";
import { Route, Switch, RouteComponentProps, withRouter, RouteProps } from "react-router";
import "./styles.scss";
import "twitch-core-ui/css/index.css";
import { PoolsPageContainer } from "../keys/pools";
import { BatchesPage } from "../keys/batches";
import { compose } from "../../common/utils/compose";
import { AccessDeniedPage } from "../not-allowed/component";

export interface PublicProps {
  userName: string,
  userToken: string,
}

export type Props = RouteComponentProps<{}> & PublicProps;

export class RootPagePresentation extends React.Component<Props> {
  public render() {
    return (
      <StyledLayout className="root" background={Background.Base} flexGrow={1} display={Display.Flex} flexDirection={FlexDirection.Column}>
        <StyledLayout className="header" flexGrow={0} padding={0.5} background={Background.Alt2} display={Display.Flex} flexDirection={FlexDirection.Row} justifyContent={JustifyContent.Between} alignItems={AlignItems.Center}>
          <Icon
            asset={SVGAsset.Heart}
            />
            <Icon
            asset={SVGAsset.Heart}
            />
            <Icon
            asset={SVGAsset.Heart}
            />
            <Icon
            asset={SVGAsset.Heart}
            />
          <CoreText type={TextType.H4}>{"Welcome " + this.props.userName}</CoreText>
          <Icon
            asset={SVGAsset.Heart}
            />
          <Icon
            asset={SVGAsset.Heart}
            />
          <Icon
            asset={SVGAsset.Heart}
            />
            <Icon
            asset={SVGAsset.Heart}
            />
        </StyledLayout>
        <Layout className="page" flexGrow={1} display={Display.Flex} flexDirection={FlexDirection.Row}>
          <StyledLayout className="nav-pane" borderRight>
            {this.renderNavContents()}
          </StyledLayout>
          <Layout className="content">
            {this.renderPageContent()}
          </Layout>
        </Layout>
        <StyledLayout className="footer" 
          background={Background.Alt2} 
          textAlign={TextAlign.Center}         
          alignItems={AlignItems.Center} 
          justifyContent={JustifyContent.Center}           
          flexGrow={0}
          padding={0.5}>
          <CoreText>{"© Twitch 2019"}</CoreText>
        </StyledLayout>
      </StyledLayout>
    );
  }

  // TODO: Render which pages the user has access to.
  private renderNavContents() {
    const paths = window.allowedPaths;
    return (
      <StyledLayout padding={1} display={Display.Flex} flexDirection={FlexDirection.Column}>
        <StyledLayout borderBottom>
          <CoreText bold>
            {"SERVICES"}
          </CoreText>
        </StyledLayout>

        <Layout>
          <CoreText>
            {"Keys"}
          </CoreText>
          <Layout margin={{left:2}} display={Display.Flex} flexDirection={FlexDirection.Column}>
            { paths.includes("/keys/pools") && 
              <CoreLink to="/keys/pools">{"Pools"}</CoreLink>
            }
            { paths.includes("/keys/batches") && 
            <CoreLink to="/keys/batches">{"Batches"}</CoreLink>
            }
          </Layout>
        </Layout>
      </StyledLayout>
    );
  }

  private renderPageContent() {
    return (
        <Switch>
          <Route exact path="/keys" component={PrivateRoute(PoolsPageContainer)} />
          <Route exact path="/keys/pools" component={PrivateRoute(PoolsPageContainer)}/>
          <Route exact path="/keys/batches" component={PrivateRoute(BatchesPage)}/>
          <Route path="/keys/batches/:poll_id" component={PrivateRoute(BatchesPage)}/>
          <Route path="/" component={null}/>
        </Switch>
    );
  }
}

export const RootPage = compose(
  withRouter,
)(RootPagePresentation) as React.ComponentClass<PublicProps>;

interface PrivateRouteProps extends RouteProps {
  innerComponent: React.ComponentType<RouteComponentProps<any>>
}

const PrivateRoute = (Component: React.ComponentType<RouteComponentProps<any>>): React.ComponentType<RouteComponentProps<any>> => { 
  return (props: RouteComponentProps<any>) => {
    if(window.allowedPaths.indexOf(props.match.path) == -1) {
      return <AccessDeniedPage {...props} />;
    }

    return  <Component {...props} />
  }
}