
# Package jwt
Package jwt implements encoding and decoding of JSON objects as defined in RFC 7519.

A JWT is represented by 3 fields: A header JSON object, specifying information about the JWT itself, a claims JSON object
specifying a series of statements and a signature that validates the token. These are dot separated and encoded in base64url (see URLEncoding of encoding/base64).

This package is essentially the JSON package with some seasoning on top, it uses all the struct label logic from the encoding/json
stdlib package.


```shell

	$ go get -u code.justin.tv/tshadwell/jwt
```

## Contents
1. [Contents](#contents)
2. [Doc](#doc)
	1. [type Algorithm](#Algorithm)
		1. [func Name() string](#Algorithm.Name)
		2. [func Sign(value []byte) (signature []byte, err error)](#Algorithm.Sign)
		3. [func Size() int](#Algorithm.Size)
		4. [func Validate(value []byte, signature []byte) error](#Algorithm.Validate)
	2. [func DecodeAndValidate(header interface{}, claims interface{}, a Algorithm, jwt []byte) (err error)](#DecodeAndValidate)
	3. [func Encode(header interface{}, claims interface{}, a Algorithm) (jwt []byte, err error)](#Encode)
	4. [type Err](#Err)
		1. [func Error() string](#Err.Error)
	5. [var ErrInvalidB64 error](#ErrInvalidB64)
	6. [var ErrInvalidHeader error](#ErrInvalidHeader)
	7. [var ErrInvalidSignature error](#ErrInvalidSignature)
	8. [var ErrNotRSAPublicKey error](#ErrNotRSAPublicKey)
	9. [var ErrTooShort error](#ErrTooShort)
	10. [var ErrValidateOnly error](#ErrValidateOnly)
	11. [type Fragment](#Fragment)
		1. [func Claims() []byte](#Fragment.Claims)
		2. [func Decode(header interface{}, claims interface{}) (err error)](#Fragment.Decode)
		3. [func Header() []byte](#Fragment.Header)
		4. [func Signature() []byte](#Fragment.Signature)
		5. [func Validate(a Algorithm) (err error)](#Fragment.Validate)
	12. [func HS256(secret []byte) Algorithm](#HS256)
	13. [func HS384(secret []byte) Algorithm](#HS384)
	14. [func HS512(secret []byte) Algorithm](#HS512)
	15. [type Header](#Header)
		1. [func ValidateEqual(h2 Header) (err error)](#Header.ValidateEqual)
	16. [func NewHeader(a Algorithm) (h Header)](#NewHeader)
	17. [var None Algorithm](#None)
	18. [func Parse(jwt []byte) (f Fragment, err error)](#Parse)
	19. [func RS256(priv *rsa.PrivateKey) Algorithm](#RS256)
	20. [func RS384(priv *rsa.PrivateKey) Algorithm](#RS384)
	21. [func RS512(priv *rsa.PrivateKey) Algorithm](#RS512)
	22. [func RSAValidateOnly(f *rsa.PrivateKey Algorithm, pub *rsa.PublicKey) Algorithm](#RSAValidateOnly)
	23. [func ReadRSAPrivateKey(pkeyfile string) (k *rsa.PrivateKey, err error)](#ReadRSAPrivateKey)
	24. [func ReadRSAPublicKey(pkeyfile string) (k *rsa.PublicKey, err error)](#ReadRSAPublicKey)
	25. [func Write(header interface{}, claims interface{}, a Algorithm, w io.Writer) (n int, err error)](#Write)
3. [Imports](#imports)


## Doc


### <a name="Algorithm"></a>  [type](jwt.go#L30) [Algorithm](#Algorithm)
#### <a name="Algorithm.Name"></a> [func](jwt.go#L38) **Name**() [string](//godoc.org/builtin#string)
Name returns the RFC 7518 ("JSON Web Algorithms") compliant name of this Algorithm.

#### <a name="Algorithm.Sign"></a> [func](jwt.go#L32) **Sign**(value [][byte](//godoc.org/builtin#byte)) (signature [][byte](//godoc.org/builtin#byte), err [error](//godoc.org/builtin#error))
Sign signs the given byte value.

#### <a name="Algorithm.Size"></a> [func](jwt.go#L36) **Size**() [int](//godoc.org/builtin#int)
Size returns the size of this Algorithm's signature in bytes.

#### <a name="Algorithm.Validate"></a> [func](jwt.go#L34) **Validate**(value [][byte](//godoc.org/builtin#byte), signature [][byte](//godoc.org/builtin#byte)) [error](//godoc.org/builtin#error)
Validate validates the signature for the given byte value.

> interface{
>   **Name**() [string](//godoc.org/builtin#string) ; **Sign**(value [][byte](//godoc.org/builtin#byte)) (signature [][byte](//godoc.org/builtin#byte), err [error](//godoc.org/builtin#error)) ; **Size**() [int](//godoc.org/builtin#int) ; **Validate**(value [][byte](//godoc.org/builtin#byte), signature [][byte](//godoc.org/builtin#byte)) [error](//godoc.org/builtin#error)
> }


Algorithm represents an algorithm that can be used to
validate or sign a JWT.


### <a name="DecodeAndValidate"></a>  [func](parse.go#L102) **[DecodeAndValidate](#DecodeAndValidate)**(header interface{}, claims interface{}, a [Algorithm](#Algorithm), jwt [][byte](//godoc.org/builtin#byte)) (err [error](//godoc.org/builtin#error))

DecodeAndValidate decodes the json and validates the signature of the JSON Web Token
in `jwt`. The Algorithm MUST be known in advance of the token, as the header
cannot be trusted before the signature is validated.

This is a convenience function short for:
	var f Fragment
	if f, err = Parse(jwt); err != nil {
		return
	}

	if err = f.Validate(a); err != nil {
		return
	}

	if err = f.Decode(header, claims); err != nil {
		return
	}


### <a name="Encode"></a>  [func](encode.go#L14) **[Encode](#Encode)**(header interface{}, claims interface{}, a [Algorithm](#Algorithm)) (jwt [][byte](//godoc.org/builtin#byte), err [error](//godoc.org/builtin#error))

Encode writes a JWT encoded value to w with specified Algorithm.
The same Algorithm MUST be used to validate this class of JWT.
The algorithm field cannot be trusted and can be used to stage exploits.


### <a name="Err"></a>  [type](errors.go#L8) [Err](#Err)
#### <a name="Err.Error"></a> [func](errors.go#L13) **Error**() [string](//godoc.org/builtin#string)
> struct {  Err [error](//godoc.org/builtin#error)  Desc [string](//godoc.org/builtin#string) }


Err represents an annotated error from another package.


### <a name="ErrInvalidB64"></a>  [var](parse.go#L19) [ErrInvalidB64](#ErrInvalidB64) [error](//godoc.org/builtin#error)
> interface{
>   **[Error](//godoc.org/builtin#Error)**() [string](//godoc.org/builtin#string)
> }


ErrInvalidB64 represents an error where padding canonicalization fails due to invalid base64


### <a name="ErrInvalidHeader"></a>  [var](parse.go#L17) [ErrInvalidHeader](#ErrInvalidHeader) [error](//godoc.org/builtin#error)
> interface{
>   **[Error](//godoc.org/builtin#Error)**() [string](//godoc.org/builtin#string)
> }


ErrInvalidHeader represents an error where a JWT's header does not match.


### <a name="ErrInvalidSignature"></a>  [var](parse.go#L15) [ErrInvalidSignature](#ErrInvalidSignature) [error](//godoc.org/builtin#error)
> interface{
>   **[Error](//godoc.org/builtin#Error)**() [string](//godoc.org/builtin#string)
> }


ErrInvalidSignature represents an error where a JWT has an invalid signature.


### <a name="ErrNotRSAPublicKey"></a>  [var](rsa.go#L79) [ErrNotRSAPublicKey](#ErrNotRSAPublicKey) [error](//godoc.org/builtin#error)
> interface{
>   **[Error](//godoc.org/builtin#Error)**() [string](//godoc.org/builtin#string)
> }




### <a name="ErrTooShort"></a>  [var](parse.go#L13) [ErrTooShort](#ErrTooShort) [error](//godoc.org/builtin#error)
> interface{
>   **[Error](//godoc.org/builtin#Error)**() [string](//godoc.org/builtin#string)
> }


ErrTooShort represents an error where an input JWT has less than three sections.


### <a name="ErrValidateOnly"></a>  [var](rsa.go#L14) [ErrValidateOnly](#ErrValidateOnly) [error](//godoc.org/builtin#error)
> interface{
>   **[Error](//godoc.org/builtin#Error)**() [string](//godoc.org/builtin#string)
> }




### <a name="Fragment"></a>  [type](parse.go#L24) [Fragment](#Fragment)
#### <a name="Fragment.Claims"></a> [func](parse.go#L36) **Claims**() [][byte](//godoc.org/builtin#byte)
Claims returns the base64url encoded jwt claims.

#### <a name="Fragment.Decode"></a> [func](parse.go#L133) **Decode**(header interface{}, claims interface{}) (err [error](//godoc.org/builtin#error))
Decode decodes the JWT's header and claims into the values
indicated by the passed `header` and `claims` pointers.

#### <a name="Fragment.Header"></a> [func](parse.go#L33) **Header**() [][byte](//godoc.org/builtin#byte)
Header returns the base64url encoded jwt header.

#### <a name="Fragment.Signature"></a> [func](parse.go#L39) **Signature**() [][byte](//godoc.org/builtin#byte)
Signature returns the base64url encoded jwt signature.

#### <a name="Fragment.Validate"></a> [func](parse.go#L121) **Validate**(a [Algorithm](#Algorithm)) (err [error](//godoc.org/builtin#error))
Validate validates a JWT's signature with specified Algorithm. The Algorithm MUST be known in advance
of the token, as the header cannot be trusted.

> struct {  Parts [3][][byte](//godoc.org/builtin#byte)  Prefix [2][][byte](//godoc.org/builtin#byte) }


A Fragment represents the three base64 parts of a JSON Web Token
canonicalized to base64url with padding.


### <a name="HS256"></a>  [func](hmac.go#L57) **[HS256](#HS256)**(secret [][byte](//godoc.org/builtin#byte)) [Algorithm](#Algorithm)

HS256 returns a SHA256 MAC Algorithm, a 256bit symmetric signing algorithm.


### <a name="HS384"></a>  [func](hmac.go#L60) **[HS384](#HS384)**(secret [][byte](//godoc.org/builtin#byte)) [Algorithm](#Algorithm)

HS384 returns a SHA384 MAC Algorithm, a 384bit symmetric signing algorithm.


### <a name="HS512"></a>  [func](hmac.go#L63) **[HS512](#HS512)**(secret [][byte](//godoc.org/builtin#byte)) [Algorithm](#Algorithm)

HS512 returns a SHA512 MAC Algorithm, a 512bit symmetric signing algorithm.


### <a name="Header"></a>  [type](jwt.go#L12) [Header](#Header)
#### <a name="Header.ValidateEqual"></a> [func](jwt.go#L21) **ValidateEqual**(h2 [Header](#Header)) (err [error](//godoc.org/builtin#error))
ValidateEqual returns ErrInvalidHeader if h1 is not equal to h2.

> struct {  Algorithm [string](//godoc.org/builtin#string)  Type [string](//godoc.org/builtin#string) }


Header is an example JWT header. Check RFC 7519 for other fields
that are useful in a header


### <a name="NewHeader"></a>  [func](jwt.go#L18) **[NewHeader](#NewHeader)**(a [Algorithm](#Algorithm)) (h [Header](#Header))

NewHeader returns a new Header where the Algorithm is set to the Name of the Algorithm parameter, and the Type is "jwt".


### <a name="None"></a>  [var](jwt.go#L42) [None](#None) [Algorithm](#Algorithm)
> interface{
>   **Name**() [string](//godoc.org/builtin#string) ; **Sign**(value [][byte](//godoc.org/builtin#byte)) (signature [][byte](//godoc.org/builtin#byte), err [error](//godoc.org/builtin#error)) ; **Size**() [int](//godoc.org/builtin#int) ; **Validate**(value [][byte](//godoc.org/builtin#byte), signature [][byte](//godoc.org/builtin#byte)) [error](//godoc.org/builtin#error)
> }


None is a no-op Algorithm. The signature is empty.


### <a name="Parse"></a>  [func](parse.go#L45) **[Parse](#Parse)**(jwt [][byte](//godoc.org/builtin#byte)) (f [Fragment](#Fragment), err [error](//godoc.org/builtin#error))

Parse parses a json web token into the standard form used by this package
where the base64 components have padding as in encoding/base64.


### <a name="RS256"></a>  [func](rsa.go#L49) **[RS256](#RS256)**(priv \*[rsa.PrivateKey](//godoc.org/crypto/rsa#PrivateKey)) [Algorithm](#Algorithm)

RS256 returns the RSASSA-PKCS1-v1_5 SHA-256 digital signature algorithm, a 256bit asymmetric signing algorithm.


### <a name="RS384"></a>  [func](rsa.go#L52) **[RS384](#RS384)**(priv \*[rsa.PrivateKey](//godoc.org/crypto/rsa#PrivateKey)) [Algorithm](#Algorithm)

RS384 returns the RSASSA-PKCS1-v1_5 SHA-384 digital signature algorithm, a 384bit asymmetric signing algorithm.


### <a name="RS512"></a>  [func](rsa.go#L55) **[RS512](#RS512)**(priv \*[rsa.PrivateKey](//godoc.org/crypto/rsa#PrivateKey)) [Algorithm](#Algorithm)

RS512 returns the RSASSA-PKCS1-v1_5 SHA-512 digital signature algorithm, a 512bit asymmetric signing algorithm.


### <a name="RSAValidateOnly"></a>  [func](rsa.go#L115) **[RSAValidateOnly](#RSAValidateOnly)**(f \*[rsa.PrivateKey](//godoc.org/crypto/rsa#PrivateKey) [Algorithm](#Algorithm), pub \*[rsa.PublicKey](//godoc.org/crypto/rsa#PublicKey)) [Algorithm](#Algorithm)

RSAValidateOnly takes a function that would take an *rsa.PrivateKey and an *rsa.PublicKey
and returns an Algorithm that only allows validation.


### <a name="ReadRSAPrivateKey"></a>  [func](rsa.go#L64) **[ReadRSAPrivateKey](#ReadRSAPrivateKey)**(pkeyfile [string](//godoc.org/builtin#string)) (k \*[rsa.PrivateKey](//godoc.org/crypto/rsa#PrivateKey), err [error](//godoc.org/builtin#error))

ReadRSAPrivateKey parses a pem & PKCS1 encoded RSA private key as can be used by this package such as might be generated by

	ssh-keygen -r rsa -f id_rsa.pem -m pem

Yours will look something like this:

	-----BEGIN RSA PRIVATE KEY-----
	MIIEpAIBAAKCAQEA3X32AoiRV+6WTjamOf1ZHvtZR507MtVgLwRs8Ay9Ih8Yqo3u
	... more letters ...
	-----END RSA PRIVATE KEY-----


### <a name="ReadRSAPublicKey"></a>  [func](rsa.go#L89) **[ReadRSAPublicKey](#ReadRSAPublicKey)**(pkeyfile [string](//godoc.org/builtin#string)) (k \*[rsa.PublicKey](//godoc.org/crypto/rsa#PublicKey), err [error](//godoc.org/builtin#error))

ReadRSAPublicKey parses a pem & PKCS1 encoded RSA public key such as might be generated by:

	openssl rsa -in id_rsa.pem -pubout > id_rsa.pub.pem
	ssh-keygen -f id_rsa.pub -e -m pem

Yours will look something like this:

	-----BEGIN RSA PUBLIC KEY-----
	MIIBCgKCAQEA3X32AoiRV+6WTjamOf1ZHvtZR507MtVgLwRs8Ay9Ih8Yqo3utCEx
	... more letters ...
	-----END RSA PUBLIC KEY-----


### <a name="Write"></a>  [func](encode.go#L63) **[Write](#Write)**(header interface{}, claims interface{}, a [Algorithm](#Algorithm), w [io.Writer](//godoc.org/io#Writer)) (n [int](//godoc.org/builtin#int), err [error](//godoc.org/builtin#error))

Write writes a JWT encoded value to w with specified Algorithm.
The same Algorithm MUST be used to validate this class of JWT.
The algorithm field cannot be trusted and can be used to stage exploits.

## Imports
[encoding/base64](//godoc.org/encoding/base64)
[encoding/json](//godoc.org/encoding/json)
[io](//godoc.org/io)
[fmt](//godoc.org/fmt)
[crypto](//godoc.org/crypto)
[crypto/hmac](//godoc.org/crypto/hmac)
[crypto/sha256](//godoc.org/crypto/sha256)
[crypto/sha512](//godoc.org/crypto/sha512)
[bytes](//godoc.org/bytes)
[errors](//godoc.org/errors)
[crypto/rand](//godoc.org/crypto/rand)
[crypto/rsa](//godoc.org/crypto/rsa)
[crypto/x509](//godoc.org/crypto/x509)
[encoding/pem](//godoc.org/encoding/pem)
[io/ioutil](//godoc.org/io/ioutil)
