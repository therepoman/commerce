package internal

import uuid "github.com/satori/go.uuid"

// NewInstanceID returns a new instance id
func NewInstanceID() string {
	return uuid.NewV4().String()
}
