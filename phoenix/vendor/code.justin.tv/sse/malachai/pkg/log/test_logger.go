package log

// Tester is the api we expect from testing.T and testing.B
type Tester interface {
	Log(args ...interface{})
	Logf(fmt string, args ...interface{})
}

// TestLogger writes logs to a tester
type TestLogger struct {
	T Tester
}

// Debug ...
func (tl *TestLogger) Debug(args ...interface{}) {
	tl.T.Log(args...)
}

// Debugf ...
func (tl *TestLogger) Debugf(format string, args ...interface{}) {
	tl.T.Logf(format, args...)
}

// Debugln ...
func (tl *TestLogger) Debugln(args ...interface{}) {
	tl.T.Log(args...)
}

// Error ...
func (tl *TestLogger) Error(args ...interface{}) {
	tl.T.Log(args...)
}

// Errorf ...
func (tl *TestLogger) Errorf(format string, args ...interface{}) {
	tl.T.Logf(format, args...)
}

// Errorln ...
func (tl *TestLogger) Errorln(args ...interface{}) {
	tl.T.Log(args...)
}

// Fatal ...
func (tl *TestLogger) Fatal(args ...interface{}) {
	tl.T.Log(args...)
}

// Fatalf ...
func (tl *TestLogger) Fatalf(format string, args ...interface{}) {
	tl.T.Logf(format, args...)
}

// Fatalln ...
func (tl *TestLogger) Fatalln(args ...interface{}) {
	tl.T.Log(args...)
}

// Info ...
func (tl *TestLogger) Info(args ...interface{}) {
	tl.T.Log(args...)
}

// Infof ...
func (tl *TestLogger) Infof(format string, args ...interface{}) {
	tl.T.Logf(format, args...)
}

// Infoln ...
func (tl *TestLogger) Infoln(args ...interface{}) {
	tl.T.Log(args...)
}

// Panic ...
func (tl *TestLogger) Panic(args ...interface{}) {
	tl.T.Log(args...)
}

// Panicf ...
func (tl *TestLogger) Panicf(format string, args ...interface{}) {
	tl.T.Logf(format, args...)
}

// Panicln ...
func (tl *TestLogger) Panicln(args ...interface{}) {
	tl.T.Log(args...)
}

// Print ...
func (tl *TestLogger) Print(args ...interface{}) {
	tl.T.Log(args...)
}

// Printf ...
func (tl *TestLogger) Printf(format string, args ...interface{}) {
	tl.T.Logf(format, args...)
}

// Println ...
func (tl *TestLogger) Println(args ...interface{}) {
	tl.T.Log(args...)
}

// Warn ...
func (tl *TestLogger) Warn(args ...interface{}) {
	tl.T.Log(args...)
}

// Warnf ...
func (tl *TestLogger) Warnf(format string, args ...interface{}) {
	tl.T.Logf(format, args...)
}

// Warnln ...
func (tl *TestLogger) Warnln(args ...interface{}) {
	tl.T.Log(args...)
}
