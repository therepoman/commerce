package policy

func newTestPolicyGenerator() *IAMPolicyGenerator {
	pg := &IAMPolicyGenerator{
		AuxPolicyArn:                 "arn:aws:iam::734326455073:policy/sandstorm/testing/aux_policy/sandstorm-agent-testing-aux",
		RWAuxPolicyArn:               "arn:aws:iam::734326455073:policy/sandstorm/testing/aux_policy/sandstorm-agent-testing-rw-aux",
		DynamoDBSecretsTableArn:      "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing",
		DynamoDBSecretsAuditTableArn: "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing_audit",
		DynamoDBNamespaceTableArn:    "arn:aws:dynamodb:us-west-2:734326455073:table/sandstorm-testing_namespaces",
		IAMPathPrefix:                IAMPathPrefix,
	}
	pg.SetPathPrefixEnvironment("testing")
	return pg
}
