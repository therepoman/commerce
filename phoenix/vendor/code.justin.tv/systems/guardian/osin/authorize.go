package osin

import (
	"errors"
	"net/http"
	"net/url"
	"time"

	"code.justin.tv/systems/guardian/internal/gctx"
	"code.justin.tv/systems/guardian/internal/oidc"
)

// AuthorizeRequestType is the type for OAuth param `response_type`
type AuthorizeRequestType string

const (
	CODE           AuthorizeRequestType = "code"
	TOKEN          AuthorizeRequestType = "token"
	ID_TOKEN       AuthorizeRequestType = "id_token"
	CODE_ID_TOKEN  AuthorizeRequestType = "code id_token"
	ID_TOKEN_TOKEN AuthorizeRequestType = "id_token token"
)

// Authorize request information
type AuthorizeRequest struct {
	Type        AuthorizeRequestType
	Client      Client
	Scope       string
	RedirectURI string
	State       string

	// Set if request is authorized
	Authorized bool

	// Token expiration in seconds. Change if different from default.
	// If type = TOKEN, this expiration will be for the ACCESS token.
	Expiration int32

	// Data to be passed to storage. Not used by the library.
	UserData interface{}

	// HttpRequest *http.Request for special use
	HttpRequest *http.Request
}

// Authorization data
type AuthorizeData struct {
	// Client information
	Client Client

	// Authorization code
	Code string

	// Token expiration in seconds
	ExpiresIn int32

	// Requested scope
	Scope string

	// Redirect URI from request
	RedirectURI string

	// State data from request
	State string

	// Date created
	CreatedAt time.Time

	// Data to be passed to storage. Not used by the library.
	UserData interface{}
}

// IsExpired is true if authorization expired
func (d *AuthorizeData) IsExpired() bool {
	return d.IsExpiredAt(time.Now())
}

// IsExpired is true if authorization expires at time 't'
func (d *AuthorizeData) IsExpiredAt(t time.Time) bool {
	return d.ExpireAt().Before(t)
}

// ExpireAt returns the expiration date
func (d *AuthorizeData) ExpireAt() time.Time {
	return d.CreatedAt.Add(time.Duration(d.ExpiresIn) * time.Second)
}

// AuthorizeTokenGen is the token generator interface
type AuthorizeTokenGen interface {
	GenerateAuthorizeToken(data *AuthorizeData) (string, error)
}

// HandleAuthorizeRequest is the main http.HandlerFunc for handling
// authorization requests
func (s *Server) HandleAuthorizeRequest(w *Response, r *http.Request) *AuthorizeRequest {
	r.ParseForm()

	// create the authorization request
	unescapedURI, err := url.QueryUnescape(r.Form.Get("redirect_uri"))
	if err != nil {
		w.SetErrorState(E_INVALID_REQUEST, "", "")
		w.InternalError = err
		return nil
	}

	ret := &AuthorizeRequest{
		State:       r.Form.Get("state"),
		Scope:       r.Form.Get("scope"),
		RedirectURI: unescapedURI,
		Authorized:  false,
		HttpRequest: r,
	}

	// must have a valid client
	ret.Client, err = w.Storage.GetClient(r.Form.Get("client_id"))
	if err != nil {
		w.SetErrorState(E_SERVER_ERROR, err.Error(), ret.State)
		w.InternalError = err
		return nil
	}
	if ret.Client == nil {
		w.SetErrorState(E_UNAUTHORIZED_CLIENT, ClientNotFound, ret.State)
		return nil
	}
	if ret.Client.GetRedirectURI() == "" {
		w.SetErrorState(E_UNAUTHORIZED_CLIENT, EmptyRedirectURI, ret.State)
		return nil
	}

	// check redirect uri, if there are multiple client redirect uri's
	// don't set the uri
	if ret.RedirectURI == "" && FirstURI(ret.Client.GetRedirectURI(), s.Config.RedirectURISeparator) == ret.Client.GetRedirectURI() {
		ret.RedirectURI = FirstURI(ret.Client.GetRedirectURI(), s.Config.RedirectURISeparator)
	}

	if err = ValidateURIList(ret.Client.GetRedirectURI(), ret.RedirectURI, s.Config.RedirectURISeparator); err != nil {
		w.SetErrorState(E_INVALID_REQUEST, RedirectURIMismatch, ret.State)
		w.InternalError = err
		return nil
	}

	w.SetRedirect(ret.RedirectURI)

	requestType := AuthorizeRequestType(r.Form.Get("response_type"))
	if s.Config.AllowedAuthorizeTypes.Exists(requestType) {
		switch requestType {
		case CODE:
			ret.Type = CODE
			ret.Expiration = s.Config.AuthorizationExpiration
		case TOKEN:
			ret.Type = TOKEN
			ret.Expiration = s.Config.AccessExpiration
		case ID_TOKEN:
			ret.Type = ID_TOKEN
		case CODE_ID_TOKEN:
			ret.Type = CODE_ID_TOKEN
			ret.Expiration = s.Config.AuthorizationExpiration
		case ID_TOKEN_TOKEN:
			ret.Type = ID_TOKEN_TOKEN
			ret.Expiration = s.Config.AccessExpiration
		}
		return ret
	}

	w.SetErrorState(E_UNSUPPORTED_RESPONSE_TYPE, "", ret.State)
	return nil
}

func (s *Server) FinishAuthorizeRequest(w *Response, r *http.Request, ar *AuthorizeRequest) {
	// don't process if is already an error
	if w.IsError {
		return
	}

	// force redirect response
	w.SetRedirect(ar.RedirectURI)

	if ar.Authorized {
		if ar.Type == TOKEN {
			w.SetRedirectFragment(true)

			ret := new(AccessRequest)
			s.outputAccessToken(w, r, ar, ret)

			if ar.State != "" && w.InternalError == nil {
				w.Output["state"] = ar.State
			}
		} else if ar.Type == CODE {
			ret := new(AuthorizeData)
			s.outputCode(w, r, ar, ret)
			w.Output["state"] = ret.State
		} else if ar.Type == ID_TOKEN {
			w.SetRedirectFragment(true)
			ret := new(AuthorizeData)
			s.outputIDToken(w, r, ar, ret)
			w.Output["state"] = ret.State
		} else if ar.Type == ID_TOKEN_TOKEN {
			w.SetRedirectFragment(true)

			accessRequestRet := new(AccessRequest)
			s.outputAccessToken(w, r, ar, accessRequestRet)
			if w.IsError {
				return
			}

			idTokenRet := new(AuthorizeData)
			s.outputIDToken(w, r, ar, idTokenRet)
			w.Output["state"] = idTokenRet.State
		} else if ar.Type == CODE_ID_TOKEN {
			w.SetRedirectFragment(true)
			ret := new(AuthorizeData)
			s.outputCode(w, r, ar, ret)
			s.outputIDToken(w, r, ar, ret)
			w.Output["state"] = ret.State
		}
	} else {
		// redirect with error
		w.SetErrorState(E_ACCESS_DENIED, "", ar.State)
	}
}

func (s *Server) outputAccessToken(
	w *Response,
	r *http.Request,
	ar *AuthorizeRequest,
	ret *AccessRequest,
) {
	// generate token directly
	ret.Type = IMPLICIT
	ret.Code = ""
	ret.Client = ar.Client
	ret.RedirectURI = ar.RedirectURI
	ret.Scope = ar.Scope
	ret.GenerateRefresh = false // per the RFC, should NOT generate a refresh token in this case
	ret.Authorized = true
	ret.Expiration = ar.Expiration
	ret.UserData = ar.UserData

	s.FinishAccessRequest(w, r, ret)
}

func (s *Server) outputIDToken(
	w *Response,
	r *http.Request,
	ar *AuthorizeRequest,
	ret *AuthorizeData,
) error {
	user, ok := gctx.AuthenticatedUser(r.Context())
	if !ok {
		return errors.New("user is not authenticated")
	}

	idToken, err := s.OIDC.GenerateIDToken(r.Context(), &oidc.GenerateIDTokenInput{
		SubjectID:          user.UserID(),
		AuthorizedParty:    ar.Client.GetID(),
		Audiences:          []string{ar.Client.GetID()},
		AuthenticationTime: time.Now(),
		Nonce:              ar.State,
		TwitchLDAPGroups:   user.Groups(),
	})
	if err != nil {
		return err
	}

	encodedIDToken, err := idToken.Encode(r.Context())
	if err != nil {
		return err
	}

	w.Output["id_token"] = string(encodedIDToken)
	return nil
}

func (s *Server) outputCode(
	w *Response,
	r *http.Request,
	ar *AuthorizeRequest,
	ret *AuthorizeData,
) {
	// generate authorization token
	ret.Client = ar.Client
	ret.CreatedAt = s.Now()
	ret.ExpiresIn = ar.Expiration
	ret.RedirectURI = ar.RedirectURI
	ret.State = ar.State
	ret.Scope = ar.Scope
	ret.UserData = ar.UserData

	// generate token code
	code, err := s.AuthorizeTokenGen.GenerateAuthorizeToken(ret)
	if err != nil {
		w.SetErrorState(E_SERVER_ERROR, err.Error(), ar.State)
		w.InternalError = err
		return
	}
	ret.Code = code

	// save authorization token
	if err = w.Storage.SaveAuthorize(ret); err != nil {
		w.SetErrorState(E_SERVER_ERROR, err.Error(), ar.State)
		w.InternalError = err
		return
	}

	// redirect with code
	w.Output["code"] = ret.Code
}
