package guardian

import "fmt"

// TestIdentifier is a mocked identifier for testing purposes
type TestIdentifier struct{}

const (
	testCN        = "Ladislav Kovács"
	testUID       = "guardian"
	testGIDNumber = 1
	testEmail     = "guardian@twitch.tv"
	testPassword  = "kappa"
	testGroupCN   = "navi"
)

// mock user and group data
var (
	TestUser = &User{
		CN:        testCN,
		UID:       testUID,
		GIDNumber: testGIDNumber,
		Email:     testEmail,
	}
	TestGroup = &Group{
		GID:     testGIDNumber,
		CN:      testGroupCN,
		Members: []string{testCN},
	}
)

// GetUser returns user
func (ti *TestIdentifier) GetUser(cn string) (user *User, err error) {
	if cn == TestUser.CN {
		user = TestUser
	}
	return
}

// GetUserByName returns user
func (ti *TestIdentifier) GetUserByName(username string) (*User, error) {
	if username == TestUser.UID {
		return TestUser, nil
	}

	return nil, nil
}

// ListUsers returns list of users
func (ti *TestIdentifier) ListUsers() ([]*User, error) {
	return []*User{TestUser}, nil
}

// GetGroup gets a single group
func (ti *TestIdentifier) GetGroup(cn string) (*Group, error) {
	if cn == testGroupCN {
		return TestGroup, nil
	}

	return nil, fmt.Errorf("No such group for cn '%s'.", cn)
}

// ListGroups returns groups
func (ti *TestIdentifier) ListGroups() ([]*Group, error) {
	return []*Group{TestGroup}, nil
}

// Authenticate checks username/password and returns success
func (ti *TestIdentifier) Authenticate(username, password string) (user *User, err error) {
	if username == testUID && password == testPassword {
		user, err = ti.GetUserByName(username)
		if err != nil {
			return nil, err
		}
	}

	return user, nil
}
