package guardian

import (
	"code.justin.tv/systems/guardian/osin"
)

// Storer implements the storage interface for guardian
type Storer interface {
	Clone() (storage osin.Storage)
	Close()
	SaveAccess(data *osin.AccessData) (err error)
	LoadAccess(token string) (data *osin.AccessData, err error)
	RemoveAccess(token string) (err error)
	LoadRefresh(token string) (data *osin.AccessData, err error)
	RemoveRefresh(token string) (err error)
	CheckToken(token string) (tc *TokenCheck, ok bool)
	LoadAuthorize(code string) (data *osin.AuthorizeData, err error)
	RemoveAuthorize(code string) (err error)
	SaveClient(client osin.Client) (err error)
	SaveAuthorize(data *osin.AuthorizeData) (err error)
	GetClient(id string) (client osin.Client, err error)
	GetAuthorizedClient(user *User, clientID string) (client osin.Client, err error)
	DeleteClient(client osin.Client) (err error)
	DeleteClientByID(user *User, id string) (err error)
	ListAuthorizedClients(user *User, startID string, limit int64) (clients []osin.Client, err error)
	ListClients(startID string, limit int64) (clients []osin.Client, err error)
}
