package guardian

import "code.justin.tv/systems/guardian/internal/gctx"

// TeamSSELDAPGroup is the name of the sse ldap group
const TeamSSELDAPGroup = "team-sse"

// Identifier is a user management interface for identity services
type Identifier interface {
	GetUser(cn string) (user *User, err error)
	GetUserByName(username string) (user *User, err error)
	ListUsers() (users []*User, err error)

	GetGroup(cn string) (group *Group, err error)
	ListGroups() (groups []*Group, err error)

	// checks ldap username & password
	Authenticate(username, password string) (user *User, err error)
}

// User defines a user in LDAP
type User struct {
	CN            string   `json:"cn,omitempty"`
	UID           string   `json:"uid,omitempty"`
	GIDNumber     int64    `json:"gid_number,omitempty"`
	HomeDirectory string   `json:"home_dir,omitempty"`
	UIDNumber     int64    `json:"uid_number,omitempty"`
	Email         string   `json:"email,omitempty"`
	SSHPubkeys    []string `json:"ssh_pubkeys,omitempty"`
	// Groups is an array of LDAP Group CNs
	Groups []string `json:"groups,omitempty"`
}

// AsGuardianContextUser returns an implementation that satifies gctx.User
func (u User) AsGuardianContextUser() gctx.User {
	return user{User: u}
}

type user struct {
	User
}

// CommonName implements gctx.User
func (u user) CommonName() string {
	return u.User.CN
}

// UserID implements gctx.User
func (u user) UserID() string {
	return u.User.UID
}

// GIDNumber implements gctx.User
func (u user) GIDNumber() int64 {
	return u.User.GIDNumber
}

// HomeDirectory implements gctx.User
func (u user) HomeDirectory() string {
	return u.User.HomeDirectory
}

// UIDNumber implements gctx.User
func (u user) UIDNumber() int64 {
	return u.User.UIDNumber
}

// Email implements gctx.User
func (u user) Email() string {
	return u.User.Email
}

// SSHPubkeys implements gctx.User
func (u user) SSHPubkeys() []string {
	return u.User.SSHPubkeys
}

// Groups implements gctx.User
func (u user) Groups() []string {
	return u.User.Groups
}

// Group defines a group in LDAP
type Group struct {
	GID int64 `json:"gid,omitempty"`
	// "ops", "infra", etc
	CN          string   `json:"cn,omitempty"`
	Description string   `json:"description,omitempty"`
	Members     []string `json:"members,omitempty"`
}
