package oidciface

import (
	"context"

	"code.justin.tv/systems/guardian/internal/oidc"
)

// OIDCAPI implements backend calls for the OIDC protocol
type OIDCAPI interface {
	GenerateIDToken(context.Context, *oidc.GenerateIDTokenInput) (oidc.IDToken, error)
	JSONWebKeySet(context.Context) (*oidc.JSONWebKeySet, error)
	OpenIDConfiguration(context.Context) *oidc.OpenIDConfiguration
}
