package tokeniface

import "context"

// EncoderAPI ...
type EncoderAPI interface {
	Encode(ctx context.Context, claims interface{}) ([]byte, error)
}
