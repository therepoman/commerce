package token

import (
	"context"

	"code.justin.tv/systems/guardian/internal/oidc/key/keyiface"
	"code.justin.tv/tshadwell/jwt"
)

// Encoder encodes JWT tokens for ID token usage
type Encoder struct {
	Key keyiface.KeyAPI
}

type header struct {
	KeyID     string `json:"kid"`
	Algorithm string `json:"alg"`
	Type      string `json:"typ"`
}

// Encode encodes a set of headers and claims as JWT.
func (e *Encoder) Encode(ctx context.Context, claims interface{}) ([]byte, error) {
	privateKey, err := e.Key.PrivateKey(ctx)
	if err != nil {
		return nil, err
	}

	return jwt.Encode(header{
		KeyID:     privateKey.KeyID(),
		Algorithm: privateKey.Name(),
		Type:      "JWT",
	}, claims, privateKey)
}
