package token

import (
	"encoding/json"
	"time"
)

// Time is a json marshallable time.Time for usage in JWT tokens.
type Time time.Time

// MarshalJSON implements json.Marshaller
func (t Time) MarshalJSON() ([]byte, error) {
	return json.Marshal(time.Time(t).Unix())
}

// UnmarshalJSON implements json.Unmarshaller
func (t *Time) UnmarshalJSON(data []byte) error {
	var asInt64 int64
	if err := json.Unmarshal(data, &asInt64); err != nil {
		return err
	}
	*t = Time(time.Unix(asInt64, 0).UTC())
	return nil
}
