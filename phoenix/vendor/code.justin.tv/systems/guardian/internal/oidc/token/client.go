// Package token encodes, signs, and decodes JWTs used for ID tokens.
package token
