package keyiface

import (
	"context"

	"code.justin.tv/systems/guardian/internal/oidc/key"
)

// KeyAPI ...
type KeyAPI interface {
	Start() error
	PublicKeys(context.Context) ([]key.PublicKey, error)
	PrivateKey(context.Context) (key.PrivateKey, error)
}
