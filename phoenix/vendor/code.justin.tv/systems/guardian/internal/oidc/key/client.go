// Package key retrieves JWT signing private and public keys from sandstorm.
package key

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.justin.tv/systems/sandstorm/callback"
	"code.justin.tv/systems/sandstorm/manager"
	"github.com/sirupsen/logrus"
)

// Client implements keyiface.KeyAPI
type Client struct {
	Logger              logrus.FieldLogger
	Sandstorm           manager.API
	guardianKeys        guardianKeys
	SandstormSecretName string
}

// Start starts the background updates
func (c *Client) Start() error {
	// put initial keys in the cache
	if err := c.getKeys(); err != nil {
		return err
	}

	splayer := &callback.Splayer{
		Duration: 5 * time.Second,
	}

	dw := &callback.Dweller{
		Duration: 10 * time.Second,
	}

	dwellSplayedCb := dw.Dwell(splayer.Splay(c.handleSecretUpdate))
	c.Sandstorm.RegisterSecretUpdateCallback(c.SandstormSecretName, dwellSplayedCb)

	return c.Sandstorm.ListenForUpdates()
}

func (c *Client) getKeys() error {
	keys, err := c.Sandstorm.Get(c.SandstormSecretName)
	if err != nil {
		return err
	}

	if keys == nil {
		return fmt.Errorf("keys not found in sandstorm: %s", c.SandstormSecretName)
	}

	return json.Unmarshal(keys.Plaintext, &c.guardianKeys)
}

// PublicKeys returns the current public keys
func (c *Client) PublicKeys(ctx context.Context) ([]PublicKey, error) {
	if len(c.guardianKeys.PublicKeys) == 0 {
		if err := c.getKeys(); err != nil {
			return nil, err
		}
	}
	return c.guardianKeys.PublicKeys, nil
}

// PrivateKey returns the current private key
func (c *Client) PrivateKey(ctx context.Context) (PrivateKey, error) {
	return c.guardianKeys.PrivateKey, nil
}

func (c *Client) handleSecretUpdate(input manager.SecretChangeset) {
	if input.SecretsHaveChanged() {
		if input.SecretHasBeenUpdated(c.SandstormSecretName) {
			if err := c.getKeys(); err != nil {
				c.Logger.Errorf("failed to get rotated keys, clearing key cache: %s", err.Error())
				c.guardianKeys = guardianKeys{}
			}
		}
	}
}
