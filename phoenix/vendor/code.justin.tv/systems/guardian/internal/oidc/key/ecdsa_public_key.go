package key

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/json"
	"errors"
)

var keyOpsSet = newStringSet("sign", "verify")

type ecdsaPublicKey struct {
	key   *ecdsa.PublicKey
	KeyID string
}

type ecdsaPublicKeyJSON struct {
	KeyType   string    `json:"kty"`
	Use       string    `json:"use"`
	Curve     string    `json:"crv"`
	KeyOps    stringSet `json:"key_ops"`
	Algorithm string    `json:"alg"`
	KeyID     string    `json:"kid"`
	X         *bigInt   `json:"x"`
	Y         *bigInt   `json:"y"`
}

// X, Y need to be 32 bytes with padding (big endian), and base64url encoded
func (e *ecdsaPublicKey) MarshalJSON() ([]byte, error) {
	return json.Marshal(ecdsaPublicKeyJSON{
		KeyType:   "EC",
		Use:       "sig",
		Curve:     "P-256",
		X:         &bigInt{Value: e.key.X},
		Y:         &bigInt{Value: e.key.Y},
		KeyOps:    *keyOpsSet,
		Algorithm: "ES256",
		KeyID:     e.KeyID,
	})
}

func (e *ecdsaPublicKey) UnmarshalJSON(inputJSON []byte) error {
	ecdsaPubKeyJSON := &ecdsaPublicKeyJSON{}
	err := json.Unmarshal(inputJSON, ecdsaPubKeyJSON)
	if err != nil {
		return err
	}

	if ecdsaPubKeyJSON.KeyType != "EC" {
		return errors.New("Incorrect KeyType")
	}
	if ecdsaPubKeyJSON.Use != "sig" {
		return errors.New("Incorrect Use")
	}
	if ecdsaPubKeyJSON.Curve != "P-256" {
		return errors.New("Incorrect Curve")
	}
	if ecdsaPubKeyJSON.Algorithm != "ES256" {
		return errors.New("Incorrect Algorithm")
	}

	if !keyOpsSet.IsSubset(ecdsaPubKeyJSON.KeyOps) {
		return errors.New("Incorrect KeyOps")
	}

	e.KeyID = ecdsaPubKeyJSON.KeyID
	e.key = new(ecdsa.PublicKey)
	e.key.X = ecdsaPubKeyJSON.X.Value
	e.key.Y = ecdsaPubKeyJSON.Y.Value
	e.key.Curve = elliptic.P256()

	return nil
}
