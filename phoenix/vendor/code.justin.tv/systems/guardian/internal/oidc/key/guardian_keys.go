package key

import "encoding/json"

// guardianKeys represent the keys in sandstorm
type guardianKeys struct {
	PublicKeys []PublicKey
	PrivateKey PrivateKey
}

func (keys *guardianKeys) UnmarshalJSON(value []byte) error {
	var rawKeys struct {
		PublicKeys []*ecdsaPublicKey
		PrivateKey *ecdsaPrivateKey
	}

	if err := json.Unmarshal(value, &rawKeys); err != nil {
		return err
	}

	var publicKeys []PublicKey
	for _, key := range rawKeys.PublicKeys {
		publicKeys = append(publicKeys, key)
	}

	keys.PrivateKey = rawKeys.PrivateKey
	keys.PublicKeys = publicKeys
	return nil
}
