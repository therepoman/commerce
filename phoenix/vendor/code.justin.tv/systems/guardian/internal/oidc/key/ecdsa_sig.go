package key

import (
	"errors"
	"math/big"
)

type ecdsaSignature struct {
	R, S *big.Int
}

func (e *ecdsaSignature) Encode() ([]byte, error) {
	rBytes := e.R.Bytes()
	sBytes := e.S.Bytes()
	out := make([]byte, 64)
	copy(out[32-len(rBytes):], rBytes)
	copy(out[64-len(sBytes):], sBytes)
	return out, nil
}

func (e *ecdsaSignature) Decode(sig []byte) error {
	if len(sig) > 64 {
		return errors.New("Invalid byte size")
	}

	e.R = big.NewInt(0).SetBytes(sig[:32])
	e.S = big.NewInt(0).SetBytes(sig[32:64])

	return nil
}
