package key

// PublicKey ...
type PublicKey interface {
	MarshalJSON() ([]byte, error)
	UnmarshalJSON([]byte) error
}
