package key

// PrivateKey represents a private key to sign JWTs with.
type PrivateKey interface {
	Sign(value []byte) ([]byte, error)
	Validate(value, signature []byte) error
	Size() int
	Name() string
	KeyID() string
}
