package key

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"errors"
)

// ErrInvalidSig is returned when a signature is invalid.
var ErrInvalidSig = errors.New("Invalid Signature")

type ecdsaPrivateKey struct {
	key   *ecdsa.PrivateKey
	keyID string
}

type privateKeyJSON struct {
	KeyType   string    `json:"kty"`
	Use       string    `json:"use"`
	Curve     string    `json:"crv"`
	KeyOps    stringSet `json:"key_ops"`
	Algorithm string    `json:"alg"`
	KeyID     string    `json:"kid"`

	X *bigInt `json:"x"`
	Y *bigInt `json:"y"`
	D *bigInt `json:"d"`
}

func (epk ecdsaPrivateKey) MarshalJSON() ([]byte, error) {
	return json.Marshal(privateKeyJSON{
		X: &bigInt{epk.key.PublicKey.X},
		Y: &bigInt{epk.key.PublicKey.Y},
		D: &bigInt{epk.key.D},

		KeyType:   "EC",
		Use:       "sig",
		Curve:     "P-256",
		KeyOps:    *keyOpsSet,
		Algorithm: "ES256",
		KeyID:     epk.keyID,
	})
}

func (epk *ecdsaPrivateKey) UnmarshalJSON(inputJSON []byte) error {
	unmarshaledJSON := &privateKeyJSON{}
	err := json.Unmarshal(inputJSON, unmarshaledJSON)
	if err != nil {
		return err
	}

	if unmarshaledJSON.KeyType != "EC" {
		return errors.New("Incorrect KeyType")
	}
	if unmarshaledJSON.Use != "sig" {
		return errors.New("Incorrect Use")
	}
	if unmarshaledJSON.Curve != "P-256" {
		return errors.New("Incorrect Curve")
	}
	if unmarshaledJSON.Algorithm != "ES256" {
		return errors.New("Incorrect Algorithm")
	}

	if !keyOpsSet.IsSubset(unmarshaledJSON.KeyOps) {
		return errors.New("Incorrect KeyOps")
	}

	epk.key = new(ecdsa.PrivateKey)
	epk.key.D = unmarshaledJSON.D.Value
	epk.key.PublicKey.Curve = elliptic.P256()
	epk.key.PublicKey.X = unmarshaledJSON.X.Value
	epk.key.PublicKey.Y = unmarshaledJSON.Y.Value
	epk.keyID = unmarshaledJSON.KeyID
	return nil
}

func (epk *ecdsaPrivateKey) KeyID() string {
	return epk.keyID
}

func (epk *ecdsaPrivateKey) Sign(value []byte) ([]byte, error) {
	hashedVal := sha256.New()
	if _, err := hashedVal.Write(value); err != nil {
		return nil, err
	}
	r, s, err := ecdsa.Sign(rand.Reader, epk.key, hashedVal.Sum(nil))

	if err != nil {
		return nil, err
	}

	ecdsaSig := &ecdsaSignature{
		R: r,
		S: s,
	}

	return ecdsaSig.Encode()
}

func (epk *ecdsaPrivateKey) Validate(value, signature []byte) error {
	sig := &ecdsaSignature{}
	err := sig.Decode(signature)
	if err != nil {
		return err
	}

	hashedVal := sha256.New()
	if _, err := hashedVal.Write(value); err != nil {
		return err
	}

	if ecdsa.Verify(&epk.key.PublicKey, hashedVal.Sum(nil), sig.R, sig.S) {
		return nil
	}

	return ErrInvalidSig
}

func (epk *ecdsaPrivateKey) Size() int {
	return 64
}

func (epk *ecdsaPrivateKey) Name() string {
	return "ES256"
}
