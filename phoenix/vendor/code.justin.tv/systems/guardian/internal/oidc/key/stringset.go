package key

import (
	"encoding/json"
)

type stringSet struct {
	keys map[string]interface{}
}

func (s stringSet) MarshalJSON() ([]byte, error) {
	var arrStr []string

	for key := range s.keys {
		arrStr = append(arrStr, key)
	}

	return json.Marshal(arrStr)
}

func (s *stringSet) UnmarshalJSON(input []byte) error {
	var unmarshalled []string

	err := json.Unmarshal(input, &unmarshalled)
	if err != nil {
		return err
	}

	s.keys = make(map[string]interface{})
	for _, key := range unmarshalled {
		s.Add(key)
	}

	return nil
}

func newStringSet(strs ...string) *stringSet {
	strSet := &stringSet{
		keys: make(map[string]interface{}),
	}
	for _, key := range strs {
		strSet.Add(key)
	}

	return strSet
}

func (s *stringSet) Add(str string) {
	s.keys[str] = nil
}

func (s *stringSet) Delete(str string) {
	delete(s.keys, str)
}

func (s *stringSet) Has(str string) bool {
	_, ok := s.keys[str]
	return ok
}

func (s *stringSet) IsSubset(strSet stringSet) bool {
	for str := range s.keys {
		if !strSet.Has(str) {
			return false
		}
	}
	return true
}
