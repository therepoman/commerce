package key

import (
	"encoding/base64"
	"math/big"
)

type bigInt struct {
	Value *big.Int
}

func (b bigInt) MarshalJSON() ([]byte, error) {
	// return out a zero padded b64url encoded bigint
	myInt := make([]byte, 32)
	num := b.Value.Bytes()

	copy(myInt[32-len(num):], num)

	b64StrLen := base64.StdEncoding.EncodedLen(len(myInt)) + 2

	encoded := make([]byte, b64StrLen)
	encoded[0] = '"'
	encoded[b64StrLen-1] = '"'
	base64.StdEncoding.Encode(encoded[1:], myInt)

	return encoded, nil
}

func (b *bigInt) UnmarshalJSON(inputNum []byte) error {
	inputStr := inputNum[1 : len(inputNum)-1]
	decoded := make([]byte, base64.StdEncoding.DecodedLen(len(inputStr)))
	_, err := base64.StdEncoding.Decode(decoded, []byte(inputStr))
	if err != nil {
		return err
	}

	b.Value = big.NewInt(0).SetBytes(decoded[:32])
	return nil
}
