// Package oidc provides the client needed to implement the OIDC interface.
package oidc

import (
	"context"
	"time"

	"code.justin.tv/systems/guardian/internal/oidc/key"
	"code.justin.tv/systems/guardian/internal/oidc/key/keyiface"
	"code.justin.tv/systems/guardian/internal/oidc/token"
	"code.justin.tv/systems/guardian/internal/oidc/token/tokeniface"
)

// Client that provides methods to serve the oidc protocol.
type Client struct {
	Config Config
	// key stores the public and private keys
	Key keyiface.KeyAPI
	// generates JWTs to be used as ID Tokens
	TokenEncoder tokeniface.EncoderAPI
}

// GenerateIDTokenInput ...
type GenerateIDTokenInput struct {
	// The subject identifier. Generally, this is the ldap id.
	SubjectID string
	// AuthorizedParty is the client_id of the service that is requesting the
	// token.
	AuthorizedParty string
	// Audiences is the list of services targetted for usage by this id token.
	Audiences          []string
	AuthenticationTime time.Time
	// Nonce is the nonce sent by the client
	Nonce string
	// User full name
	Name string
	// User's email address
	Email string
	// List of twitch ldap groups the subject is in.
	TwitchLDAPGroups []string
}

// GenerateIDToken generates an ID token.
func (c *Client) GenerateIDToken(ctx context.Context, req *GenerateIDTokenInput) (IDToken, error) {
	iat := time.Now().UTC()
	return idToken{
		idTokenJSON: idTokenJSON{
			IssuerID:           c.Config.IssuerID,
			SubjectID:          req.SubjectID,
			Audiences:          req.Audiences,
			ExpirationTime:     token.Time(iat.Add(c.Config.IDTokenTTL)),
			IssuedAtTime:       token.Time(iat),
			AuthenticationTime: token.Time(req.AuthenticationTime),
			Nonce:              req.Nonce,
			AuthorizedParty:    req.AuthorizedParty,
			Name:               req.Name,
			Email:              req.Email,
			TwitchLDAPGroups:   req.TwitchLDAPGroups,
		},
		tokenEncoder: c.TokenEncoder,
	}, nil
}

// JSONWebKeySet represents a JSON web key set used by an oidc provider
type JSONWebKeySet struct {
	Keys []key.PublicKey `json:"keys"`
}

// MaxCacheAge is the max time keys should be in client caches
func (JSONWebKeySet) MaxCacheAge() time.Duration {
	return time.Hour
}

// JSONWebKeySet returns the JSON web keys used by the oidc provider
func (c *Client) JSONWebKeySet(ctx context.Context) (*JSONWebKeySet, error) {
	publicKeys, err := c.Key.PublicKeys(ctx)
	if err != nil {
		return nil, err
	}
	return &JSONWebKeySet{
		Keys: publicKeys,
	}, nil
}

// OpenIDConfiguration is a JSON serializable openid configuration
type OpenIDConfiguration struct {
	Issuer                           string   `json:"issuer"`
	AuthorizationEndpoint            string   `json:"authorization_endpoint"`
	TokenEndpoint                    string   `json:"token_endpoint"`
	UserInfoEndpoint                 string   `json:"userinfo_endpoint"`
	JwksURI                          string   `json:"jwks_uri"`
	ResponseTypesSupported           []string `json:"response_types_supported"`
	SubjectTypesSupported            []string `json:"subject_types_supported"`
	IDTokenSigningAlgValuesSupported []string `json:"id_token_signing_alg_values_supported"`
	ScopesSupported                  []string `json:"scopes_supported"`
	ClaimsSupported                  []string `json:"claims_supported"`
}

// OpenIDConfiguration returns the openid configuration.
func (c *Client) OpenIDConfiguration(ctx context.Context) *OpenIDConfiguration {
	return &OpenIDConfiguration{
		Issuer:                           c.Config.IssuerID,
		AuthorizationEndpoint:            c.Config.AuthorizationEndpoint,
		TokenEndpoint:                    c.Config.TokenEndpoint,
		UserInfoEndpoint:                 c.Config.UserInfoEndpoint,
		JwksURI:                          c.Config.JwksURI,
		ResponseTypesSupported:           c.Config.ResponseTypesSupported,
		ScopesSupported:                  c.Config.ScopesSupported,
		SubjectTypesSupported:            []string{"private"},
		IDTokenSigningAlgValuesSupported: []string{"ES256"},
		ClaimsSupported: []string{
			"iss",
			"sub",
			"aud",
			"exp",
			"iat",
			"auth_time",
			"nonce",
			"azp",
			"name",
			"email",
			"groups",
		},
	}
}
