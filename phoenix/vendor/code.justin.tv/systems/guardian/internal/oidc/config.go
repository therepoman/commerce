package oidc

import "time"

// Config for Client
type Config struct {
	// iss of the ID tokens generated
	IssuerID string
	// oauth 2.0 authorization endpoint
	AuthorizationEndpoint string
	// oauth 2.0 token endpoint
	TokenEndpoint string
	// openid user info endpoint
	UserInfoEndpoint string
	// urls of json web key set document
	JwksURI string
	// list of oauth 2.0 response type values supported
	ResponseTypesSupported []string
	// list of oauth 2.0 scopes supported
	ScopesSupported []string
	// TTL ID tokens
	IDTokenTTL time.Duration
}
