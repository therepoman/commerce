package oidc

import (
	"context"

	"code.justin.tv/systems/guardian/internal/oidc/token"
	"code.justin.tv/systems/guardian/internal/oidc/token/tokeniface"
)

// IDToken represents an ID Token in OIDC
type IDToken interface {
	Encode(context.Context) ([]byte, error)
}

type idToken struct {
	idTokenJSON
	tokenEncoder tokeniface.EncoderAPI
}

type idTokenJSON struct {
	IssuerID           string     `json:"iss"`
	SubjectID          string     `json:"sub"`
	Audiences          []string   `json:"aud"`
	ExpirationTime     token.Time `json:"exp"`
	IssuedAtTime       token.Time `json:"iat"`
	AuthenticationTime token.Time `json:"auth_time"`
	Nonce              string     `json:"nonce,omitempty"`
	AuthorizedParty    string     `json:"azp"`
	Name               string     `json:"name"`
	Email              string     `json:"email"`
	TwitchLDAPGroups   []string   `json:"groups"`
}

// Encode returns IDToken in JWT format
func (it idToken) Encode(ctx context.Context) ([]byte, error) {
	return it.tokenEncoder.Encode(ctx, it)
}
