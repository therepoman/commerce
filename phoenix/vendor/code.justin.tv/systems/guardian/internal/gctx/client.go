package gctx

import (
	"context"
)

const (
	authorizingClientContextKey = contextKey("authorizingClient")
)

// Client represents a client that has requsted an access token.
type Client interface {
	ID() string
	IsExpired() bool
}

// SetAuthorizingClient sets the authorizing client in the context.
func SetAuthorizingClient(ctx context.Context, client Client) context.Context {
	return context.WithValue(ctx, authorizingClientContextKey, client)
}

// AuthorizingClient gets the authorizing client
func AuthorizingClient(ctx context.Context) (Client, bool) {
	client, ok := ctx.Value(authorizingClientContextKey).(Client)
	return client, ok
}
