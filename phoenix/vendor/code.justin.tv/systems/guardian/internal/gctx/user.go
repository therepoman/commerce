package gctx

import "context"

type contextKey string

const (
	authenticatedUserContextKey = contextKey("authenticatedUser")
)

// User represents a user to be inserted into the request scope.
type User interface {
	CommonName() string
	UserID() string
	GIDNumber() int64
	HomeDirectory() string
	UIDNumber() int64
	Email() string
	SSHPubkeys() []string
	Groups() []string
}

// SetAuthenticatedUser sets the authenticated user in the context.
func SetAuthenticatedUser(ctx context.Context, user User) context.Context {
	return context.WithValue(ctx, authenticatedUserContextKey, user)
}

// AuthenticatedUser gets the authenticated user in the context.
func AuthenticatedUser(ctx context.Context) (User, bool) {
	user, ok := ctx.Value(authenticatedUserContextKey).(User)
	return user, ok
}
